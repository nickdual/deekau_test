require 'active_support'
require 'mongoid'

class CategoryLanguage
  include Mongoid::Document
  #language, example :en, vn
  field :language
  field :name,    :type => String
  field :description,    :type => String
  embedded_in :category
end
