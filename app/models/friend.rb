class Friend
  include Mongoid::Document
  field :recent_chat, type: Hash
  field :uid, type: String
  embeds_many :friend_detail
end

class FriendDetail
  include Mongoid::Document
  field :uid, type: String
  field :username, type: String
  field :avatar, type: String
  field :status, type: Integer
  embedded_in :Friend
end


