require 'open-uri'
require 'net/http'
require 'openssl'
class Merchant
  include Mongoid::Document

  #embeds_many :offers

  has_many :offers
  belongs_to :user
  belongs_to :category
  embeds_many :images, as: :imageable
  
  embeds_many :addresses, as: :locatable
  
  field :name,    :type => String
  field :business_street,    :type => String
  field :business_city,    :type => String
  field :business_postal_code,    :type => String

  field :description,    :type => String
  field :user_rating,    :type => Integer
  field :mobile_number,    :type => String
  #  field :phone,    :type => String         # place in address model
  field :start_open_time, :type => Time
  field :end_open_time, :type => Time
  field :created_at, :type => Time,   :default => DateTime.current
  field :updated_at, :type => Time  

  #  field :tags, :type => Array # tags contain category names
  field :tags, :type => String
  index "tags" => 1
  
  field :city_tags ,    :type => Array # tags contain category names
  field :favourite_count , :type => Integer
  field :favourite_voters ,    :type => Array # tags contain voter
  field :mon, :type => Boolean
  field :tue, :type => Boolean
  field :wed, :type => Boolean
  field :thu, :type => Boolean
  field :fri, :type => Boolean
  field :sat, :type => Boolean
  field :sun, :type => Boolean
  field :local_mon_start_time, :type => DateTime
  field :utc_mon_start_time, :type => DateTime
  field :local_mon_end_time, :type =>DateTime
  field :utc_mon_end_time, :type => DateTime

  field :local_tue_start_time, :type => DateTime
  field :utc_tue_start_time, :type => DateTime
  field :local_tue_end_time, :type =>DateTime
  field :utc_tue_end_time, :type => DateTime

  field :local_wed_start_time, :type => DateTime
  field :utc_wed_start_time, :type => DateTime
  field :local_wed_end_time, :type =>DateTime
  field :utc_wed_end_time, :type => DateTime

  field :local_thu_start_time, :type => DateTime
  field :utc_thu_start_time, :type => DateTime
  field :local_thu_end_time, :type =>DateTime
  field :utc_thu_end_time, :type => DateTime

  field :local_fri_start_time, :type => DateTime
  field :utc_fri_start_time, :type => DateTime
  field :local_fri_end_time, :type =>DateTime
  field :utc_fri_end_time, :type => DateTime

  field :local_sat_start_time, :type => DateTime
  field :utc_sat_start_time, :type => DateTime
  field :local_sat_end_time, :type =>DateTime
  field :utc_sat_end_time, :type => DateTime

  field :local_sun_start_time, :type => DateTime
  field :utc_sun_start_time, :type => DateTime
  field :local_sun_end_time, :type =>DateTime
  field :utc_sun_end_time, :type => DateTime
  # gem geocoder
  include Geocoder::Model::Mongoid
  geocoded_by :add_description     # select field use to generate coordinates[lng,lat]
  after_validation :geocode
  
  #field :coordinates, :type => Array, :geo => true
  field :add_description, type: String
  field :phone , type: String
  field :mobile_phone , type: String

  field :quota , type: Integer

  #validation

  validates_presence_of :name
  #validates_presence_of :description
  #validates_presence_of :mobile_number
  #validates_presence_of :phone

#  def alert_error_sms(text,phone)
#    nexmo = Nexmo::Client.new('a0fe12e3', '6abb001c')
#    nexmo.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
#    response = nexmo.send_message({
#        from: 'Deekau',
#        to: phone, #'+84908077763'
#        text: text
#      })
#  end

  def self.check_num(text)
    return true if text.to_i.to_s == text
    return false
  end
  def self.create_merchant_by_mobile(text,phone_customer)
    #  Joe's haircuts,Get the best haircut ever at Joe's haircuts,1200-1500,  , 65 Cleary Court San Francisco CA 94109, 415922112
    error_structure = "your number of fields is wrong , please send message again"
    split_text = text.split(',')
    if split_text.count == 5
      arr_time = split_text[2].squish.split('-')
      
      # check arr_time is = 2 and it != blank
      if !arr_time.blank? &&  arr_time.count == 2 
        
        #check each time is all number not contain string
        if check_num(arr_time[0].to_s) && check_num(arr_time[1])
          begin
            start_open_time = Time.parse(arr_time[0][0..1].to_s + ':' + arr_time[0][2..3].to_s)
            end_open_time = Time.parse(arr_time[1][0..1].to_s + ':' + arr_time[1][2..3].to_s)
          rescue
            start_open_time = Time.parse('00:00')
            end_open_time = Time.parse('23:59')
          end
        else
          start_open_time = Time.parse('00:00')
          end_open_time = Time.parse('23:59')
        end
        @merchant = Merchant.create(:name => split_text[0].squish.split.each{|i| i.capitalize!}.join(' '),
          :add_description => split_text[3].squish,
          :phone => split_text[4].squish.scan(/\d+/).map { |n| n.to_i }.join(),
          :start_open_time => start_open_time ,
          :end_open_time => end_open_time
        )
        @offer = @merchant.offers.create(:description => split_text[1],
          :add_description => split_text[3].squish,
          :phone => split_text[4].squish  
        )
        puts @merchant.start_open_time
        puts @merchant.end_open_time
        return true
      else
        puts error_structure
        #      alert_error_sms(error_structure,phone_customer)
        return false
      end
    end
  end

  def self.get_google_places(content,city_id)
    places = []
    if content != ''  and city_id.present?
      city = City.find(city_id)
      if city
        test = URI.encode('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + content + '&location=' + city.latitude.to_s + ',' +  city.longitude.to_s + '&rankby=distance&sensor=false&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8')
        docs = URI.parse(test).read
        docs = JSON.parse(docs)
        i = 0
        if docs["status"] == 'OK'
          docs['predictions'].each do |prediction|
            item = {}
            item[:description]  = prediction["description"]
            results = URI.parse('https://maps.googleapis.com/maps/api/place/details/json?reference=' + prediction["reference"] + '&sensor=true&key=AIzaSyAzjO9pC_cyHdNYW74s_rc-5owbmrABsG8').read
            results = JSON.parse(results)
            if results["status"] == 'OK'
              results["result"]["address_components"].each do |type|
                if type["types"][0] == 'locality'
                  item[:city_name] = type["long_name"]
                end

                if type["types"][0] == 'country'
                  country = Country.where(:iso_code_two_letter => type["long_name"]).first
                  if country
                    item[:country_id] = country.iso_code_two_letter
                    if item[:city_name]
                      city = City.where( :name => /places[i]["city_name"].strip/i, :country_id => country.id).first
                      if city
                        item[:city_name] = city.name
                        item[:city_id] = city.id
                      else
                        item[:city_name]  = nil
                      end
                    end
                  end
                end
                if type["types"][0] == 'postal_code'
                  item[:postal_code] = type["long_name"]
                end
                if type["types"][0] == 'administrative_area_level_1'
                  item[:state_province_region] = type["long_name"]
                end
              end

              item[:name] = prediction["terms"][0]["value"]
              item[:merchant_type_name] = Merchant.merchant_type(results["result"]["types"][0])
              #if places[i]["merchant_type_name"]
              #  merchant_type = CategoryLanguage.where(:name => /places[i]["merchant_type_name"].strip/i)
              #  if merchant_type
              #    places[i]["merchant_type_id"] = merchant_type.id
              #  else
              #    places[i]["merchant_type_id"] = 1
              #
              #  end
              #else
              #  places[i]["merchant_type_id"] = 1
              #end
              item[:formatted_phone_number] = results["result"]["formatted_phone_number"]
              item[:formatted_address] = results["result"]["formatted_address"]
              item[:lat] = results["result"]["geometry"]["location"]["lat"]
              item[:lng] = results["result"]["geometry"]["location"]["lng"]
            end
            places << item
          end
        end
      end
    end
    return places
  end

  def self.merchant_type(merchant_type)
    if  ['ARTS AND ENTERTAINMENT','ENTERTAINMENT', 'BOWLING', 'COMEDY CLUBS','CONCERTS', 'DANCE CLASSES', 'DINING & NIGHTLIFE', 'GAY', 'GOLF', 'GYM', 'KIDS','OUTDOOR ADVENTURES', 'SKIING','SKYDIVING','SPORTING EVENTS', 'THEATER'].include? merchant_type.strip.upcase
      return 'Fun'
    end
    if ['AUTOMOTIVE','EDUCATION', 'HOME SERVICES','PETS','PROFESSIONAL SERVICES','SERVICES','BABY','JEWISH','KOSHER'].include? merchant_type.strip.upcase
      return 'Services'
    end
    if ['BEAUTY & SPA', 'BEAUTY & SPAS', 'HEALTH & FITNESS', 'BOOT CAMP','FACIAL', 'FITNESS', 'FITNESS CLASSES','HAIR SALON','HEALTH & BEAUTY','MAKEUP','MANICURE & PEDICURE','MARTIAL ARTS','MASSAGE','PERSONAL TRAINING','PILATES','SPA', 'TANNING','YOGA'].include? merchant_type.strip.upcase
      return 'Look & Feel'
    end
    if ['FOOD & DRINK', 'RESTAURANTS', 'RESTAURANT', 'BAKERY','TREATS'].include? merchant_type.strip.upcase
      return 'Eat'
    end
    if ['GROUPON'].include? merchant_type.strip.upcase
      return 'Default'
    end
    if ['NIGHTLIFE', 'BAR', 'BARS & CLUBS', 'WINE TASTING'].include? merchant_type.strip.upcase
      return 'Drink'
    end
    if ['SHOPPING','FOOD & GROCERY',"MEN'S CLOTHING",'PETS','RETAIL & SERVICES',"WOMEN'S CLOTHING" ].include? merchant_type.strip.upcase
      return 'Shop'
    end
    if ['TRAVEL','CITY TOURS', 'MUSEUMS', 'TRAVEL'].include? merchant_type.strip.upcase
      return 'Tourism'
    end
    return merchant_type
  end

end
