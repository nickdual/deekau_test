require 'active_support'
require 'mongoid'


class Image
  include Mongoid::Document
  field :url, :type => String
  field :created_at, :type => Time,   :default => DateTime.current
  field :active, :type => Boolean
  field :img_url_web, :type => String
  mount_uploader :url, ImageUploader
  embedded_in :imageable , polymorphic: true


#  def self.find_by_name(input)
#    return City.where("name = '"+"#{input}"+"'").first
#  end
end
