class Photo
  include Mongoid::Document
  field :image_file_name, :type => String
  field :description, :type => String
  field :image_type, :type => String

end
