require 'active_support'
require 'mongoid'

class City
  include Mongoid::Document
  #has_many :offers
  #  embedded_in :country
  field :_id, :type => String
  field :geonames_id, :type => Integer
  # field :country_id,    :type => String
  field :name,    :type => String
  field :ascii_name, :type => String
  field :alternate_name,:type =>  String
  field :slug,    :type => String
  field :state,    :type => String
  field :latitude , :type => BigDecimal
  field :longitude, :type => BigDecimal
  # field :country_iso_code_two_letters, :type => String
  field :geonames_timezone_id,:type => Integer
  field :timezone,    :type => String
  field :country_tag, :type => String
  #  validates_presence_of :name
  #  validates_presence_of :slug
  #  validates_presence_of :lat
  #  validates_presence_of :lon
  #  validates_uniqueness_of :slug

  
  def self.find_by_name(input)
    return City.where("name = '"+"#{input}"+"'").first
  end
end
