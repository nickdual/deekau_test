# To change this template, choose Tools | Templates
# and open the template in the editor.
require 'mongoid'

class UserBasicInfo
  include Mongoid::Document
  embedded_in :user
  field :birthday, :type => Hash
  field :sex, :type => Hash
  field :languages , :type => String
  field :hometown , :type => String
  field :relationship_status , :type => Hash

end
