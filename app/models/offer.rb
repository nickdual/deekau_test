require 'active_support'
require 'mongoid'

class Offer
  include Mongoid::Document
  include Geocoder::Model::Mongoid

  #  geocoded_by :primary_address
  #  after_validation :geocode

  embeds_many :images , as: :imageable
  #embedded_in :merchant
  #embedded_in :merchant_test

  embeds_many :addresses, as: :locatable
  # belongs_to :city
   belongs_to :merchant
  # belongs_to :user

  # temporary
  field :status,:type => String
  #field :merchant_id,    :type => String



  # field :coordinates, :type => Array

  # field :city_id,    :type => String
  # field :user_id,    :type => String
  field :merchant_id,    :type => String
  field :name,    :type => String
  field :active,    :type => Integer
  field :description,    :type => String
  # field :primary_address,    :type => String
  field :start_time,    :type => Time
  field :end_time,    :type => Time
  field :category_id, :type => String
  field :category_name , :type => String
  field :price,    :type => Float#
  field :value,    :type => Float#
  field :discount,    :type => Float#
  field :link,    :type => String
  # field :image,    :type => String
  # field :image_thumbnail,    :type => String
  #  field :lon,    :type => Float
  #  field :lat,    :type => Float
  field :currency,    :type => String
  field :timezone,    :type => String
  # field :city_name,    :type => String
  # field :day_in_week,    :type => Integer

  embeds_many :day_in_weeks
  field :favourite_count,:type => Integer
  field :favourite_voters ,    :type => Array # tags contain voter




  # gem geocoder
  include Geocoder::Model::Mongoid
  geocoded_by :add_description     # select field use to generate coordinates[lng,lat]
  after_validation :geocode

  field :coordinates, :type => Array
  field :add_description, type: String
  field :phone , type: String
  field :mobile_phone , type: String

  field :created_at,    :type => DateTime, :default => DateTime.current
  field :updated_at,    :type => DateTime, :default => DateTime.current
  field :mon, :type => Boolean
  field :tue, :type => Boolean
  field :wed, :type => Boolean
  field :thu, :type => Boolean
  field :fri, :type => Boolean
  field :sat, :type => Boolean
  field :sun, :type => Boolean
  field :local_mon_start_time, :type => DateTime
  field :utc_mon_start_time, :type => DateTime
  field :local_mon_end_time, :type => DateTime
  field :utc_mon_end_time, :type => DateTime

  field :local_tue_start_time, :type => DateTime
  field :utc_tue_start_time, :type => DateTime
  field :local_tue_end_time, :type => DateTime
  field :utc_tue_end_time, :type => DateTime

  field :local_wed_start_time, :type => DateTime
  field :utc_wed_start_time, :type => DateTime
  field :local_wed_end_time, :type => DateTime
  field :utc_wed_end_time, :type => DateTime

  field :local_thu_start_time, :type => DateTime
  field :utc_thu_start_time, :type => DateTime
  field :local_thu_end_time, :type => DateTime
  field :utc_thu_end_time, :type => DateTime

  field :local_fri_start_time, :type => DateTime
  field :utc_fri_start_time, :type => DateTime
  field :local_fri_end_time, :type => DateTime
  field :utc_fri_end_time, :type => DateTime

  field :local_sat_start_time, :type => DateTime
  field :utc_sat_start_time, :type => DateTime
  field :local_sat_end_time, :type => DateTime
  field :utc_sat_end_time, :type => DateTime

  field :local_sun_start_time, :type => DateTime
  field :utc_sun_start_time, :type => DateTime
  field :local_sun_end_time, :type => DateTime
  field :utc_sun_end_time, :type => DateTime
  field :status_quota, :type => Boolean
  #validation
  #validates_presence_of :name
  validates_presence_of :description
  #validates_presence_of :value
  #validates_presence_of :price
  #validates_presence_of :category_id





end
