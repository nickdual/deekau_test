# To change this template, choose Tools | Templates
# and open the template in the editor.
require 'mongoid'

class Address
  include Mongoid::Document
  embedded_in :locatable, polymorphic: true
  belongs_to :user
  field :is_primary, type: Boolean
#  field :location, type: Array
  field :description, type: String
  field :phone , type: String
  field :mobile_phone , type: String
end
