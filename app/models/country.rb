require 'mongoid'

class Country
  include Mongoid::Document
  embeds_many :cities
#  field :name,    :type => String
  field :code,    :type => String
  field :iso_code_two_letter, :type => String
  field :iso_code_three_letter, :type => String
  field :iso_number, :type => Integer
  field :continent, :type => String
  
  field :created_at,    :type => DateTime, :default => DateTime.current
  field :updated_at,    :type => DateTime, :default => DateTime.current
  field :_id,:type => String
#  validates_presence_of :name
#  validates_presence_of :code
#  validates_uniqueness_of :name
#  validates_uniqueness_of :code

  def find_by_name(input)
    return Country.where("name = #{input}").first
  end
end
