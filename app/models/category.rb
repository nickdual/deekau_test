require 'active_support'
require 'mongoid'

class Category
  include Mongoid::Document

  field :tags, :type => Array # list of parent category names
  field :color, :type => String
  field :large_icon, :type => String
  field :map_icon, :type => String
  field :marker_icon, :type => String
  embeds_many :category_languages
end
