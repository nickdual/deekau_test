require 'active_support'
require 'mongoid'

class UserRole
  include Mongoid::Document
  has_many :users , autosave: true
  field :name,    :type => String
end
