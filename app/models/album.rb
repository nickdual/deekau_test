require 'active_support'

class Album
  include Mongoid::Document

  field :path, :type => String
  field :style, :type => String
  belongs_to :user

end
