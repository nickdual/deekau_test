require 'active_support'
require 'mongoid'

class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable ,:confirmable#:omniauthable

  devise :omniauthable

  belongs_to :user_role
  has_many :merchants

  #embeds_one :address, as: :locatable
  #embeds_one :profile_basic_info
  #accepts_nested_attributes_for :user_basic_info
  embeds_many :images

  embeds_many :profile_universities
  embeds_many :profile_high_schools
  embeds_many :profile_companys
  #Data basic info
  field :first_name, :type => String
  field :last_name, :type => String
  field :birthday, :type => Hash
  field :sex, :type => Hash
  field :languages , :type => String
  field :country , :type => String
  field :relationship_status , :type => Hash
  field :avatar ,:type => String
  field :about_me , :type => Hash
  field :favorite, :type => Hash
  field :web_url , :type => String
  field :address, :type => String
  field :hometown , :type => String


  embeds_many :user_providers
  embeds_many :user_invitations


  ## Database authenticatable
  field :username,:type => String
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  field :phone , :type => Hash



  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String


  # field :user_role_id,      :type => String
  #field :merchant_id,       :type => String
  ## Encryptable
  # field :password_salt, :type => String

  ## Confirmable
   field :confirmation_token,   :type => String
   field :confirmed_at,         :type => Time
   field :confirmation_sent_at, :type => Time
   field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
   field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
   field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
   field :locked_at,       :type => Time

  ## Token authenticatable
  #field :authentication_token, :type => String


  attr_accessible :email,:email_confirmation,:password,:user_name,:provider,:name,:about_me,:favorite,:phone,:birthday,:sex,:address,:country,:languages,:web_url,:first_name,:last_name,:hometown

  validates_uniqueness_of :email
  def self.find_by_email(email)
    return User.where(:email =>"#{email}").first
  end

  def role?(name)
    return (self.user_role.name == name)? true : false
  end

  def self.find_for_oauth(auth, signed_in_resource=nil)
    if (auth.provider == 'google_oauth2')
      auth.uid = auth.info.email
    end
    user = User.where("email" => auth.info.email).first
    unless user
      user = User.new(username: auth.info.nickname,
                      email:auth.info.email,
                      password:Devise.friendly_token[0,20],

      )
      user.skip_confirmation!
      user.user_providers = [UserProvider.new(name: auth.provider, uid: auth.uid, token: auth.credentials.token)]
      user.save
    else
      result = user.user_providers.where({name: auth.provider})
      if (result.count == 0)
        user.user_providers.push(UserProvider.new(name: auth.provider, uid: auth.uid, token: auth.credentials.token))
      else
        result.first.update_attributes(token: auth.credentials.token, uid: auth.uid)
      end
    end
    return user
  end

  def self.find_for_oauth_secret(auth, signed_in_resource=nil)
    user = User.where("email" => auth.info.email).first
    unless user
      user = User.new(username: auth.info.nickname,
                      email:auth.info.email,
                      password:Devise.friendly_token[0,20],

      )
      user.skip_confirmation!
      user.user_providers = [UserProvider.new(name: auth.provider, uid: auth.uid, token: auth.credentials.token, secret: auth.credentials.secret)]
      user.save
    else
      result = user.user_providers.where({name: auth.provider})
      if (result.count == 0)
        user.user_providers.push(UserProvider.new(name: auth.provider, uid: auth.uid, token: auth.credentials.token, secret: auth.credentials.secret))
      else
        result.first.update_attributes(token: auth.credentials.token, uid: auth.uid, secret: auth.credentials.secret)
      end
    end
    return user
  end

  def self.add_friend_detail(friend, id, username, avatar, status)
    fr_detail = friend.friend_detail.where(:uid => id).first
    if fr_detail == nil
      friend.friend_detail.push(FriendDetail.new(uid: id, username: username, avatar: avatar, status: status))
    else
      fr_detail.update_attributes(status: status)
    end
  end

  def self.friend_action(uid1, uid2, action)
    if (uid1.to_s == uid2.to_s)
      return
    end
    friend_root = Friend.find_or_create_by(:uid => uid1)
    friend_guest = Friend.find_or_create_by(:uid => uid2)
    if (action == 'friend' || action == 'accept')
      user_root = User.where("_id" => uid1).first
      user_guest = User.where("_id" => uid2).first
      add_friend_detail(friend_root, uid2, user_guest['username'], user_guest['avatar'], 3)
      add_friend_detail(friend_guest, uid1, user_root['username'], user_root['avatar'], 3)
    elsif (action == 'make')
      user_root = User.where("_id" => uid1).first
      user_guest = User.where("_id" => uid2).first
      add_friend_detail(friend_root, uid2, user_guest['username'], user_guest['avatar'], 1)
      add_friend_detail(friend_guest, uid1, user_root['username'], user_root['avatar'], 2)
    elsif (action == 'refuse' || action == 'unfriend')
      friend_root.friend_detail.where(:uid => uid2).delete
      friend_guest.friend_detail.where(:uid => uid1).delete
    end
  end
end

class UserProvider
  include Mongoid::Document
  field :uid, :type => String
  field :token, :type => String
  field :name, :type => String
  embedded_in :user
end


class UserInvitation
  include Mongoid::Document
  field :uid, :type => String
  field :provider, :type => String
  field :time, :type => Integer
  embedded_in :user
end

class ProfileUniversity
  include Mongoid::Document
  field :name, :type => String
  field :faculty, :type => String
  field :year, :type => String
  field :address, :type => String
  field :created_at, :type => Time,   :default => DateTime.current
  embedded_in :user , polymorphic: true

end
class ProfileHighSchool
  include Mongoid::Document
  field :name, :type => String
  field :high_school_address, :type => String
  field :year, :type => String
  field :created_at, :type => Time,   :default => DateTime.current
  embedded_in :user , polymorphic: true

end
class ProfileCompany
  include Mongoid::Document
  field :name, :type => String
  field :company_address, :type => String
  field :year, :type => String
  field :position, :type => String
  field :created_at, :type => Time,   :default => DateTime.current
  embedded_in :user , polymorphic: true
end
