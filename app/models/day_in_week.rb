# To change this template, choose Tools | Templates
# and open the template in the editor.
require 'active_support'
require 'mongoid'

class DayInWeek
  include Mongoid::Document
  field :is_close,:type => Boolean,default: -> {false}
#  field :name, :type => Integer
  field :start_time, :type => String
  field :end_time, :type => String
  field :_id,:type => Integer
end
