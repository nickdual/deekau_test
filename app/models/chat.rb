class Chat
  include Mongoid::Document
  field :uid1, type: String
  field :uid2, type: String
  field :log, type: Hash
  field :date, type: Time
  field :count, type: Integer
end
