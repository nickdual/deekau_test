class MerchantImagesController < ApplicationController
  def destroy
    @merchant = Merchant.find(params[:object_id])
    @merchant.images.find(params[:id]).destroy
    if is_mobile_device?
      redirect_to @merchant
    else
      render :text => 'ok'
    end
  end
end
