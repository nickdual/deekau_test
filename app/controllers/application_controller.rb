class ApplicationController < ActionController::Base
  has_mobile_fu # Detect the device type
  before_filter :set_locale
  before_filter :set_request_format
  before_filter :check_cookies_city_country

  def set_request_format
    request.format = :mobile if is_mobile_device?
  end
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Access denied."
    redirect_to root_url
  end
  def current_ability
    @current_ability ||= Ability.new(current_user)
  end


  def current_city
#    @current_city = City.find_by_name("New York")
  end
  
  def current_country
    return current_city.country
  end
  
  def check_cookies_city_country # :D
#    cookies['city_id'] = City.where(name: 'HCM').first.id.to_s
#    cookies['country_id'] = 'VN'
  end

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || session[:social_login_back] || '/'
  end
end
