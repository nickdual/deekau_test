class AlbumsController < ApplicationController
  # GET /articles
  # GET /articles.json
  def index
    @albums = Album.all

    respond_to do |format|
      format.html # index.html.erbb
      format.json { render json: @albums }
    end
  end


end
