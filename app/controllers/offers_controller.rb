require "simple_time_select"
class OffersController < ApplicationController
  #  load_and_authorize_resource
  include Geocoder::Model::Mongoid
  include ApplicationHelper
  LIMIT_OFFER = 5

  def set_favourite # Dieu :D
    @merchant = Merchant.find(params[:merchant_id])
    @offer = @merchant.offers.find(params[:id])
    if params[:status].to_i == 0
      @offer.favourite_count +=1
      @offer.favourite_voters += current_user.id.to_s.to_a
      @offer.save
      render :json => 'vote'.to_json
    else
      @offer.favourite_count -=1
      @offer.favourite_voters -= current_user.id.to_s.to_a
      @offer.save
      render :json => 'unvote'.to_json
    end
    
  end
    
  def index # Dieu , not finish

    #tam comment lai de chay post offer => Tin
    @offer = Offer.new
    #@categories = Category.where(tags: nil).to_a
    #@categories.collect! {|item| {:label => item.category_languages[0].name, :id =>item._id }}
    #city_name = City.find(cookies['city_id']).name.to_a
    #if session[:lon]
    #  puts '111111111111111111111111111111111111'
    #  puts session[:lat] + ', ' + session[:lon]
    #  @arr_merchant = Merchant.near([ session[:lat].to_f , session[:lon].to_f ], 10000).order("distance").map(& :offers)
    #  @offers =  get_offer(@arr_merchant,LIMIT_OFFER)
    #else
    #  puts '2222222222222222222222222222'
    #  @arr_merchant = Merchant.near([request.location.lat,request.location.lng], 10000).order("distance").map(& :offers)
    #  @offers =  get_offer(@arr_merchant,LIMIT_OFFER)
    #end
    
  end

  def new
    @merchant = Merchant.find(params[:merchant_id])
    @offer = @merchant.offers.new
    @offer.day_in_weeks.build
  end

  def show
    @offer = Merchant.find(params[:merchant_id]).offers.find(params[:id])
  end

  def edit
    @offer = Merchant.find(params[:merchant_id]).offers.find(params[:id])
  end

  def create
    @offers=[]
    #    params[:event][:time] = Time.parse(params[:event][:"time(5i)"])
    #    params[:event].delete(:”time(5i)”).


    @merchant = Merchant.find(params[:id])
    @offer = @merchant.offers.new(params[:offer])
    if !params[:files].nil?

        if @offer.save
          params[:files].each do |file|
            @offer.images.create(:url=>  file)
          end
          @offers << @offer
              respond_to do |format|
            format.html { redirect_to '/' }
            format.json { render json: @offers }
          end

        else
          #respond_to do |format|
          #  format.json { render json: @offer.errors }
          #end
          render :text => 'false1'
        end

    else
      render :text => false
    end
  end

  def destroy
    @merchant = Merchant.find(params[:merchant_id])
    @offer = @merchant.offers.find(params[:id])
    @offer.destroy
    redirect_to @merchant
  end

  def update
    @offer = Merchant.find(params[:merchant_id]).offers.find(params[:id])
    @offer.update_attributes(params[:offer])
    redirect_to @offer
  end

  def get_more_offers # Dieu :D
    city_name = City.find(cookies['city_id']).name.to_a
    if params[:offset] == 'undefined' || params[:merchant_offset]== 'undefined'
      render :text => ''
    else
      
      @arr_merchant = Merchant.near([ session[:lat].to_f , session[:lon].to_f ], 10000).order("distance").in(:city_tags=>city_name).map(& :offers)
      @offers =  get_offer(@arr_merchant,params[:offset].to_i,LIMIT_OFFER)
      
      #      @next_offset = params[:offset].to_i + 1
      #      @html_offers = render_to_string :partial => "get_more_offer"
      #      render :json => @html_offers.to_json
      render :json => @offers.to_json
    end
  end


  def get_image  # Dieu :D
    @images = []
    #@merchant = Merchant.find(params[:object_parent_id])
    #@offer = @merchant.offers.find(params[:object_id])
    @offer = Offer.all()
    if !params[:files].nil?
      params[:files].each do |file|
        @images << @offer.images.create(:url=>file)
      end
    end
    if !params[:object_id].nil? && !params[:image].nil?
      @images << @offer.images.create(:url=>params[:image])
    end

    if params[:files].nil?
      @images = @offer.images
    end
    if is_mobile_device?
      #redirect_to merchant_offer_path(@merchant,@offer)

    else
      respond_to do |format|
        format.html # index.html.erbb
        format.json { render json: @images }
      end
    end
    render :text => 'true'
  end

  def list_offer # Dieu :D
    city_name = City.find(cookies['city_id']).name.to_a
    if session[:lon]
      puts '111111111111111111111111111111111111'
      puts session[:lat] + ', ' + session[:lon]
      @arr_merchant = Merchant.near([ session[:lat].to_f , session[:lon].to_f ], 10000).order("distance").in(:city_tags=>city_name).map(& :offers)
      @offers =  get_offer(@arr_merchant,0,LIMIT_OFFER)
    else
      puts '2222222222222222222222222222'
      @arr_merchant = Merchant.near([request.location.lat,request.location.lng], 10000).order("distance").in(:city_tags=>city_name).map(& :offers)
      @offers =  get_offer(@arr_merchant,0,LIMIT_OFFER)
    end
  end

  def get_google_places
    session[:city_id] = 'West New York_United States'
    merchants = Merchant.get_google_places(params[:content], session[:city_id])
    puts merchants
    render :json => merchants
  end

  def get_offer
    @offers = []
    @offers = Offer.where(:merchant_id => params[:id])
    render json: @offers

  end


end
