class RegistrationsController < Devise::RegistrationsController
  def create
    @user = User.new(params[:user])
    begin
      @client = Twilio::REST::Client.new ACCOUNT_SID, AUTH_TOKEN
      @client.account.sms.messages.create(:from => TWILIO_NUM, :to => params[:user][:mobile_number],
        :body => "please login yacto.com with username=" + params[:user][:username] + ",password=" + params[:user][:password])
    rescue
      puts 'International SMS delivery not available'
    end
    if  @user.save
      flash[:notice] = t("users.confirmationsent")
      respond_to do |format|
        format.json { render json: @user }
      end
    else
      respond_to do |format|
        format.json { render json: @user.errors }
      end
    end  
  end
end
