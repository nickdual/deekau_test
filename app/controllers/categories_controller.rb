class CategoriesController < ApplicationController
  
  def sub_category
    
  end
  
  def category
    @category = Category.first(conditions: { name: params[:category] })
    @categories = Category.all(conditions: { parent_tag: [params[:category]]})
    #    render :json =>  @sub_category
    render :partial => 'categories/category'
  end
  
  def offers
    
  end
  def top_lvl
    @query = /^#{params[:term]}/i
    @categories = Category.where("category_languages.name" => @query).to_a
    #@categories = Category.where(tags: nil).to_a
    @categories.collect! {|item| {:label => item.category_languages[0].name, :id =>item._id }}
    render :json => @categories
  end
end
