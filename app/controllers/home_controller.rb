class HomeController < ApplicationController
#  require 'geokit'
#  include Geokit::Geocoders
  def index
    @offer = Offer.new
    if current_user
      @cur_user = current_user
      puts(@cur_user.id)
      params[:user_id] = @cur_user.id
      @merchants = Merchant.where(user_id: @cur_user.id)
      @merchant_check= @merchants.length != 0? true : false

    end

  end

  def lookup_address
      loc = Geocoder.coordinates(params[:address])
     puts loc
    if loc
      render :text => "not found", :status => 500
    else
      render :text => loc.to_s
    end
    
  end

  def autocomplete
    @query = /^#{params[:term]}/i
    @cities = City.all(conditions: { name: @query},limit: 50).to_a
    @cities.collect! {|item| {:label => item.name, :id => item.id}}
    render :json => @cities
  end
  
  def autocomplete_category
    #@query = /^#{params[:term]}/i
    #@categories = Category.where("category_languages.name" => @query).to_a
    #puts @query
    #puts  @categories
    #@categories.collect! {|item| {:label => item.name, :id => item.id}}
    #render :json => @categories
    @categories = Category.all
  end
  
  def get_current_position
    render :text => 'ok'
    session[:lat] = params[:lat]
    session[:lon] = params[:lon]
  end

  def social_post
    params.each do |key, value|
      session["#{key}"] = value
      break
    end
    render :text => 'ok'
  end
end

