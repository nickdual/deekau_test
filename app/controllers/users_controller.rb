require 'gmail_xoauth'

class UsersController < ApplicationController

  def index
  end

  def show

    @user = current_user
    #@images = []
    #if !params[:files].nil?
    #  params[:files].each do |file|
    #    @images << @user.images.create(:url=>file)
    #  end
    #end
    #if !params[:object_id].nil? && !params[:image].nil?
    #  @images << @user.images.create(:url=>params[:image])
    #end
    #
    #if params[:files].nil?
    #  @images = @user.images
    #end
    #if is_mobile_device?
    #  redirect_to @user
    #else
    #  respond_to do |format|
    #    format.html # index22.html.erb
    #    format.json { render json: @images }
    #  end
    #end
  end

  def new
  end

  def edit
  end

  def create
  end

  def update

    @user = User.find(current_user.id)
    flag = params[:user][:flag]

    if flag == '1'

      @user.relationship_status = [params[:user][:relationship_status], params[:user][:policy_relationship_status]]
      @user.sex= [params[:user][:sex], params[:user][:policy_sex]]
      @user.birthday= [params[:user][:birthday], params[:user][:policy_birthday]]
      @user.update_attributes(:address => params[:user][:address], :country => params[:user][:country], :hometown => params[:user][:hometown], :languages => params[:user][:languages])
      if @user.save
        render json: @user
      else
        puts('er')
      end

    elsif flag == '2'

      @user.profile_companys[0].name = params[:user][:company_name]
      @user.profile_companys[0].company_address = params[:user][:company_address]
      @user.profile_companys[0].year = params[:user][:year_work] if params[:user][:year_work].present?
      @user.profile_companys[0].position = params[:user][:position] if params[:user][:position].present?
      if @user.save
        render json: @user
      else
        puts('error')
      end

    elsif flag == '8'
      @user.profile_companys.create(:name => params[:user][:company_name], :company_address => params[:user][:company_address], :year => params[:user][:year_work], :position => params[:user][:position])
      render json: @user

    elsif flag == '3'

      @user.profile_universities[0].name = params[:user][:profile_universities]
      @user.profile_universities[0].faculty = params[:user][:faculty]
      @user.profile_universities[0].address = params[:user][:university_address] if params[:user][:university_address].present?
      @user.profile_universities[0].year = params[:user][:year] if params[:user][:year].present?
      if @user.save
        render json: @user
      else
        puts('error')
      end

    elsif flag == '9'

      @user.profile_universities.create(:name => params[:user][:profile_universities], :faculty => params[:user][:faculty], :address => params[:user][:university_address], :year => params[:user][:year])
      render json: @user

    elsif flag == '4'

      @user.profile_high_schools[0].name = params[:user][:high_schools]
      @user.profile_high_schools[0].high_school_address = params[:user][:high_schools_address]
      @user.profile_high_schools[0].year = params[:user][:high_schools_year] if params[:user][:high_schools_year].present?
      if @user.save
        render json: @user
      else
        puts('error')
      end

    elsif flag == '10'
      @user.profile_high_schools.create(:name => params[:user][:high_schools], :high_school_address => params[:user][:high_schools_address], :year => params[:user][:high_schools_year])
      render json: @user
    elsif flag == '5'
      @user.about_me = [params[:user][:about_me], params[:user][:policy_about_me]]

      if @user.save
        render json: @user
      else
        puts('error')
      end


    elsif flag == '6'

      @user.favorite = [params[:user][:favorite], params[:user][:policy_favorite]]
      if @user.save
        render json: @user
      else
        puts('error')
      end


    elsif flag == '7'
      @user.update_attributes(:phone => [params[:user][:phone],params[:user][:policy_phone]],:web_url => params[:user][:web_url])
      if @user.save
        render json: @user
      else
        puts('error')
      end

    elsif flag == '5_mobile'
      @user.about_me = [params[:user][:about_me],params[:user][:policy_about_me]]
      @user.favorite = [params[:user][:favorite],params[:user][:policy_favorite]]

      if @user.save
        render json: @user
      else
        puts('error')
      end

    end

  end

  def get_user
    @user = User.find(current_user.id)
    render :json => @user
  end

  def destroy

  end

  def unique_email
    user = User.where(email: params[:email]).first
    if user
      render :text => 'false'
    else
      render :text => 'true'
    end
  end

  def find_friend_oauth_provider
    #friends = Friend.where("list.uid" => "50b58e67ad901878f7000004").update_all("$addToSet" => { "list.$.50b58e67ad901878f7000004" => {"avatar" => "\/img\/mquy.jpg"} })
    #friends.each do | friend |
    #friend.update_attributes(demo: "Sdfsdf")
    #end
    friends = User.where({"user_providers.name" => params[:provider], "user_providers.uid" => {'$in' => params[:friends]}}).only("_id", "user_providers.uid", "user_providers.name", "username")
    results = {}
    friends.each do |friend|
      friend["user_providers"].each do |provider|
        if provider['name'] == params[:provider]
          results[provider['uid']] = {id: friend.id, username: friend.username}
        end
      end
    end
    render json: {:users => results, :provider => params[:provider]}
  end

  def friends

=begin
    if(params[:oauth_token] && params[:oauth_verifier])

      @access_token = session[:request_token].get_access_token(:oauth_verifier => params[:oauth_verifier])
      @response = MultiJson.decode(@access_token.get("http://social.yahooapis.com/v1/user/#{@access_token.params[:xoauth_yahoo_guid]}/contacts?format=json").body)

      puts "http://social.yahooapis.com/v1/user/#{@access_token.params[:xoauth_yahoo_guid]}/contacts?format=json"
      return
    end
    @callback_url = "http://localhost:3000/users/friends"
    @consumer = OAuth::Consumer.new('dj0yJmk9clB2MXZudVNKTklZJmQ9WVdrOWNHUllVbGxhTm5FbWNHbzlNVEk0TkRrd05USTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD0xNw--', '22a91739bf69ff0da4b8ce9788f8bc0f23890ab7',
                                    {
                                        :site => 'https://api.login.yahoo.com',
                                        :request_token_path => '/oauth/v2/get_request_token',
                                        :access_token_path => '/oauth/v2/get_token',
                                        :authorize_path => '/oauth/v2/request_auth',
                                        :signature_method => 'HMAC-SHA1',
                                        :oauth_version => '1.0'
                                    })

    @request_token = @consumer.get_request_token(
        {:oauth_callback => @callback_url}
    )
    session[:request_token] = @request_token
    redirect_to @request_token.authorize_url(:oauth_callback => @callback_url)
=end
  end

  def friend_action
    type = params[:type]
    id = params[:id]
    status = 'ok'
    if (type == 'invite')
      result = self.invite_friend(id, params[:provider])
      if (result == false)
        status = 'error'
      end
    else
      User.friend_action(current_user['_id'], id, type)
    end
    render json: {status: status}
  end

  def invite_friend(id, provider)
    pro = current_user.user_providers.where({name: provider, uid: id}).first
    if pro.nil?
      return false
    end

    time = (Time.now.to_f * 1000).to_int
    link = 'http://127.0.0.1:3000/users/invite?uid=' + current_user._id + '&from='+provider+'&time=' + time.to_s

    invi = current_user.user_invitations.where({provider: provider, uid: id})
    if (invi.count == 0)
      current_user.user_invitations.push(UserProvider.new(provider: provider, uid: id, time: time))
    else
      invi.first.update_attributes(time: time)
    end

    if (provider == 'facebook')
      @graph = Koala::Facebook::API.new(pro.token)
      @graph.put_wall_post("Hey, Welcome to the Web Application!!!!", {
          "link" => link}, id)
    elsif (provider == 'google_oauth2')
      smtp = Net::SMTP.new('smtp.gmail.com', 587)
      smtp.enable_starttls_auto
      smtp.start('gmail.com', current_user.email, pro.token, :xoauth2)
      smtp.send_mail(User_Mailer.invite({subject: "Invitation to Deekau", link: link, to: id}).to_s, current_user.email, id)
      smtp.finish
    elsif (provider == 'linkedin')
      client = LinkedIn::Client.new('1tzzmg3qvkn4', 'cfVqDKObuf0ww4Zq')
      client.authorize_from_access(pro.token, pro.secret)
      client.send_message('Invitation to Deekau', link, [id])
    elsif (provider == 'foursquare')
      User_Mailer.invite({subject: "Invitation to Deekau", link: link, to: id}).deliver
    end
    return true
  end

  def invite
    uri = URI.parse(request.url)
    params = CGI::parse(uri.query)
    if (params['uid'] != nil && params['from'] != nil && params['time'] != nil)
      uid = params['uid'][0]
      from = params['from'][0]
      time = params['time'][0].to_i
      if (user_signed_in?)
        @user = User.where({"_id" => uid, "user_invitations.provider" => from, "user_invitations.time" => time}).first
        days = (Time.now.to_f - time.to_i / 1000) / 1.day
        if (@user != nil && days <= 15)
          if (session[:social_login_back] != nil)
            User.friend_action(current_user['_id'], uid, 'friend')
            rid = @user.user_invitations.where(time: time).first['uid']
            @friends = User.where({"user_invitations.uid" => rid, "user_invitations.provider" => from}).only(:_id, :images, :username)
            render 'users/invitation'
            User.where({"user_invitations.uid" => rid, "user_invitations.provider" => from}).update_all({"$pull" => {"user_invitations" => {"uid" => rid, "provider" => from}}})
          else
            session[:social_login_back] = request.url
            redirect_to '/users/auth/' + from
            return
          end
        else
          render 'users/invitation_expiration'
        end
        session.delete(:social_login_back)
      else
        session[:social_login_back] = request.url
        redirect_to '/users/auth/' + from
        return
      end
    end
  end
end
