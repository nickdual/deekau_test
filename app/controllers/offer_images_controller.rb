class OfferImagesController < ApplicationController
  def destroy
    @merchant = Merchant.find(params[:object_parent_id])
    @offer = @merchant.offers.find(params[:object_id])
    @offer.images.find(params[:id]).destroy
    if is_mobile_device?
      redirect_to @offer
    else
      render :text => 'ok'
    end
  end
end
