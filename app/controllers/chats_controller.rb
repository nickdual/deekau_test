class ChatsController < ApplicationController

  def find
    @friend = Friend.where(:uid => params[:id]).first
    render json: @friend
  end

  def load_more
    @user_id = params[:user_id]
    @friend_id = params[:friend_id]
    @total = params[:total].to_i
    @index = @total - (params[:index].to_i + 1)
    @compare = {}
    if (@user_id.to_s > @friend_id.to_s)
      @compare = {:uid1 => @user_id, :uid2 => @friend_id}
    else
      @compare = {:uid1 => @friend_id, :uid2 => @user_id}
    end
    if (@index <= @total && @index >= 0)
      @msgs = Chat.where(@compare).skip(@index).limit(1).first
    end
    render json: @msgs
  end

  def init
    @user_id = params[:user_id]
    @friend_id = params[:friend_id]
    @compare = {}
    if (@user_id.to_s > @friend_id.to_s)
      @compare = {:uid1 => @user_id, :uid2 => @friend_id}
    else
      @compare = {:uid1 => @friend_id, :uid2 => @user_id}
    end
    @count = Chat.only(:_id).where(@compare).count
    if (@count == 0)
      @msgs = Chat.where(@compare).first
    else
      @msgs = Chat.where(@compare).skip(@count.to_i - 1).limit(1).first
    end
    if (@msgs.blank?)
      @msgs = {}
    end
    @msgs['count'] = @count
    render json: @msgs
  end

  def add_recent_chat
    @user_id = params[:user_id]
    @friend_id = params[:friend_id]
    @result = Friend.where({:uid => @user_id}).first
    if (@result['recent_chat'] == nil)
      @result['recent_chat'] = []
    else
      if (@result['recent_chat'].length >= 5)
        @result['recent_chat'] = @result['recent_chat'][(@result['recent_chat'].length - 4)..@result['recent_chat'].length]
      end
    end
    if (@result['recent_chat'].index(@friend_id) == nil)
      @result['recent_chat'].push(@friend_id)
    end

    @result.upsert
    render json: @recent_chat
  end
end
