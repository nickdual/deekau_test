class ImagesController < ApplicationController
  def destroy
    @image = current_user.images.find(params[:id])
    @image.destroy
    if is_mobile_device?
      redirect_to user_profile_path
    else
      render :text => 'ok'
    end
  end
end
