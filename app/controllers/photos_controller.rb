

class PhotosController < ApplicationController

  def create
    @photo = Photo.new(params[:photo])
    @photo.save

    redirect_to @photo
  end

  def show
    @photo = Photo.find(params[:id])
  end

  def index

  end

  def upload

    now = Time.now
    time_now = []
    time_now.push(now.year , now.month ,now.day , now.hour , now.min  , now.sec )
    time_string = time_now.join('')
    file_name = time_string.to_s + '.jpg'
    file = File.join(Rails.root, 'public', 'uploads', file_name)

    File.open(file, 'wb') do |f|
      f.write request.raw_post
    end
    render :text => "ok"

  end

  #private


end
