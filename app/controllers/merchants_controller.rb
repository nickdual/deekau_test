require "simple_time_select"

class MerchantsController < ApplicationController
  include MerchantsHelper
  include ApplicationHelper
  #  load_and_authorize_resource
  
  def set_favourite
    @merchant = Merchant.find(params[:id])
    if params[:status].to_i == 0
      @merchant.favourite_count +=1
      @merchant.favourite_voters += current_user.id.to_s.to_a
      @merchant.save
      render :json => 'vote'.to_json
    else
      @merchant.favourite_count -=1
      @merchant.favourite_voters -= current_user.id.to_s.to_a
      @merchant.save
      render :json => 'unvote'.to_json
    end
    
  end
  
  def get_image
    @images = []
    @merchant = Merchant.find(params[:object_id])
    if !params[:files].nil?
      params[:files].each do |file|
        @images << @merchant.images.create(:url=>file)
      end
    end
    if !params[:object_id].nil? && !params[:image].nil?
      @images << @merchant.images.create(:url=>params[:image])
    end

    if params[:files].nil?
      @images = @merchant.images
    end


    if is_mobile_device?
      redirect_to @merchant
    else
      respond_to do |format|
        format.html # index.html.erbb
        format.json { render json: @images }
      end
    end
  end
  
  # GET /merchants
  # GET /merchants.json
  def index
    @merchants = Merchant.limit(80000)
    @categories = Category.where(tags: nil).to_a
    @categories.collect! {|item| {:label => item.category_languages[0].name, :id =>item._id }}
    respond_to do |format|
      format.html # index22.html.erb
      format.json { render :json => @merchants }
    end
  end

  # GET /merchants/1
  # GET /merchants/1.json
  def show
    @merchant = Merchant.find(params[:id])
    #@map_center = "#{@merchant.lat},#{@merchant.long}"
    @map_zoom = 15
    #    if @merchant.another_loc_s != ''
    #      @another_address = "#{@merchant.another_loc.first},#{@merchant.another_loc.last}"
    #    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @merchant }
    end
  end

  # GET /merchants/new
  # GET /merchants/new.json
  def new
    @map_center = '10.75000000,106.66667000' # @map_center not nil
    if current_city
      @map_center = "20,20"#"#{current_city.loc.first},#{current_city.loc.last}"
    end
    @merchant = Merchant.new
    @merchant.addresses.build
    @map_zoom = 15
    @tags = Category.all(conditions: { name: ""})
    respond_to do |format|
      format.html # new.html.erb
      format.mobile
      format.json { render json: @merchant }
    end
  end

  # GET /merchants/1/edit
  def edit
    @merchant = Merchant.find(params[:id])
    @map_zoom = 15
    params[:city_name] = @merchant.city ? @merchant.city.name : ''
    @map_center = "#{@merchant.lat},#{@merchant.long}"
    #    if !@merchant.another_loc_s.blank?
    #      @another_address = "#{@merchant.another_loc.first},#{@merchant.another_loc.last}"
    #    end
  end

  # POST /merchants
  # POST /merchants.json
  def create
    params[:merchant][:coordinates][0] =  params[:merchant][:coordinates][0].to_f;
    params[:merchant][:coordinates][1] =  params[:merchant][:coordinates][1].to_f;
    params[:merchant][:user_id] = current_user.id
    @merchant = Merchant.new(params[:merchant])
    respond_to do |format|
      if @merchant.save
        #        if current_user.role?('Super User') 
        #          current_user.update_attribute("merchant_id" ,@merchant.id)
        #        else
        #          current_user.update_attributes(:merchant_id => @merchant.id, :user_role_id => UserRole.first(conditions: { name: "Merchant" }).id)
        #        end
        format.html { redirect_to merchants_path(@merchant), notice: t("merchant.create") }
        format.mobile { redirect_to merchants_path(@merchant), notice: t("merchant.create") }
        format.json { render json: @merchant, status: :created, location: @merchant }
      else
        @tags = Category.all(conditions: { name: ""})
        @map_center = '10.75000000,106.66667000' # @map_center not nil
        @map_zoom = 15
        format.html { render action: "new" }
        format.mobile { render action: "new" }
        format.json { render json: @merchant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /merchants/1
  # PUT /merchants/1.json
  def update
    #    setting_params(params)
    @merchant = Merchant.find(params[:id])
    params[:merchant][:updated_at] = DateTime.current
    respond_to do |format|
      if @merchant.update_attributes(params[:merchant])
        format.html { redirect_to @merchant, notice: t("merchant.update") }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @merchant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchants/1
  # DELETE /merchants/1.json
  def destroy
    @merchant = Merchant.find(params[:id])
    @merchant.destroy

    respond_to do |format|
      format.html { redirect_to offers_url }
      format.json { head :no_content }
    end
  end
  def get_offers_quota
    if current_user
      @merchant = Merchant.where(user_id: current_user.id).first
      @offer = Offer.where("merchant_id" => @merchant.id)
      render :json => @offer
    else
      render :json => nil
    end
  end
  #  private
  #  def setting_params(params)
  #    params[:merchant][:coordinates] = params[:merchant][:coordinates_s].squish.split(',')
  #    params[:merchant][:another_coordinates] = params[:merchant][:another_coordinates_s].squish.split(',')
  #    params[:merchant][:start_open_time] = get_date(params[:start_open_time])
  #    params[:merchant][:end_open_time] = get_date(params[:end_open_time])
  #  end
end
