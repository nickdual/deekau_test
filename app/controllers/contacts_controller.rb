# gem install oauth2
require 'oauth2'
require 'net/http'
require 'net/https'
require 'uri'
require 'json'
class ContactsController < ApplicationController
  # To change this template use File | Settings | File Templates.
  def find_friend_google
    @token = current_user.google_token
    @contact = import_google_contacts(@token).to_json
    puts @contact
    puts @contact.version
    respond_to do |format|
      format.html # index22.html.erb
      format.json { render json: @contact }
    end
  end
  def google_auth2
    puts request.env["omniauth.auth"]
    User.update_google_token(request.env["omniauth.auth"],current_user)
    @token = request.env["omniauth.auth"]["credentials"]["token"]
    puts  @token
    import_google_contacts(@token)
    render :text => "OK"
  end

  private
  def import_google_contacts(token)
    # GET http://www.google.com/m8/feeds/contacts/default/base
    uri = URI.parse('https://www.google.com')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    #path = '/m8/feeds/contacts/default/full?max-results=10000'
    #headers = {'Authorization' => 'AuthSub token="ya29.AHES6ZQ5yZ551USUWqhYRvxidjCEyjNl0ErEszGYLkuB-w"','GData-Version' => '3.0'}
    data = http.get("https://www.google.com/m8/feeds/contacts/default/full/?access_token=#{token}&alt=json&max-results=10000")
    # extract the name and email address from the response data
    data.body
  end
end