class User_Mailer < ActionMailer::Base
  default :from => "sugiacupit@gmail.com",
          "Reply-To" => "reply-to@deekau.com"

  def invite(email)
    @content = email
    mail(:subject => @content[:subject], :to => @content[:to], :template_path => "users", :template_name => "invitation_email")
  end
end
