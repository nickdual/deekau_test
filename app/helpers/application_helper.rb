module ApplicationHelper
 $OFFER_IMG_NULL = 'http://files.list.co.uk/images/2011/12/19/mg-3394purslane-LST092298-0002t.jpg'
 
  
  def get_name_resource(object) #Dieu :D
    return object.class.name.pluralize.downcase
  end
  
  
  def check_favourite(object) # Dieu :D
    return 1 if !current_user.blank? && !object.favourite_voters.blank? && object.favourite_voters.include?(current_user.id.to_s)
    return 0
  end
  
  
  def get_favourite_image(object) # Dieu :D
    return "/assets/icon_categories/vote_star.jpeg" if check_favourite(object) == 1
    return "/assets/icon_categories/unvote_star.jpeg"
  end
  
  
  
  def get_offer(arr_merchant,offset,limit)  # Dieu :D
    # arr_merchant chua nhieu` merchant, moi merchant co' 1 array chua 1 hoac nhieu` offer
    offers = []
    arr_merchant.each do |arr_offer|
      # kiem tra offset co nam trong merchant hay ko, neu ko thi` bo wa va` giam bien offset
      # vi` offset chay tu` 0 nen >= 
      if offset >= arr_offer.count  # neu offset = 3 , ma` arr_offer co' 3  thi` se ko tru` offer , nhay vao` vong lap
        offset = offset - arr_offer.count
      else
        arr_offer.each do |offer|
          if offset > 0
            offset = offset -1
          else
            if limit != 0
              offer.merchant_id = offer.merchant.id.to_s
              offer.status = check_favourite(offer)
              offers << offer
              limit = limit - 1
            else
              break
            end
          end
        end
        
        break if limit == 0  # thoat neu da lay du offer
      end
      
    end
    return offers
  end
  def get_object_parent_id(object)  # Dieu :D
    class_name = object.class.name
    if class_name == 'Offer'
      return object.merchant.id
    end
  end

  def file_upload_path(class_name)  # Dieu :D
    if class_name == 'User'
      return user_profile_path
    elsif class_name == 'Merchant'
      return merchant_get_image_path
    else
      return offer_get_image_path
    end
  end

  def name_object_delete_file(class_name)  # Dieu :D
    if class_name == 'User'
      return 'images'
    elsif class_name == 'Merchant'
      return 'merchant_images'
    else
      return 'offer_images'
    end
  end
  
# Get default url when img is null
  def get_image(object_img,img_default)   # Dieu :D
    puts '========================================='
    puts img_default
    if object_img.blank?
      return img_default
    else
      return object_img.url
    end
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  
  def time_select_tag(name,selected,style)
    times = []
    (00..23).each do |i|
      (0..1).each do |inc| 
        times << Time.parse("#{i}:#{inc * 30}") 
      end
    end
    return select_tag name, options_for_select(times.map{|t| [t.strftime('%I:%M %p'),t.strftime('%H:%M')]},selected),"data-role" => "none","class" => style
  end

  def check_num(text)   # Dieu :D
    return true if text.to_i.to_s == text
    return false
  end
  
end
