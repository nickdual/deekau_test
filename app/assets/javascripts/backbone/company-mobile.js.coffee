#= require_self
# require ./common
#= require_tree ./mobile/models
#= require_tree ./mobile/templates
#= require_tree ./mobile/views
#= require_tree ./mobile/routers
window.Company =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  init: ->
    if (window.user != undefined)
      window.chat = new Company.Views.ChatIndex;
    new Company.Routers.Home()
    new Company.Routers.Users()

$(document).ready ->
  Company.init()
  Backbone.history.start({pushState: true, silent: false})
