class Company.Views.HomeIndex extends Backbone.View
  el: 'body'

  events:
    'click .mnavi-header > .mi-justify': 'snShowClick'
    'click .ms-block': 'snHideClick'
    'click .mvmb-item': 'snMenuItemClick'

  initialize: ->
    _.bindAll(this, 'snShowClick', 'snMenuItemClick')
    this.$el.find('.mi-goback').change_screen({effect: 'slide-out'})
    this.$el.find('.mi-close').change_screen({effect: 'slide-out', remove: true})

  snShowClick: (event) ->
    tab = null
    _.each(this.$el.find('.mt-screen'), (e) ->
      if($(e).css('display') != 'none')
        tab = $(e)
    )
    if (tab != null)
      menu = this.$el.find('#mn-menu')
      if (tab.css('left') == '0px')
        $('body').append('<div class="ms-block mfullscreen"></div>')
        menu.css('right', '-256px');
        tab.stop().animate({'left': '-256px'}, 400);
        menu.stop().animate({'right': '0px'}, 400);
        $('body').children('.ms-block').stop().animate({'left': '-256px'}, 400);
      else
        tab.stop().animate({'left': '0px'}, 400);
        menu.stop().animate({'right': '-256px'}, 400, () ->
          menu.css('right', '-260px');
          $('body').children('.ms-block').remove()
        );

  snHideClick: (event) ->
    tab = null
    _.each(this.$el.find('.mt-screen'), (e) ->
      if($(e).css('display') != 'none')
        tab = $(e)
    )
    if (tab != null)
      menu = this.$el.find('#mn-menu')
      if (tab.css('left') != '0px')
        tab.stop().animate({'left': '0px'}, 400);
        $('body').children('.ms-block').stop().animate({'left': '0px'}, 400);
        menu.stop().animate({'right': '-256px'}, 400, () ->
          menu.css('right', '-260px');
          $('body').children('.ms-block').remove()
        );

  snMenuItemClick: (event) ->
    $(event.currentTarget).closest('.mvm-body').find('.mvmb-inset').removeClass('mvmb-inset').find('.mvmbi-select').remove()
    $(event.currentTarget).removeClass('mvmb').addClass('mvmb-inset')
    $(event.currentTarget).prepend('<div class="mvmbi-select"></div>')