class Company.Views.ChatIndex extends Backbone.View
  template_tab: JST['backbone/mobile/templates/chat/chat_tab']
  template_msg: JST['backbone/mobile/templates/chat/chat_msg']
  template_friend: JST['backbone/mobile/templates/chat/chat_friend']
  template_invi: JST['backbone/mobile/templates/chat/invite_item']
  template_noti: JST['backbone/mobile/templates/chat/notification']
  template_setting: JST['backbone/mobile/templates/chat/chat_setting']

  el: '#mts-chat'
  el_csbody: ''

  friends: {}
  recent_chat: []
  user_online: {}
  groupsmiles: ''
  smiles: {}
  rooms: {}
  unreadmsg: {}
  status: 1

  events:
    'click .mtsbh-content': 'csgroupHeaderClick'
    'click #mts-chatsystem .mnavi-footer > div': 'csfNaviClick'
    'click #mtsb-currentchat > .mtsb-body > .mtsb-friend a': 'csbFriendCloseClick'
    'focusin .mtsc-footer textarea': 'ctfInputFocusin'
    'keydown .mtsc-footer textarea': 'ctfInputKeypress'
    'click .mtsc-footer > .mtscf-input > button': 'ctfSendMessageClick'
    'click .mtscf-tab > div': 'ctfNaviClick'
    'click .smileicon': 'ctfSmileClick'
    'click .mts-chattab .mi-plus': 'cthAddFriendClick'
    'click .mdbf-invite': 'ctbInviFriendClick'
    'click .mtsf-loadmore': 'ctbLoadMoreClick'
    'mouseenter #mtsb-currentchat > .mtsb-body > .mtsb-friend': 'cscFriendMouseEnter'
    'mouseleave #mtsb-currentchat > .mtsb-body > .mtsb-friend': 'cscFriendMouseLeave'
    'click #mtsb-currentchat > .mtsb-body > .mtsb-friend a': 'cscFriendCloseClick'



  initialize: ->
    _.bindAll(this, 'ctfNaviClick', 'beforeChangeScreen', 'finishChangeScreen', 'cbfetch',
                    'sk_cbConnect', 'ctfInputKeypress', 'ctbLoadMoreClick',
                    'initInviDialog', 'inviDialogSubmit', 'sk_switchtoRoom', 'sk_userinRoom',
                    'showChatTabNoti', 'initSettingDialog', 'settingDialogSubmit')

    this.init()
    new Company.Models.Friend({uid: window.user['id']}).fetch({success: this.cbfetch})

  cbfetch: (model, response) ->
    @status = 1
    @friends = {}
    @recent_chat = []
    @user_online = {}
    @rooms = {}
    @unreadmsg = {}
    window.iosocket.init_Connect(this);
    if (model.get('friend_detail') != null)
      _.each(model.get('friend_detail'), (value, key) ->
        @friends[value['uid']] = value
      , this)
    @recent_chat = model.get('recent_chat')
    if (@recent_chat == null)
      @recent_chat = []

    window.friend = @friends

  init: () ->
    @el_csbody = this.$el.children('#mts-chatsystem').children('.mts-body')

    this.$el.find('.mtsb-body > .mtsb-friend').change_screen({begin: this.beforeChangeScreen, finish: this.finishChangeScreen})
    this.$el.find('.mvhdn-msg').change_screen({begin: this.beforeChangeScreen, finish: this.finishChangeScreen})
    this.$el.find('.mi-plus').show_dialog({
      header_title: 'Invite Friends'
      id: 'md-invitefriend'
      parent: this.el
      btn_submit: this.inviDialogSubmit
      begin: this.initInviDialog
      finish: () ->
    })
    this.$el.find('#csvf-setting').show_dialog({
      header_title: 'Chat Setting'
      id: 'md-chatsetting'
      parent: this.el
      effect_out: 'blur'
      btn_submit: this.settingDialogSubmit
      begin: this.initSettingDialog
      finish: () ->
    })
    this.emotionLoad()

    tab = @el_csbody
    tab.children().addClass('hide')
    tab.children('#mtsb-recentchat').removeClass('hide').children('.mtsb-header').removeClass('hide')
    tab.children('#mtsb-online').removeClass('hide').children('.mtsb-header').removeClass('hide')
    tab.children('#mtsb-offline').removeClass('hide').children('.mtsb-header').removeClass('hide')

  beforeChangeScreen: (event) ->
    id = $(event).attr('ref').substr(7)
    if (@friends[id] == null)
      return;
    tab = this.$el.children('#mts-chattabs')
    if (tab.children('#mtcid-'+id).length == 0)
      if ($(event).find('span').length == 0)
        status = 'invisible'
      else
        status = $(event).find('span').attr('class').substr(3)
      this.initChatTabContent({id: id, username: @friends[id]['username'], tab_id: id, status: status, avatar: @friends[id]['avatar']})

    tab = tab.children('#mtcid-'+id)
    tab_menu = tab.children('.mnavi-header').find('.mnhb-menu')
    tab_menu.children('a').children('span').empty()
    tab_menu.children('ul').empty()
    @el_csbody.find('div[ref="#mtcid-'+ id+'"]').removeClass('msg-unread')
    delete @unreadmsg[id]

  finishChangeScreen: (event) ->
    this.$el.children('#mts-chattabs').children('#mtcid-'+$(event).attr('ref').substr(7)).children('.mtsc-footer').find('.mtscf-input > textarea').focus()

  initChatTabContent: (user) ->
    this.$el.children('#mts-chattabs').append(@template_tab({id: user['tab_id'], username: user['username'], status: user['status']}));
    this.csAddNewFriendItem('#mtsb-currentchat', {id: user['id'], tab_id: user['tab_id'], username: user['username'], avatar: user['avatar'], status: user['status']})
    this.saveTabToCookie(user['tab_id'], 1)

    obj = this
    $.ajax({
      type: 'POST',
      url: '/chats/init',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {user_id: window.user['id'], friend_id: user['id']},
      success: (data) ->
        tab = obj.$el.children('#mts-chattabs').children('#mtcid-' + user['tab_id'])
        if (tab.length != 0 && data['count'] > 0)
          obj.loadmoreAdd(data, tab)
          tab.children('.mts-body').prepend('<div class="mtsf-loadmore" index="1" total="'+data['count']+'">load more</div>');
    })

    $.ajax({
      type: 'POST',
      url: '/chats/add_recent_chat',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {user_id: window.user['id'], friend_id: user['id']},
      success: (data) ->
        #console.log(data)
      error: (response) ->
        #console.log(response)
    })

  initInviDialog: (event) ->
    tab = $(event.currentTarget).closest('.mt-screen')
    tab_id = tab.attr('id').substr(6)
    type = tab.attr('type')
    _.each(@user_online, (value, key) ->
      if ((type == 'person' && tab_id != key) || (type =='group' && @rooms[tab_id][key] == undefined))
        $(event.dialog).append(@template_invi({id: key, avatar: @friends[key]['avatar'], username: @friends[key]['username']}))
    , this)

  initSettingDialog: (event) ->
    $(event.dialog).append(@template_setting())

  #listen event from socket
  sk_cbConnect: (users) ->
    @user_online = users
    if (@user_online == null)
      @user_online = {}

    this.put_RecentChat()
    this.put_Online()
    this.put_Offline()

    tabs = JSON.parse($.docCookies.getItem('deekau_chat_tabs'))
    _.each(tabs, (value, key) ->
      if (@friends[key] != undefined)
        this.initChatTabContent({id: key, tab_id: key, username: @friends[key]['username'], status: this.getStatusFromID(key), avatar: @friends[key]['avatar']})
    , this)

  sk_userConnect: (user) ->
    if (user['id'] == window.user['id'])
      new Company.Models.Friend({uid: window.user['id']}).fetch({success: this.cbfetch})
    else
      this.uiUserConnectChange(user, user['status'])

  sk_userDisconnect: (user) ->
    if (user['id'] == window.user['id'])
      @status = 4
      window.iosocket.disconnect()
      this.emptyView()
    else
      this.uiUserConnectChange(user, 3)

  sk_receiveMessage: (msg) ->
    if (this.$el.children('#mts-chattabs').children('#mtcid-' + msg['tab_id']).length == 0)
      this.initChatTabContent({id: msg['id'], username: msg['username'], tab_id: msg['tab_id'], status: this.toStatus(msg['status']), avatar: msg['avatar']})
    else
      this.addNewMessage({date: new Date(msg['time']), 'tab_id': msg['tab_id'], username: msg['username'], avatar: msg['avatar'], type: msg['id'], content: msg['content'], append: 'last'})
    this.showChatTabNoti(msg)

  sk_receiveStatus: (msg) ->
    if (msg['id'] == window.user['id'])
      @status = msg['status']
    else
      this.uiUserConnectChange(msg, msg['status'])

  sk_switchtoRoom: (msg) ->
    if (msg['status'] == 'ok')
      tab = this.$el.children('#mts-chattabs').children('#mtcid-' + msg['id'])
      if (tab.length != 0)
        tab.attr('id', 'mtcid-' + msg['groupid'])
        tab.find('.mtsf-loadmore').remove()
        tab.attr('type', 'group')
      tab_friend = this.$el.find('#mts-chatsystem > .mts-body').children('#mtsb-currentchat').find('div[ref="#mtcid-'+ msg['id']+'"]').remove()

  sk_userinRoom: (msg) ->
    if (@rooms[msg['id']] == undefined)
      @rooms[msg['id']] = {}
    title = ''
    group_title = ''
    joinRoom = ''
    _.each(msg['users'], (value, key) ->
      if (title == '')
        group_title = "<span>"+value['username']+"</span>"
        title = value['username']
      else
        group_title += ', ' + "<span>"+value['username']+"</span>"
        title += ',' + value['username']
      if (@rooms[msg['id']][key] == undefined)
        @rooms[msg['id']][key] = msg['users'][key]
        if (key != window.user['id'])
          joinRoom += '<div class="mtsf-alert">'+msg['users'][key]['username']+' join in room</div>'
    , this)

    tab = this.$el.children('#mts-chattabs').children('#mtcid-' + msg['id'])
    if (tab.length == 0)
      html = @template_tab({id: msg['id'], username: msg['users'], type: 'group'})
      this.$el.children('#mts-chattabs').append(html);
      tab = this.$el.children('#mts-chattabs').children('#mtcid-' + msg['id'])
    else
      tab_title = tab.find('.mtsch-list')
      tab_title.empty()
      tab_title.append(group_title)
    tab.children('.mts-body').append(joinRoom)
    this.csAddNewFriendItem('#mtsb-currentchat', {id: msg['id'], tab_id: msg['id'], username: title, avatar: '/img/mquy.jpg'})

  sk_leaveRoom: (msg) ->
    if (msg['id'] == window.user['id'])
      this.removeTabChat(msg['roomid'])
    else
      tab = this.$el.children('#mts-chattabs').children('#mtcid-' + msg['roomid'])
      tab_body = tab.children('.mts-body');
      html_msg = '<div class="mtsf-alert">'+msg['username']+' left room</div>'
      tab_body.append(html_msg)
      title = ''
      group_title = ''
      delete @rooms[msg['roomid']][msg['id']]
      _.each(@rooms[msg['roomid']], (value, key) ->
        if (title == '')
          group_title = "<span>"+value['username']+"</span>"
          title = value['username']
        else
          group_title += ', ' + "<span>"+value['username']+"</span>"
          title += ',' + value['username']
      )
      @el_csbody.find('div[ref="#mtcid-'+ msg['roomid']+'"]').find('.mtsbf-title > .clip-content').text(title)
      this.$el.children('#mts-chattabs').children('#mtcid-' + msg['roomid']).find('.mtsch-list').empty().append(group_title)

  #listen event from dom
  cscFriendMouseEnter: (event) ->
    $(event.currentTarget).children('div').append('<a class="mi-close micon" style="background:red; right: 12px;position: absolute;top: 16px;"></a>')

  cscFriendMouseLeave: (event) ->
    $(event.currentTarget).children('div').children('a').remove()

  cscFriendCloseClick: (event) ->
    id = $(event.currentTarget).closest('.mtsb-friend').attr('ref').substr(7)
    this.removeTabChat(id)
    if (id.indexOf('_') != -1)
      window.iosocket.sendleaveRoom({roomid: id})

    event.stopPropagation()
    return false;

  csgroupHeaderClick: (event) ->
    tab = $(event.currentTarget).closest('.mtsb-tab')
    if (tab.children('.mtsb-body').css('display') == 'none')
      tab.children('.mtsb-body').removeClass('hide')
      $(event.currentTarget).children('.mtsbh-arrow').removeClass('mtsbh-arrowright').addClass('mtsbh-arrowdown')
    else
      tab.children('.mtsb-body').addClass('hide')
      $(event.currentTarget).children('.mtsbh-arrow').removeClass('mtsbh-arrowdown').addClass('mtsbh-arrowright')

  ctfInputFocusin: (event) ->
    tab = $(event.currentTarget).closest('.mt-screen')
    footer_tab = $(event.currentTarget).closest('.mtsc-footer')
    footer_height = footer_tab.outerHeight(false)
    footer_tab.stop().animate({'margin-bottom':  '0px'}, 250)
    tab.animate({'padding-bottom': footer_height + 'px'}, 250)

  ctfInputKeypress: (event) ->
    if (event.which == 27)
      tab = $(event.currentTarget).closest('.mt-screen')
      footer_tab = $(event.currentTarget).closest('.mtsc-footer')
      footer_height = footer_tab.outerHeight(false)
      if (footer_tab.css('margin-bottom') == '0px')
        footer_tab.stop().animate({'margin-bottom': (-footer_height + 40) + 'px'}, 250)
        tab.stop().animate({'padding-bottom': '40px'}, 250)
      else
        footer_tab.stop().animate({'margin-bottom': '0px'}, 250)
        tab.stop().animate({'padding-bottom': footer_height + 'px'}, 250)
    else if (event.which == 13)
      if (!event.shiftKey)
        tab = $(event.currentTarget).closest('.mt-screen')
        this.sendMessageAction(tab)
        return false;

  ctfSendMessageClick: (event) ->
    tab = $(event.currentTarget).closest('.mt-screen')
    this.sendMessageAction(tab)

  ctfNaviClick: (event) ->
    type = $(event.currentTarget).attr('type')
    tab = $(event.currentTarget).closest('.mt-screen')
    footer_tab = $(event.currentTarget).closest('.mtsc-footer')
    if (type == 'emotion')
      if (footer_tab.children().length == 2)
        footer_tab.append('<div class="mtsfg-smiles">'+@groupsmiles+'</div>')
        tab.animate({'padding-bottom': (footer_tab.height() + 3) + 'px'}, 250)
      else
        footer_tab.find('.mtsfg-smiles').remove()
        tab.animate({'padding-bottom': (footer_tab.height() + 3) + 'px'}, 250)

  ctfSmileClick: (event) ->
    tx = $(event.currentTarget).closest('.mtsc-footer').children('.mtscf-input').children('textarea')
    tx.focus().val(tx.val() + $(event.currentTarget).attr('symbol'));

  cthAddFriendClick: (event) ->

  ctbInviFriendClick: (event) ->
    if ($(event.currentTarget).attr('class').indexOf('mdbfi-select') != -1)
      $(event.currentTarget).removeClass('mdbfi-select')
    else
      $(event.currentTarget).addClass('mdbfi-select')

  csfNaviClick: (event) ->
    tab = @el_csbody
    type = $(event.currentTarget).attr('type')
    if (type == 'chats')
      tab.children().addClass('hide')
      tab.children('#mtsb-currentchat').removeClass('hide').children('.mtsb-header').addClass('hide')
    else if (type == 'online')
      tab.children().addClass('hide')
      tab.children('#mtsb-online').removeClass('hide').children('.mtsb-header').addClass('hide')
    else if (type == 'all')
      tab.children().addClass('hide')
      tab.children('#mtsb-recentchat').removeClass('hide').children('.mtsb-header').removeClass('hide')
      tab.children('#mtsb-online').removeClass('hide').children('.mtsb-header').removeClass('hide')
      tab.children('#mtsb-offline').removeClass('hide').children('.mtsb-header').removeClass('hide')

    else if (type == 'setting')
      x = 1

  csbFriendCloseClick: (event) ->
    return false;

  ctbLoadMoreClick: (event) ->
    tab = $(event.currentTarget).closest('.mt-screen')
    friend_id = tab.attr('id').substr(6)
    index = $(event.currentTarget).attr('index')
    total = $(event.currentTarget).attr('total')
    if (!(((parseFloat(index) == parseInt(index)) && !isNaN(index)) && ((parseFloat(total) == parseInt(total)) && !isNaN(total))))
      return
    obj = this;
    $(event.currentTarget).remove()
    $.ajax({
      type: 'POST',
      url: '/chats/load_more',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {user_id: window.user['id'], friend_id: friend_id, index: index, total: total},
      success: (data) ->
        if (data == null || data["log"].length == 0)
          return
        else
          obj.loadmoreAdd(data, tab)
          tab.children('.mts-body').prepend('<div class="mtsf-loadmore" index="'+(parseInt(index) + 1)+'" total="'+total+'">load more</div>');
      })

  #support function
  sendMessageAction: (tab) ->
    textarea = tab.children('.mtsc-footer').find('.mtscf-input > textarea')
    content = textarea.val()
    if (content == '')
      return;
    textarea.val('')
    id = tab.attr('id').substr(6)
    if (tab.attr('type') == 'person')
      if (@friends[id] != undefined)
        window.iosocket.sendMessage({id: id, username: @friends[id]['username'], content: content})
        this.addNewMessage({date: new Date(), 'tab_id': id, username: window.user['username'], avatar: window.user['avatar'], type: window.user['id'], content: content, append: 'last'})
    else
      if (@rooms[id] != undefined)
        window.iosocket.sendMessageGroup({id: id, content: content})

    body_tag = tab.children('.mts-body')[0]
    body_tag.scrollTop = body_tag.scrollHeight;
    return false;

  saveTabToCookie: (id, status) ->
    if (id.indexOf('_') != '-1')
      return
    ck_tabs = JSON.parse($.docCookies.getItem('deekau_chat_tabs'))
    if (ck_tabs == null)
      ck_tabs = {}
    ck_tabs[id] = status
    $.docCookies.setItem('deekau_chat_tabs', JSON.stringify(ck_tabs))

  removeTabFromCookie: (id) ->
    ck_tabs = JSON.parse($.docCookies.getItem('deekau_chat_tabs'))
    if (ck_tabs == null)
      ck_tabs = {}
    delete ck_tabs[id]
    $.docCookies.setItem('deekau_chat_tabs', JSON.stringify(ck_tabs))

  uiUserConnectChange: (user, type) ->
    type = parseInt(type)
    if (type == 1 || type == 2)
      @user_online[user['id']] = type;
      tab = @el_csbody
      #remove in offline tag
      tab.children('#mtsb-offline').find('div[ref="#mtcid-'+ user['id']+'"]').remove()
      #add in online tag if not
      this.csAddNewFriendItem('#mtsb-online', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']})
    else
      delete @user_online[user['id']]
      tab = @el_csbody
      #remove in online tag
      tab.children('#mtsb-online').find('div[ref="#mtcid-'+ user['id']+'"]').remove()
      #add in offline tag if not
      this.csAddNewFriendItem('#mtsb-offline', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']})

    #change status in recent if has
    this.csAddNewFriendItem('#mtsb-recentchat', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']}, false)

    #change status in current if has
    this.csAddNewFriendItem('#mtsb-currentchat', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']}, false)

    #change status in tab chat if has
    chat = this.$el.children('#mts-chattabs').children('#mtcid-'+ user['id'])
    if (chat.length != 0)
      chat.children('.mnavi-header').children('.mtsch-list').children('span').children('span').attr('class', 'cs-'+this.toStatus(type))

  removeTabChat: (id) ->
    delete @unreadmsg[id]
    delete @rooms[id]
    this.$el.children('#mts-chattabs').children('#mtcid-'+ id).remove()
    @el_csbody.children('#mtsb-currentchat').find('div[ref="#mtcid-'+ id+'"]').remove()
    this.removeTabFromCookie(id)

  showChatTabNoti: (item) ->
    body_tag = this.$el.children('#mts-chattabs').children('#mtcid-'+item['tab_id']).children('.mts-body:first')[0]
    body_tag.scrollTop = body_tag.scrollHeight;
    if (this.$el.children('#mts-chatsystem').css('display') != 'none')
      @el_csbody.find('div[ref="#mtcid-'+ item['tab_id']+'"]').addClass('msg-unread')
      if (@unreadmsg[item['tab_id']] == undefined)
        @unreadmsg[item['tab_id']] = 0
      else
        @unreadmsg[item['tab_id']] += 1
      return;

    tab_active = null;
    _.each(this.$el.children('#mts-chattabs').children(), (value, key) ->
      if ($(value).css('display') != 'none')
        tab_active = $(value)
    , this)
    if (tab_active == null)
      return;
    if (tab_active.attr('id').substr(6) != item['tab_id'])
      if (@unreadmsg[item['tab_id']] == undefined)
        @unreadmsg[item['tab_id']] = 0
      else
        @unreadmsg[item['tab_id']] += 1

      @el_csbody.find('div[ref="#mtcid-'+ item['id']+'"]').addClass('msg-unread')
      this.showUnReadMessage(tab_active, item)

  showUnReadMessage: (tab, msg) ->
    tab = tab.find('.mnavi-header > .mnhb-menu')
    tab.find('a > span').text($.json_length(@unreadmsg))
    tab = tab.find('.mvh-dropdown')
    if (tab.children('.msg-' +msg['id']).length == 0)
      tab.append(@template_noti({id: msg['id'], username: msg['username'], avatar: msg['avatar'], content: msg['content']}))
    else
      tab.children('.msg-' +msg['id']).children('div').children('div:last').html(msg['content'])

  csAddNewFriendItem: (id, item, iscreate) ->
    tab = this.$el.children('#mts-chatsystem').find(id).children('.mtsb-body')
    if (tab.find('div[ref="#mtcid-'+ item['tab_id']+'"]').length == 0)
      if (iscreate == undefined || iscreate == true)
        html = @template_friend({id: item['tab_id'], username: item['username'], avatar: item['avatar'], status: item['status']})
      tab.append(html)
    else
      tab_item = tab.find('div[ref="#mtcid-'+ item['tab_id']+'"]')
      tab_item.find('.mtsbf-title').children('div:first').text(item['username'])
      if (item['content'] != undefined)
        tab_item.find('.mtsbf-msg').text(item['content'])
      if (item['status'] == undefined || item['status'] == 'invisible')
        tab_item.find('span').remove()
      else
        if (tab_item.find('span').length == 0)
          tab_item.children('div').prepend('<span class="cs-'+item['status']+'" style="position: absolute; right: 12px; top: 22px;"></span>')
        else
          tab_item.find('span').attr('class', 'cs-'+item['status']+'');

  inviDialogSubmit: (event) ->
    tab = $(event.currentTarget).closest('.mt-screen')
    tab_id = tab.attr('id').substr(6)
    type = tab.attr('type')
    if (type == 'person')
      group_id = window.user['id'] + '_' +Date.now()
      window.iosocket.joinRoom({id: tab_id, username: @friends[tab_id]['username'], avatar: @friends[tab_id]['avatar'], roomid: group_id, type: 0})
      tab_id = group_id
    _.each($(event.dialog).children('.mdbfi-select'), (value, key) ->
      id = $(value).attr('id').substr(6)
      window.iosocket.joinRoom({id: id, username: @friends[id]['username'], avatar: @friends[id]['avatar'], roomid: tab_id})
    , this)

  settingDialogSubmit: (event) ->
    type = 0
    _.each($(event.dialog).children('#mds-status').find('input'), (value) ->
      if ($(value).is(':checked'))
        type = parseInt($(value).val())
    )
    if (type == 0)
      return
    if (type == 4)
      window.iosocket.disconnect()
      @status = 4
      this.emptyView()
    else
      if (@status == 4)
        new Company.Models.Friend({uid: window.user['id']}).fetch({success: this.cbfetch})
      else
        window.iosocket.sendStatus({status: type})

  getStatusFromID: (id) ->
    status = 'invisible'
    if (@user_online[id] != undefined)
      if (@user_online[id] == 1)
        status = 'available'
      else if (@user_online[id] == 2)
        status = 'busy'
    return status

  toStatus: (id) ->
    id = parseInt(id)
    if (id == 1)
      type = 'available'
    else if (id == 2)
      type = 'busy'
    else if (id == 3)
      type = 'invisible'

  put_RecentChat: ->
    tag_body = @el_csbody.children('#mtsb-recentchat').children('.mtsb-body')
    _.each(@recent_chat, (key, value) ->
      if (@friends[key] != undefined)
        html = @template_friend({id: key, username: @friends[key]['username'], status: this.getStatusFromID(key), avatar: @friends[key]['avatar']})
        tag_body.append(html);
    , this);

  put_Online: ->
    tag_body = @el_csbody.children('#mtsb-online').children('.mtsb-body')
    _.each(@user_online, (value, key) ->
      if (@friends[key] != undefined)
        html = @template_friend({id: key, username: @friends[key]['username'], status: this.getStatusFromID(key), avatar: @friends[key]['avatar']})
        tag_body.append(html);
    , this);

  put_Offline: ->
    tag_body = @el_csbody.children('#mtsb-offline').children('.mtsb-body')
    _.each(@friends, (value, key) ->
      if (@user_online[key] == undefined)
        html = @template_friend({id: key, username: @friends[key]['username'], avatar: @friends[key]['avatar']})
        tag_body.append(html);
    , this);

  addNewMessage: (msg, type) ->
    content = this.replaceEmotions(msg['content'])
    tab = this.$el.children('#mts-chattabs').children('#mtcid-'+msg['tab_id']).children('.mts-body');
    if (msg['append'] == 'last')
      finder = 'div:last'
    else
      finder = 'div:first'
    if (tab.children(finder).length == 0 || tab.children(finder).attr('class').indexOf(msg['type']) == -1)
      time = this.convertTwoDigital(msg['date'].getMinutes())
      rtime = msg['date'].getHours() - 12
      if (rtime > 0)
        time = this.convertTwoDigital(rtime)+ ':' + time + ' PM';
      else
        time = this.convertTwoDigital(msg['date'].getHours()) + ':'  + time + ' AM';
      if (msg['type'] == window.user['id'])
        html_msg = @template_msg({time: time, 'type': 'right', id: msg['type'], 'avatar': msg['avatar'], username: msg['username']})
      else
        html_msg = @template_msg({time: time, 'type': 'left', id: msg['type'], 'avatar': msg['avatar'], username: msg['username']})
      if (msg['append'] == 'last')
        tab.append(html_msg)
      else
        tab.prepend(html_msg)
    if (msg['append'] == 'last')
      tab.children(finder).children('.mtsbm-content').children('div').append('<div>' + content + '</div>');
    else
      tab.children(finder).children('.mtsbm-content').children('div').prepend('<div>' + content + '</div>');

  emptyView: ()->
    @user_online = {}
    @friends = {}
    @recent_chat = []
    @rooms = {}
    @unreadmsg = {}
    @el_csbody.children('#mtsb-currentchat').children('.mtsb-body').empty()
    @el_csbody.children('#mtsb-recentchat').children('.mtsb-body').empty()
    @el_csbody.children('#mtsb-online').children('.mtsb-body').empty()
    @el_csbody.children('#mtsb-offline').children('.mtsb-body').empty()
    this.$el.children('#mts-chattabs').empty()

  convertTwoDigital: (time)->
    if (time < 10)
      return '0' + time
    else
      return time

  replaceEmotions: (content) ->
    patterns = []
    metachars = /[[\]{}()*+?.\\|^$\-,&#\s]/g

    _.each(@smiles, (value, key) ->
      if (@smiles.hasOwnProperty(key))
        patterns.push('('+key.replace(metachars, "\\$&")+')')
    , this)
    smiles = @smiles
    content = content.replace(new RegExp(patterns.join('|'),'g'), (match) ->
      if (smiles[match] != 'undefined')
        return '<img src="'+smiles[match]['dynamic_image']+'"/>'
      else
        return match;
    );
    return content

  loadmoreAdd: (model, tab) ->
    log = model['log'].reverse()
    _.each(log, (value, key) ->
      type = value['sender']
      if (window.user['id'] == value['sender'])
        username = window.user['username']
        avatar = window.user['avatar']
      else
        username = @friends[value['sender']]['username']
        avatar = @friends[value['sender']]['avatar']
      this.addNewMessage({date: new Date(value['timestamp']), 'tab_id': tab.attr('id').substr(6), username: username, avatar: avatar, content: value['content'], type: type, append: 'first'})
    , this)
    date = model['date']
    date = date.split('T')[0].split('-')
    tab.children('.mts-body').prepend('<div class="mtsb-seperator"><div class="mtsbs-line"></div><div class="mtsbs-day">' + date[1] + '-' + date[2]+'</div></div>')

  emotionLoad: () ->
    @smiles = {
    ':) ': {'title': 'happy', 'static_image': '/img/smiles/1.png', 'dynamic_image': '/img/smiles/1.gif'},
    ':( ': {'title': 'sad', 'static_image': '/img/smiles/2.png', 'dynamic_image': '/img/smiles/2.gif'},
    ';) ': {'title': 'winking', 'static_image': '/img/smiles/3.png', 'dynamic_image': '/img/smiles/3.gif'},
    ':D ': {'title': 'big grin', 'static_image': '/img/smiles/4.png', 'dynamic_image': '/img/smiles/4.gif'},
    ';;) ': {'title': 'batting eyelashes', 'static_image': '/img/smiles/5.png', 'dynamic_image': '/img/smiles/5.gif'},
    '>:D< ': {'title': 'big hug', 'static_image': '/img/smiles/6.png', 'dynamic_image': '/img/smiles/6.gif'},
    ':-/ ': {'title': 'confused', 'static_image': '/img/smiles/7.png', 'dynamic_image': '/img/smiles/7.gif'},
    ':x ': {'title': 'love stuck', 'static_image': '/img/smiles/8.png', 'dynamic_image': '/img/smiles/8.gif'},
    ':"> ': {'title': 'blushing', 'static_image': '/img/smiles/9.png', 'dynamic_image': '/img/smiles/9.gif'},
    ':P ': {'title': 'tongue', 'static_image': '/img/smiles/10.png', 'dynamic_image': '/img/smiles/10.gif'},
    ':-* ': {'title': 'kiss', 'static_image': '/img/smiles/11.png', 'dynamic_image': '/img/smiles/11.gif'},
    '=(( ': {'title': 'broken heart', 'static_image': '/img/smiles/12.png', 'dynamic_image': '/img/smiles/12.gif'},
    ':o ': {'title': 'surprise', 'static_image': '/img/smiles/13.png', 'dynamic_image': '/img/smiles/13.gif'},
    'X( ': {'title': 'angry', 'static_image': '/img/smiles/14.png', 'dynamic_image': '/img/smiles/14.gif'},
    ':> ': {'title': 'smug', 'static_image': '/img/smiles/15.png', 'dynamic_image': '/img/smiles/15.gif'},
    'B-( ': {'title': 'cool', 'static_image': '/img/smiles/16.png', 'dynamic_image': '/img/smiles/16.gif'},
    ':-S ': {'title': 'worried', 'static_image': '/img/smiles/17.png', 'dynamic_image': '/img/smiles/17.gif'},
    '#:-S ': {'title': 'whew!', 'static_image': '/img/smiles/18.png', 'dynamic_image': '/img/smiles/18.gif'},
    '>:) ': {'title': 'devil', 'static_image': '/img/smiles/19.png', 'dynamic_image': '/img/smiles/19.gif'},
    ':(( ': {'title': 'crying', 'static_image': '/img/smiles/20.png', 'dynamic_image': '/img/smiles/20.gif'},
    ':)) ': {'title': 'laughing', 'static_image': '/img/smiles/21.png', 'dynamic_image': '/img/smiles/21.gif'},
    ':| ': {'title': 'straight face', 'static_image': '/img/smiles/22.png', 'dynamic_image': '/img/smiles/22.gif'},
    '/:) ': {'title': 'raised eyebrows', 'static_image': '/img/smiles/23.png', 'dynamic_image': '/img/smiles/23.gif'},
    '=)) ': {'title': 'rolling on the floor', 'static_image': '/img/smiles/24.png', 'dynamic_image': '/img/smiles/24.gif'},
    }
    _.each(@smiles, (value, key) ->
      @groupsmiles += '<div class="smileicon" title=\''+ value['title'] + '\' symbol=\'' + key + '\'><img src="' + value['static_image'] + '"/></div>'
    , this)

  lostconnect: (msg) ->

  reconnect_success: (msg) ->
    window.iosocket.init_Connect(this);

  reconnect_fail: (msg) ->
    @status = 4
    this.emptyView()