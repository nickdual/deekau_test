class Company.Views.UserEditContactMobile extends Backbone.View

  el: '.edit_contact_zone'
  template: JST['backbone/mobile/templates/users/edit_contact_mobile']

  render:->
    $(@el).html(@template())