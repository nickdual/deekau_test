class Company.Views.UserEditEducationMobile extends Backbone.View

  el: '.edit_edu_zone'
  template: JST['backbone/mobile/templates/users/edit_education_mobile']

  render:->
    $(@el).html(@template())