class Company.Views.UserEditBasicInfoMobile extends Backbone.View

  el: '.edit_basic_info_zone'
  template: JST['backbone/mobile/templates/users/edit_basic_info_mobile']

  render:->
    $(@el).html(@template())