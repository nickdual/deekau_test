class Company.Views.UserEditHighSchoolMobile extends Backbone.View

  el: '.edit_high_school_zone'
  template: JST['backbone/mobile/templates/users/edit_high_school_mobile']

  render:->
    $(@el).html(@template())