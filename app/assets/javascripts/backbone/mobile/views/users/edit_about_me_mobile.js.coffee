class Company.Views.UsersEditAboutMeMobile extends Backbone.View

  el: '.edit_about_me_zone'
  template: JST['backbone/mobile/templates/users/edit_about_me_mobile']

  render:->
    $(@el).html(@template())