class Company.Views.UserEditWorkMobile extends Backbone.View

  el: '.edit_work_zone'
  template: JST['backbone/mobile/templates/users/edit_work_mobile']

  render:->
    $(@el).html(@template())