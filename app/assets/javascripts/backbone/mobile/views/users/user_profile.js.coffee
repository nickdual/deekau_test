class Company.Views.UserProfileMobile extends Backbone.View

  el: '#User_profile_mobile'
  template: JST['backbone/mobile/templates/users/user_profile']

  events:
    "click .edit_basic_info_button":"showBasicInfoEdit"
    "click .edit_work_button":"showWorkEdit"
    "click .edit_university_button":"showCollegeUniversityEdit"
    "click .edit_highschool_button":"showHighSchoolEdit"
    "click .edit_about_me_button":"showAboutMeEdit"
    "click .edit_contact_button":"showContactEdit"
    "click .save_basic_info_button" :"saveBasicInfoEdit"
    "click .save_work_button" :"saveWorkEdit"
    "click .save_education_button" :"saveEducationEdit"
    "click .save_high_school_button" :"saveHighSchoolEdit"
    "click .save_about_me_button" :"saveAboutMeEdit"
    "click .save_contact_button" :"saveContactEdit"
    "click .Cancel_edit_basic_info_button" :"CancelEditBasicInfo"
    "click .cancel_about_me_button" :"CancelAboutMeEdit"
    "click .cancel_edit_contact_button" :"CancelEditContact"
    "click .cancel_education_edit_button" :"CancelEducationEdit"
    "click .cancel_high_school_edit_button" :"CancelHighSchoolEdit"
    "click .cancel_work_edit" :"CancelWorkEdit"

  render:->
    $(@el).html(@template())
    @initShowProfile()
    @


  initShowProfile:->
    $.ajax
      type: "GET"
      url: "/users/get_user"
      dataType: "json"
      success: (data) ->

        if data.birthday[0] != null
          document.getElementById('Birthday_data').innerHTML= data.birthday[0] ;
        else
          document.getElementById('Birthday_data').innerHTML='';

        if data.address != null
          document.getElementById('Address_data').innerHTML= data.address ;
        else
          document.getElementById('Address_data').innerHTML='';

        if data.hometown != null
          document.getElementById('hometown_data').innerHTML= data.hometown ;
        else
          document.getElementById('hometown_data').innerHTML='';

#        if data.country != null
#          document.getElementById('Country_data').innerHTML= data.country ;
#        else
#          document.getElementById('Country_data').innerHTML='';

        if data.sex[0] != null
          if data.sex[0] == '0'
            document.getElementById('Sex_data').innerHTML= 'Male' ;
          else if data.sex[0] == '1'
            document.getElementById('Sex_data').innerHTML='Female';
          else
            document.getElementById('Sex_data').innerHTML='';

        if data.relationship_status != null
          document.getElementById('relationship_status_data').innerHTML= data.relationship_status[0]

        if data.languages != null
          document.getElementById('Languages_data').innerHTML= data.languages ;
        else
          document.getElementById('Languages_data').innerHTML='';

        if data.college_university != null
#          uni = new Company.Views.UsersUniversity
#          uni.render()
          document.getElementById('college_university_data').innerHTML= data.profile_universities[0].name ;
          document.getElementById('faculty_data').innerHTML= data.profile_universities[0].faculty ;


        if data.high_school != null
#          high_school = new Company.Views.UsersHighSchool
#          high_school.render()
          document.getElementById('high_school_data').innerHTML= data.profile_high_schools[0].name ;
          document.getElementById('high_school_address_data').innerHTML= data.profile_high_schools[0].high_school_address ;


        if data.company != null
#          comp = new Company.Views.UsersCompanyWork
#          comp.render()
          document.getElementById('company_data').innerHTML= data.profile_companys[0].name ;
          document.getElementById('company_address_data').innerHTML= data.profile_companys[0].company_address ;



        if data.about_me != null
          document.getElementById('about_me_data').innerHTML= data.about_me[0] ;
        else
          document.getElementById('about_me_data').innerHTML='';
        if data.favorite != null
          document.getElementById('favorite_data').innerHTML= data.favorite[0] ;
        else
          document.getElementById('favorite_data').innerHTML='';

        if data.phone != null
          document.getElementById('phone_data').innerHTML= data.phone[0] ;
        else
          document.getElementById('phone_data').innerHTML='';
        if data.email != null
          document.getElementById('email_data').innerHTML= data.email ;
        else
          document.getElementById('email_data').innerHTML='';
        if data.web_url != null
          document.getElementById('web_url_data').innerHTML= data.web_url ;
        else
          document.getElementById('web_url_data').innerHTML='';




  showBasicInfoEdit:->
    edit_profile_mobile = new Company.Views.UserEditBasicInfoMobile()
    edit_profile_mobile.render()
    $('.value_birthday').attr('value', document.getElementById('Birthday_data').innerHTML  )
    $('.value_sex').attr('value', document.getElementById('Sex_data').innerHTML  )
    $('.value_languages').attr('value', document.getElementById('Languages_data').innerHTML  )
    $('.value_relationship').attr('value', document.getElementById('relationship_status_data').innerHTML)
    $('.value_hometown').attr('value', document.getElementById('hometown_data').innerHTML)
    $('.value_address').attr('value', document.getElementById('Address_data').innerHTML)
    $(".biz_form").addClass('disable')
    $('.value_birthday').focus()

  showWorkEdit:->
    edit_work_mobile = new Company.Views.UserEditWorkMobile
    edit_work_mobile.render()
    $('.value_work').attr('value', document.getElementById('company_data').innerHTML  )
    $('.value_work_address').attr('value', document.getElementById('company_address_data').innerHTML  )
    $(".wz_form").addClass('disable')
    $('.value_work').focus()

  showCollegeUniversityEdit:->
    edit_education_mobile = new Company.Views.UserEditEducationMobile
    edit_education_mobile.render()
    $('.value_university').attr('value', document.getElementById('college_university_data').innerHTML  )
    $('.value_faculty').attr('value', document.getElementById('faculty_data').innerHTML  )
    $(".ez_form").addClass('disable')
    $('.value_university').focus()

  showHighSchoolEdit:->
    edit_h_school_mobile = new Company.Views.UserEditHighSchoolMobile
    edit_h_school_mobile.render()
    $('.value_high_school').attr('value', document.getElementById('high_school_data').innerHTML  )
    $('.value_high_schools_address').attr('value', document.getElementById('high_school_address_data').innerHTML  )
    $(".hz_form").addClass('disable')
    $('.value_high_school').focus()



  showAboutMeEdit:->
    edit_about_me_mobile = new Company.Views.UsersEditAboutMeMobile
    edit_about_me_mobile.render()
    $('.value_about_me').attr('value', document.getElementById('about_me_data').innerHTML  )
    $('.value_favorite').attr('value', document.getElementById('favorite_data').innerHTML  )

    $(".amz_form").addClass('disable')
    $('.value_about_me').focus()

  showContactEdit:->
    edit_contact_mobile = new Company.Views.UserEditContactMobile
    edit_contact_mobile.render()
    $('.value_email').attr('value', document.getElementById('email_data').innerHTML  )
    $('.value_phone').attr('value', document.getElementById('phone_data').innerHTML  )
    $('.value_web_url').attr('value', document.getElementById('web_url_data').innerHTML  )
    $(".cz_form").addClass('disable')
    $('.value_phone').focus()


  saveBasicInfoEdit:(e)->
    sex = ''
    if($(".value_sex").val() == 'Female')
      sex = 1
    else if($(".value_sex").val() == 'Male')
      sex = 0
    else sex = 2
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '1'
        "user[birthday]": $('.value_birthday').val()
        "user[policy_birthday]":'public' #$('#policy_birthday').val()
        "user[sex]":sex
        "user[policy_sex]":'public' #$('#policy_sex').val()
        "user[address]": $('#address_input').val()
        "user[relationship_status]": $('.value_relationship').val()
        "user[policy_relationship_status]":'public' #$('#policy_relationship_status').val()
        "user[hometown]": $('#hometown_input').val()
        "user[languages]":$('.value_languages').val()
      dataType: "json"
      success: (data) ->
        $('.edit_basic_info_zone').html('')
        $(".biz_form").removeClass('disable')

#        document.getElementById('Country_data').innerHTML= data.country
        document.getElementById('relationship_status_data').innerHTML= data.relationship_status[0]
        if data.sex[0]=='0'
          document.getElementById('Sex_data').innerHTML=  'Male'
        else if data.sex[0] == '1'
          document.getElementById('Sex_data').innerHTML= 'Female'
        else
          document.getElementById('Sex_data').innerHTML= 'Other'

        document.getElementById('Languages_data').innerHTML= data.languages
        document.getElementById('Address_data').innerHTML= data.address
        document.getElementById('hometown_data').innerHTML= data.hometown
	
        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()


  saveContactEdit:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '7'
        "user[phone]": $('.value_phone').val()
        "user[policy_phone]":'public' #$('#policy_phone').val()
        "user[web_url]": $('.value_web_url').val()
      dataType: "json"
      success: (data) ->
        $('.edit_contact_zone').html('')
        $(".cz_form").removeClass('disable')
        document.getElementById('phone_data').innerHTML= data.phone[0]
        document.getElementById('web_url_data').innerHTML= data.web_url
        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()


  saveAboutMeEdit:(e)->
    $.ajax
      type: "PUT"
      url: "users/update"
      data:
        "user[flag]": '5_mobile'
        "user[about_me]": $('.value_about_me').val()
        "user[policy_about_me]":'public' #$('#policy_about_me').val()
        "user[favorite]": $('.value_favorite').val()
        "user[policy_favorite]":'public' #$('#policy_favorite').val()
      dataType: "json"
      success: (data) ->
        $('.edit_about_me_zone').html('')
        $(".amz_form").removeClass('disable')
        document.getElementById('about_me_data').innerHTML= data.about_me[0]
        document.getElementById('favorite_data').innerHTML= data.favorite[0]
      error: (e) ->
        alert e
    e.preventDefault()



  saveWorkEdit:(e)->
    $.ajax
      type: "PUT"
      url: "users/update"
      data:
        "user[flag]": '2'
        "user[company_name]": $('.value_work').val()
        "user[company_address]": $('.value_work_address').val()

      dataType: "json"
      success: (data) ->
        $('.edit_work_zone').html('')
        $(".wz_form").removeClass('disable')
        document.getElementById('company_works').innerHTML= data.profile_companys[0].name
        document.getElementById('company_address_data').innerHTML= data.profile_companys[0].company_address
      error: (e) ->
        alert e
    e.preventDefault()

  saveEducationEdit:(e)->
    $.ajax
      type: "PUT"
      url: "users/update"
      data:
        "user[flag]": '3'
        "user[profile_universities]": $('.value_university').val()
        "user[faculty]": $('.value_faculty').val()

      dataType: "json"
      success: (data) ->
        $('.edit_education_zone').html('')
        $(".ez_form").removeClass('disable')
        document.getElementById('college_university_data').innerHTML= data.profile_universities[0].name
        document.getElementById('faculty_data').innerHTML= data.profile_universities[0].faculty
      error: (e) ->
        alert e
    e.preventDefault()

  saveHighSchoolEdit:(e)->
    $.ajax
      type: "PUT"
      url: "users/update"
      data:
        "user[flag]": '4'
        "user[high_schools]": $('.value_high_school').val()
        "user[high_schools_address]": $('.value_high_schools_address').val()

      dataType: "json"
      success: (data) ->
        $('.edit_high_school_zone').html('')
        $(".hz_form").removeClass('disable')
        document.getElementById('high_school_data').innerHTML= data.profile_high_schools[0].name
        document.getElementById('high_school_address_data').innerHTML= data.profile_high_schools[0].high_school_address
      error: (e) ->
        alert e
    e.preventDefault()

  CancelEditBasicInfo:->
    $('.edit_basic_info_zone').html('')
    $(".biz_form").removeClass('disable')

  CancelAboutMeEdit:->
    $('.edit_about_me_zone').html('')
    $(".amz_form").removeClass('disable')

  CancelEditContact:->
    $('.edit_contact_zone').html('')
    $(".cz_form").removeClass('disable')

  CancelEducationEdit:->
    $('.edit_edu_zone').html('')
    $(".ez_form").removeClass('disable')

  CancelHighSchoolEdit:->
    $('.edit_high_school_zone').html('')
    $(".hz_form").removeClass('disable')

  CancelWorkEdit:->
    $('.edit_work_zone').html('')
    $(".wz_form").removeClass('disable')