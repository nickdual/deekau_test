class Company.Routers.Users extends Backbone.Router
  routes:
    "sign_up": "sign_up"
    "sign_in": "sign_in"
    "" : "index"
    "users/profile":"profile"

  initialize: () ->
    Backbone.history.navigate(window.location.pathname + window.location.search+ window.location.hash, {trigger: true, replace: false});

  index: ->
    sign_up = new Company.Views.UsersSignUp()
    sign_up.render()
    sign_in = new Company.Views.UsersSignIn()
    sign_in.render()

  sign_up: ->
    sign_up = new Company.Views.UsersSignUp()
    sign_up.render()
    #$(".signupForm.rfloat").html(sign_up.el)
  sign_in: ->
    sign_in = new Company.Views.UsersSignIn()
    sign_in.render()


  profile: ->
    profile = new Company.Views.UserProfileMobile()
    profile.render()

