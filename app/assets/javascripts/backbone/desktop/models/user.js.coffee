class Company.Models.User extends Backbone.Model
  paramRoot: 'user'

  defaults:
    user_name: null
    email: null
    phone: null
    address:null
    hometown:null
    languages:null
    college_university:null
    sex:null
    about_me:null
    favorite:null



