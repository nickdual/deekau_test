class Company.Models.Friend extends Backbone.Model
  idAttribute: "_id"
  uid: ""

  url: () ->
    url_target = ''
    if (@uid)
      url_target = '/chats/find/' + @uid
    else
      url_target = '/chats/find/'

  initialize: (params) ->
    @uid = params['uid'];
