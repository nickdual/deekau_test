class Company.Views.MapView extends Backbone.View
  el: "#content-col-right"
  template: JST['backbone/desktop/templates/map/map_view']

  render:->
    $(@el).html(@template())
