class Company.Views.UsersSignUp extends Backbone.View
  el: ".signupForm.rfloat"
  template: JST['backbone/desktop/templates/users/sign_up']
  events:
    "click input.btn_sign_up": "submit_sign_up"

  render: ->
    model = Company.currentUser
    unless model
      $(@el).html(@template())
      @iphone_checkbox_style()
      @validate_sign_up()
    else
      $(@el).html('')
    @
  validate_sign_up: ->
    response = undefined
    $.validator.addMethod "unique_email", ( value = ->
      $.ajax
        type: "POST"
        url: "/users/unique_email"
        data: "email=" + value
        dataType: "text"
        success: (msg) ->
          response = if (msg is "true") then true else false
      response
    ), I18n.t("sign_up.unique_email_error")
    @$("form.new_user").validate
      errorLabelContainer: @$("form.new_user .box_error_sign_up")
      rules:
        "user[user_name]":
          required: true
        "user[email]":
          required: true
          email: true
          unique_email: true
        "user[email_confirmation]":
          equalTo: "form#new_user #user_email"
        "user[password]":
          required: true
          maxlength: 128
          minlength: 6
      messages:
        "user[user_name]":
          required: I18n.t("sign_up.user_name_error.required")
        "user[email]":
          required: I18n.t("sign_up.email_error.required")
          email: I18n.t("sign_up.email_error.email")
          unique_email: I18n.t("sign_up.unique_email_error")
        "user[password]":
          required: I18n.t("sign_up.password_error.required")
          maxlength: I18n.t("sign_up.password_error.maxlength")
          minlength: I18n.t("sign_up.password_error.minlength")
        "user[email_confirmation]":
          equalTo:  I18n.t("sign_up.email_confirmation_error.equalTo")

  iphone_checkbox_style: ->
    @$('#twitter_on').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_twitter.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_twitter.png">'
      #    onChange: (elem, value) ->
      #      if value
      #        $.ajax
      #          type: "GET"
      #          url: "home/ajax_twitter_on"
      #          success:  ->
      #            $.cookie('twitter_on', true)
      #          error: ->
      #            console.log("There is an error : can't set data")
      #      else
      #        $.ajax
      #          type: "GET"
      #          url: "home/ajax_twitter_off"
      #          success:  ->
      #            $.cookie('twitter_on', null)
      #          error: ->
      #            console.log("There is an error : can't set data")

    )
    @$('#facebook_on').iphoneStyle(
      checkedLabel:'<img src="../assets/ios-style-checkboxes/post_facebook.png">',
      uncheckedLabel:'<img class="image_off" src="../assets/ios-style-checkboxes/post_facebook.png">'
      #    onChange: (elem, value) ->
      #      if value
      #        $.ajax
      #          type: "GET"
      #          url: "home/ajax_facebook_on"
      #          success:  ->
      #            $.cookie('facebook_on', true)
      #          error: ->
      #            console.log("There is an error : can't set data")
      #      else
      #        $.ajax
      #          type: "GET"
      #          url: "home/ajax_facebook_off"
      #          success:  ->
      #            $.cookie('facebook_on',null)
      #          error: ->
      #            console.log("There is an error : can't set data")
    )
  submit_sign_up: (e) ->
    e.preventDefault()
    if @$(".new_user").validate().form()
      $.ajax
        type: "POST"
        url: "/users"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "user[user_name]": @$("#user_user_name").val()
          "user[first_name]": @$("#user_first_name").val()
          "user[last_name]": @$("#user_last_name").val()
          "user[email]": @$("#user_email").val()
          "user[password]": @$("#user_password").val()
          "user[email_confirmation]": @$("#user_email_confirmation").val()
        dataType: "json"
        success: (msg) ->
          alert "success"
        error: (e) ->
          result = $.parseJSON(e.responseText())
          alert result.error






