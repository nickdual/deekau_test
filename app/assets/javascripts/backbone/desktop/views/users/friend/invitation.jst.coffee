class Company.Views.Friends extends Backbone.View

  template_friend_invi: JST['backbone/desktop/templates/users/friends_invi']

  el: '#userinfo-friends'
  events:
    'click .provider': 'providerClick'
    'click .tinif-button': 'btnInviFriendClick'

  initialize: (params) ->
    _.bindAll(this, 'cb_pvfriends', 'cb_sgetUser');
    @parent = params['router']
    @fb = new Facebook(this)
    @gg = new Google(this)
    @ld = new Linkedin(this)

  providerClick: (event) ->
    type = $(event.currentTarget).attr('type')
    if (type == 'facebook')
      @fb.login()
    else if (type == 'google')
      @gg.login()
    else if (type == 'linkedin')
      @ld.login()

  cb_pvfriends: (response) ->
    $.ajax({
      type: 'POST'
      url: '/users/find_friend_oauth_provider'
      async: true
      jsonpCallback: 'jsonCallback'
      dataType: 'json'
      data: {friends: response.data, provider: response.provider}
      success: @cb_sgetUser
    })

  cb_sgetUser: (data) ->
    $('#tini-body > div').empty()
    @put_Unknown(data)
    @put_Known(data)

  #event from UI
  btnInviFriendClick: (event) ->
    tab = $(event.currentTarget).closest('.tini-friends')
    id = tab.attr('id').substr(6)
    type = $(event.currentTarget).attr('type')
    provider = tab.attr('provider')
    $.ajax({
      type: 'POST'
      url: '/users/friend_action'
      async: true
      dataType: 'json'
      data: {id: id, type: type, provider: provider}
      beforeSend: () ->
        if (provider == undefined)
          tab.find('.tinif-button').remove()
        else
          tab.remove()
      success: (response)->
        if (provider == undefined)
          if (type == 'unfriend' || type == 'refuse')
            tab.children('div').append('<button class="tinif-button" type="make">Make Friend</button>');
          else if (type == 'make')
            tab.children('div').append('<button class="tinif-button" type="wait">Waitting</button>');
          else if (type == 'accept')
            tab.children('div').append('<button class="tinif-button" type="unfriend">Unfriend</button>');
        else


    })

  #support function
  put_Unknown: (data) ->
    users = data.users
    _.each(@friends, (value, key) ->
      if (users[key] == undefined)
        this.$el.find('#tini-body').children('.tini-unknown').append(@template_friend_invi({provider: data.provider, id: key, username: value['username'], avatar: value['avatar']}))
    , this)

  put_Known: (data) ->
    if (data.provider == 'facebook')
      _.each(data.users, (value, key) ->
        if (window.friend[value['id']] == undefined)
          this.$el.find('#tini-body').children('.tini-known').append(@template_friend_invi({id: value['id'], username: value['username'], avatar: 'https://graph.facebook.com/'+key+'/picture', type: 0}))
        else
          this.$el.find('#tini-body').children('.tini-known').append(@template_friend_invi({id: value['id'], username: value['username'], avatar: 'https://graph.facebook.com/'+key+'/picture', type: window.friend[value['id']]['status']}))
      , this)
    else if (data.provider == 'google_oauth2')
      _.each(data.users, (value, key) ->
        if (window.friend[value['id']] == undefined)
          this.$el.find('#tini-body').children('.tini-known').append(@template_friend_invi({id: value['id'], username: value['username'], avatar: 'https://lh6.googleusercontent.com/-48cg63WOg_g/AAAAAAAAAAI/AAAAAAAAAAA/cWBSGTs0bJM/s96-c/photo.jpg', type: 0}))
        else
          this.$el.find('#tini-body').children('.tini-known').append(@template_friend_invi({id: value['id'], username: value['username'], avatar: 'https://lh6.googleusercontent.com/-48cg63WOg_g/AAAAAAAAAAI/AAAAAAAAAAA/cWBSGTs0bJM/s96-c/photo.jpg', type: window.friend[value['id']]['status']}))
      , this)
    else if (data.provider == 'linkedin')
      _.each(data.users, (value, key) ->
        if (window.friend[value['id']] == undefined)
          this.$el.find('#tini-body').children('.tini-known').append(@template_friend_invi({id: value['id'], username: value['username'], avatar: 'https://lh6.googleusercontent.com/-48cg63WOg_g/AAAAAAAAAAI/AAAAAAAAAAA/cWBSGTs0bJM/s96-c/photo.jpg', type: 0}))
        else
          this.$el.find('#tini-body').children('.tini-known').append(@template_friend_invi({id: value['id'], username: value['username'], avatar: 'https://lh6.googleusercontent.com/-48cg63WOg_g/AAAAAAAAAAI/AAAAAAAAAAA/cWBSGTs0bJM/s96-c/photo.jpg', type: window.friend[value['id']]['status']}))
      , this)