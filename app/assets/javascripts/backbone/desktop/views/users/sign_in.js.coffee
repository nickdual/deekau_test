class Company.Views.UsersSignIn extends Backbone.View
  el: ".menu_login_container.rfloat"
  template: JST['backbone/desktop/templates/users/sign_in']
  template1: JST['backbone/desktop/templates/users/sign_in_success']
  events:
    "click input.btn_sign_in": "submit_sign_in"
  render: ->
    model = Company.currentUser

    if model
      $(@el).html(@template1(user: model.toJSON()))
    else
      $(@el).html(@template())
    @
  submit_sign_in: (e) ->
    e.preventDefault()

    $.ajax
      type: "POST"
      url: "/users/sign_in"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "user[email]": @$("#user_email").val()
        "user[password]": @$("#user_password").val()
      dataType: "json"
      success: (data) ->
        Company.currentUser =  new Company.Models.User(data);
        sign_up = new Company.Views.UsersSignUp()
        sign_up.render()
        sign_in = new Company.Views.UsersSignIn()
        sign_in.render()
        $('body').removeClass('index').addClass('home')
#        alert(Company.currentUser.merchant)

        document.location='/home';


      error: (e) ->
        #result = $.parseJSON(e.responseText)
        alert e
