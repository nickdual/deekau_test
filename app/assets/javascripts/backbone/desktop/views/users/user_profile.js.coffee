

class Company.Views.UsersProfile extends Backbone.View

  el: '#User_profile'
  template: JST['backbone/desktop/templates/users/user_profile']
  template_university_show: JST['backbone/desktop/templates/users/university']
  template_high_school_show: JST['backbone/desktop/templates/users/high_school']
  template_company_show: JST['backbone/desktop/templates/users/company']
  template_user_basic_info: JST['backbone/desktop/templates/users/user_basic_info']

  events:
    "click .cancel_button" : "cancel_edit"
    "click .save_basic_info" :"save_basic_info"
    "click .save_contact_user": "save_contact"
    "click .save_favorite_edit" :"save_favorite"
    "click .save_about_me_edit" :"save_about_me"
    "click .save_company" :"save_update_company"
    "click .save_high_school" :"save_update_high_school"
    "click .save_university" :"save_update_university"
    "click .save_add_company" :"save_add_company"
    "click .save_add_university" :"save_add_university"
    "click .save_add_high_school" :"save_add_high_school"
#    "click #edit_basic_info_button" :"initBirthday"

  render:->
    $(@el).html(@template())
    @initshowdata()
    @initEditAboutMe()
    @initEditFavorite()
    @initEditContact()
    @initEditBasicInfo()
    @initEditCollegeUniversity()
    @initEditHighSchool()
    @initEditCompany()
    @initAddCollegeUniversity()
    @initAddHighSchool()
    @initAddCompany()
#    @initBirthday()
    @

#  initBirthday:->
#    $("#bday_picker").birthdaypicker({
#      dateFormat: "bigEndian",
#      monthFormat: "long",
#      placeholder: false,
#      hiddenDate: false
#    });


  initshowdata:->
    obj = this
    $.ajax
      type: "GET"
      url: "/users/get_user"
      dataType: "json"
      success: (data) ->
        name = ""
        name = data.first_name + " " + data.last_name
        if name != ""
          document.getElementById('Name_data').innerHTML= name;
        else
          document.getElementById('Name_data').innerHTML= data.user_name;


        if data.sex != null
          if data.sex[0] != null
            if data.sex[0] == '0'
              sex = 'Male' ;
            else if data.sex[0] == '1'
              sex='Female';
            else
              sex = 'Other'

          else
            sex = " "
        else
          sex = " "
          policy_sex = "Public"

        if data.birthday != null
          if data.birthday[0] != null
            birthday= data.birthday[0]
          else
            birthday = " "

          policy_birthday = data.birthday[1]
        else
          birthday = " "
          policy_birthday = " "

        if data.relationship_status != null
          if data.relationship_status[0] != null
            relationship =  data.relationship_status[0]
          else relationship = " "

          policy_relationship =  data.relationship_status[1]
        else
          relationship = " "
          policy_relationship = "Public"

        $('#user_basic_info').html(obj.template_user_basic_info({birthday:birthday, policy_birthday: policy_birthday, address:data.address, hometown: data.hometown ,sex: sex,policy_sex:policy_sex, relationship: relationship,policy_relationship: policy_relationship  ,languages: data.languages}))

        if data.college_university != null
          university = data.profile_universities
          _.each(university, (value) ->
            $('#edu_list').append(obj.template_university_show({name: value['name'],faculty: value['faculty'],address: value['address'],year:value['year']}))
          , this)

        if data.high_school != null
          high_school = data.profile_high_school
          _.each(high_school, (value) ->
            $('#high_shool_list').append(obj.template_high_school_show({name: value['name'], year:value['year'],address:value['high_school_address']}))
          , this)

        if data.profile_companys != null
          company = data. profile_company
          _.each(company, (value) ->
            $('#company_list').append(obj.template_company_show({id:value['_id'] ,name: value['name'], position:value['position'],address:value['company_address'],year:value['year']}))
          , this)

        if data.about_me != null
          document.getElementById('about_me_data').innerHTML= data.about_me[0] ;

        if data.favorite != null
          document.getElementById('favorite_data').innerHTML= data.favorite[0] ;


        if data.phone != null
          document.getElementById('phone_data').innerHTML= data.phone[0] ;

        if data.email != null
          document.getElementById('email_data').innerHTML= data.email ;

        if data.web_url != null
          document.getElementById('web_url_data').innerHTML= data.web_url ;

  initEditAboutMe:->
    about_me = new Company.Views.UsersEditAboutMe
    about_me.render()
    @$('#edit_about_me_button')
      .popover(
        html : true,
        content: ->
          about_me.el
      )
    @$('#edit_about_me_button').live('click',(e)->
      document.getElementById('about_me_edit').innerHTML= document.getElementById('about_me_data').innerHTML
      $('.chosen_option').chosen()
      $('.chzn-search').hide()
      e.preventDefault()
    )
  initEditFavorite:->
    favorite = new Company.Views.UsersEditFavorite
    favorite.render()
    @$('#edit_favorite_button')
      .popover(
        html : true,
        content: ->
          favorite.el
      )
    @$('#edit_favorite_button').live('click',(e)->
      document.getElementById('favorite_edit').innerHTML= document.getElementById('favorite_data').innerHTML
      $('.chosen_option').chosen()
      $('.chzn-search').hide()
      e.preventDefault()
    )
  initEditContact:->
    contact = new Company.Views.UsersEditContact
    contact.render()
    @$('#edit_contact_button')
      .popover(
        html : true,
        content: ->
          contact.el
      )
    @$('#edit_contact_button').live('click',(e)->
      $('#email_edit').attr('value', document.getElementById('email_data').innerHTML)
      $('#phone_edit').attr('value', document.getElementById('phone_data').innerHTML)
      $('#web_url_edit').attr('value', document.getElementById('web_url_data').innerHTML)
      $('.chosen_option').chosen()
      $('.chzn-search').hide()
      e.preventDefault()
    )

  initEditBasicInfo:->
    bday =  "1996-9-17"
    basic_info = new Company.Views.UsersEditBasicInfo
    basic_info.render()
    @$('#edit_basic_info_button')
      .popover(
        html : true,
        content: ->
          basic_info.el
        ,
        clickOff: true
      )

    $('#edit_basic_info_button').live 'click',(e) ->
#      if document.getElementById('Birthday_data').innerHTML != ""
#        day = document.getElementById('Birthday_data').innerHTML.toString()
#        bday = '"'+ day + '"'

      $('#input_address').attr('value', document.getElementById('Address_data').innerHTML)
      $('#hometown_input').attr('value', document.getElementById('hometown_data').innerHTML)
      $('.sex').attr('value', document.getElementById('Sex_data').innerHTML)
      $('#languages_input').attr('value', document.getElementById('Languages_data').innerHTML)
      $('#relationship_status_input').attr('value', document.getElementById('relationship_status_data').innerHTML)
      $('#policy_birthday').attr('value',$('.policy_birthday').val())
      $('#policy_sex').attr('value',$('.policy_sex').val())
      $('#policy_relationship_status').attr('value',$('.policy_relationship').val())
      $('.chosen_option').chosen()
      $('.chzn-search').hide()
      $("#bday_picker").birthdaypicker({
        dateFormat: "bigEndian",
        monthFormat: "long",
        placeholder: false,
        hiddenDate: true,
        defaultDate: bday
      });
      e.preventDefault()

  cancel_edit:->
    $('.popover').hide()

  save_basic_info:(e)->
    sex = ''
    if($(".sex").val() == 'Female')
      sex = 1
    else if($(".sex").val() == 'Male')
      sex = 0
    else sex = 2
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '1'
        "user[birthday]": $('#birthdate').val()
        "user[policy_birthday]":$('#policy_birthday').val()
        "user[sex]":sex
        "user[policy_sex]":$('#policy_sex').val()
        "user[address]": $('#input_address').val()
        "user[relationship_status]": $('#relationship_status_input').val()
        "user[policy_relationship_status]":$('#policy_relationship_status').val()
        "user[hometown]": $('#hometown_input').val()
        "user[languages]":$('#languages_input').val()
      dataType: "json"
      success: (data) ->
        sex=''
        document.getElementById('Birthday_data').innerHTML= data.birthday[0]
        document.getElementById('Address_data').innerHTML= data.address
        document.getElementById('hometown_data').innerHTML= data.hometown
        document.getElementById('relationship_status_data').innerHTML= data.relationship_status[0]
        if data.sex[0]=='0'
          sex == 'Male'
        else if data.sex[0] == '1'
          sex == 'Female'
        else sex = 'Other'

        document.getElementById('Sex_data').innerHTML= sex
        document.getElementById('Languages_data').innerHTML= data.languages

        $('.policy_birthday').attr('value',data.birthday[1])
        $('.policy_sex').attr('value',data.sex[1])
        $('.policy_relationship').attr('value',data.relationship_status[1])


        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()


  save_contact:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '7'
        "user[phone]": $('#phone_edit').val()
        "user[policy_phone]":$('#policy_phone').val()
        "user[web_url]": $('#web_url_edit').val()
      dataType: "json"
      success: (data) ->
        document.getElementById('phone_data').innerHTML= data.phone[0]
        document.getElementById('web_url_data').innerHTML= data.web_url
        $('.policy_phone').attr('value',data.phone[1])
        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_favorite:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '6'
        "user[favorite]": $('#favorite_edit').val()
        "user[policy_favorite]":$('#policy_favorite').val()
      dataType: "json"
      success: (data) ->
        document.getElementById('favorite_data').innerHTML= data.favorite[0]
        $('.policy_favorite').attr('value',data.favorite[1])
        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_about_me:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '5'
        "user[about_me]": $('#about_me_edit').val()
        "user[policy_about_me]":$('#policy_about_me').val()
      dataType: "json"
      success: (data) ->
        document.getElementById('about_me_data').innerHTML= data.about_me[0]
        $('.policy_about_me').attr('value',data.about_me[1])
        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()


  save_update_company:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '2'
        "user[company_name]": $('#company_name').val()
        "user[company_address]":$('#input_address').val()
        "user[year_work]": $('#year_input').val()
        "user[position]":$('#input_position').val()
      dataType: "json"
      success: (data) ->
        document.getElementById('company_data').innerHTML= data.profile_companys[0].name
        document.getElementById('company_address_data').innerHTML= data.profile_companys[0].company_address
#        document.getElementById('company_year_data').innerHTML= data.profile_companys[0].year
#        document.getElementById('job_data').innerHTML= data.profile_companys[0].position

        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_add_company:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '8'
        "user[company_name]": $('#company_name').val()
        "user[company_address]":$('#input_address').val()
        "user[year_work]": $('#year_input').val()
        "user[position]":$('#input_position').val()
      dataType: "json"
      success: (data) ->
#        document.getElementById('company_data').innerHTML= data.profile_companys.last.name
#        document.getElementById('company_address_data').innerHTML= data.profile_companys.last.company_address
#        document.getElementById('company_year_data').innerHTML= data.profile_companys[0].year
#        document.getElementById('job_data').innerHTML= data.profile_companys[0].position

        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_update_university:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '2'
        "user[profile_universities]": $('#college_university_name').val()
        "user[university_address]":$('#input_address').val()
        "user[year]": $('#year_input').val()
        "user[faculty]":$('#input_faculty').val()
      dataType: "json"
      success: (data) ->
        document.getElementById('college_university_data').innerHTML= data.profile_universities[0].name
        document.getElementById('faculty_data').innerHTML= data.profile_universities[0].faculty
#        document.getElementById('university_year_data').innerHTML= data.profile_companys[0].year
#        document.getElementById('university_address_data').innerHTML= data.profile_companys[0].position

        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_add_university:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '9'
        "user[profile_universities]": $('#college_university_name').val()
        "user[university_address]":$('#input_address').val()
        "user[year]": $('#year_input').val()
        "user[faculty]":$('#input_faculty').val()
      dataType: "json"
      success: (data) ->
#        document.getElementById('college_university_data').innerHTML= data.profile_universities[0].name
#        document.getElementById('faculty_data').innerHTML= data.profile_universities[0].faculty


        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_update_high_school:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '2'
        "user[high_schools]": $('#high_school_name').val()
        "user[high_schools_address]":$('#input_address').val()
        "user[high_schools_year]": $('#year_input').val()

      dataType: "json"
      success: (data) ->
        document.getElementById('high_school_data').innerHTML= data.profile_high_schools[0].name
        document.getElementById('high_school_address_data').innerHTML= data.profile_high_schools[0].company_address


        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  save_add_high_school:(e)->
    $.ajax
      type: "PUT"
      url: "/users/update"
      data:
        "user[flag]": '10'
        "user[high_schools]": $('#high_school_name').val()
        "user[high_schools_address]":$('#input_address').val()
        "user[high_schools_year]": $('#year_input').val()

      dataType: "json"
      success: (data) ->


        $('.popover').hide()
      error: (e) ->
        alert e
    e.preventDefault()

  initAddCollegeUniversity:->
    add_university = new Company.Views.UsersAddUniversity
    add_university.render()
    @$('#add_college_university')
      .popover(
        html : true,
        content: ->
          add_university.el
#
      )
    $('#add_college_university').live 'click',(e) ->
      $('#save_button').addClass('save_add_university')
      $('#save_button').removeClass('save_university')

  initAddHighSchool:->
    addhigh_school = new Company.Views.UsersAddHighSchool
    addhigh_school.render()
    @$('#add_high_school')
      .popover(
        html : true,
        content: ->
          addhigh_school.el
      )
    $('#add_high_school').live 'click',(e) ->
      $('#save_button_high_school').addClass('save_add_high_school')
      $('#save_button_high_school').removeClass('save_high_school')


  initAddCompany:->
    add_company = new Company.Views.UsersAddCompany()
    add_company.render()
    @$('#add_company')
      .popover(
        html : true,
        content: ->
          add_company.el
      )

    $('#add_company').live 'click',(e) ->
      $('#save_button_company').addClass('save_add_company')
      $('#save_button_company').removeClass('save_company')



  initEditCollegeUniversity:->
    edit_col_uni = new Company.Views.UsersEditUniversity()
    edit_col_uni.render()

    @$('.edit_university').live 'click', (e)->
      $('.edit_university').popover(
        html : true,
        content: ->
          edit_col_uni.el
      )
      $('#college_university_name').attr('value', document.getElementById('college_university_data').innerHTML)
      $('#input_faculty').attr('value', document.getElementById('faculty_data').innerHTML)
      $('#year_input').attr('value', document.getElementById('year_data').innerHTML)
      $('#input_address').attr('value', document.getElementById('address_data').innerHTML)

  initEditCompany:->
    edit_company = new Company.Views.UsersEditCompany()
    edit_company.render()
    @$('.edit_company_button').live 'click',(e)->
      id = $('.company_id').val()
      edit_button =  $('.edit_company_button_'+id)

      edit_button.popover(
        html : true,
        content: ->
          edit_company.el
      )
      $('#company_name').attr('value', document.getElementById('company_data').innerHTML)
      $('#input_position').attr('value', document.getElementById('company_position_data').innerHTML)
      $('#year_input').attr('value', document.getElementById('company_year_data').innerHTML)
      $('#input_address').attr('value', document.getElementById('company_address_data').innerHTML)

  initEditHighSchool:->
    edit_high_school = new Company.Views.UsersEditHighSchool()
    edit_high_school.render()
    @$('.edit_high_school').live 'click', (e)->
      $('.edit_high_school').popover(
        html : true,
        content: ->
          edit_high_school.el
      )
      $('#high_school_name').attr('value', document.getElementById('high_school_data').innerHTML)
      $('#year_input').attr('value', document.getElementById('high_shool_year_data').innerHTML)
      $('#input_address').attr('value', document.getElementById('high_shool_address_data').innerHTML)
