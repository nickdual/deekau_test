class Company.Views.UsersEditAboutMe extends Backbone.View

  template: JST['backbone/desktop/templates/users/edit_about_me']


  render:->
    $(@el).html(@template())
    @