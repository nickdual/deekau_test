class Company.Views.UserContacts extends Backbone.View
  el: "#container-friend"
  template: JST['backbone/desktop/templates/users/contact']

  events:
    "click button.btn_invite": "invite"
  render: ->
    collection = new  Company.Collections.Contacts()
    collection.fetch()
    $(@el).html(@template1(contacts: collection.toJSON()))
    @
  invite: (e) ->
    e.preventDefault()
    alert("click")