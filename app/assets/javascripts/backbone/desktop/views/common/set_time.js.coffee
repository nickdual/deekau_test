class Company.Views.SetTime extends Backbone.View

  template: JST['backbone/desktop/templates/common/set_time']
#  events:
#    "click span.ggcheck": "active_open_day"


  render:->
    $(@el).html(@template())
    @set_openhour()
    @

  initTimePicker: ->
    @$('.time_picker').timepicker({
      timeFormat:'H:i'
    });

  $("span.ggcheck").live 'click',(e) ->

  #active_open_day: (e)->
    $(e.currentTarget).toggleClass("checked")
    if $(e.currentTarget).attr("aria-checked") == "true"
      $(e.currentTarget).attr("aria-checked","false")
    else
      $(e.currentTarget).attr("aria-checked","true")
    e.preventDefault()


  set_openhour:->
    $('#gwt-uid-mon').live('click', ->
      if ($('input.mon').attr('value') == 'false')
        $('input.mon').attr('value','true')
      else
        $('input.mon').attr('value','false')
    )
    $('#gwt-uid-tue').live('click', ->
      if ($('input.tue').attr('value') == 'false')
        $('input.tue').attr('value','true')
      else
        $('input.tue').attr('value','false')
    )
    $('#gwt-uid-wed').live('click', ->
      if ($('input.wed').attr('value') == 'false')
        $('input.wed').attr('value','true')
      else
        $('input.wed').attr('value','false')
    )
    $('#gwt-uid-thu').live('click', ->
      if ($('input.thu').attr('value') == 'false')
        $('input.thu').attr('value','true')
      else
        $('input.thu').attr('value','false')
    )
    $('#gwt-uid-fri').live('click', ->
      if ($('input.fri').attr('value') == 'false')
        $('input.fri').attr('value','true')
      else
        $('input.fri').attr('value','false')
    )
    $('#gwt-uid-sat').live('click', ->
      if ($('input.sat').attr('value') == 'false')
        $('input.sat').attr('value','true')
      else
        $('input.sat').attr('value','false')
    )
    $('#gwt-uid-sun').live('click', ->
      if ($('input.sun').attr('value') == 'false')
        $('input.sun').attr('value','true')
      else
        $('input.sun').attr('value','false')
    )
