class Company.Views.ChatIndex extends Backbone.View

  template_tab: JST['backbone/desktop/templates/chat/tab']
  template_group: JST['backbone/desktop/templates/chat/group']
  template_user: JST['backbone/desktop/templates/chat/user']
  template_msg: JST['backbone/desktop/templates/chat/message']
  template_user_status: JST['backbone/desktop/templates/chat/user_status']

  el: '#chat-system'
  el_csbody: ''

  friends: {}
  recent_chat: []
  user_online: {}
  groupsmiles: ''
  smiles: {}
  rooms: {}
  status: 0
  unreadmsg: {}
  events:
    'click .clfb-friend': 'friendClick'
    'keyup #ctlf-search': 'searchFriendsKeyup'
    'click .ctlfc-header': 'groupdisplayClick'
    'click .ct-hide': 'cthideClick'
    'click .ct-header': 'ctheaderClick'
    'click .close': 'btncloseClick'
    'keypress .ct-input > textarea': 'inputareaKeypress'
    'click .smileicon': 'smileiconClick'
    'click .chat-tab > .ct-show > .ctf-body .ctfb-loadmore': 'loadmoreClick'
    'mouseenter .ctfb-msg': 'msgMouseEnter'
    'mouseleave .ctfb-msg': 'msgMouseLeave'
    'click #cthu-status-symbol': 'statusdialogClick'
    'click #cthl-status > li': 'userstatusClick'
    'click .ct-addfriend': 'addfriendClick'
    'keyup .ctfb-addfriend > input': 'addfriendKeypress'
    'click .ctfb-addfriend > input': 'addfriendKeypress'
    'click .ctfb-addfriend li': 'addfrienddialogClick'
    'keypress .ctfb-addfriend li': 'addfrienddialogClick'
    'click .ctfb-addfriend > button': 'inviteClick'
    'click .ctgl-users': 'groupchatshowClick'
    'click .ctf-body': 'tabfocusClick'
    'click #cts-minimize > ul > li > i': 'ctmCloseClick'
    'click #cts-minimize > ul > li': 'ctmItemClick'

  initialize: ->
    _.bindAll(this, 'cbfetch', 'sk_cbConnect', 'put_RecentChat', 'put_Online', 'getStatusFromID',
                    'friendClick', 'searchFriendsKeyup', 'groupdisplayClick', 'cthideClick',
                    'ctheaderClick', 'inputareaKeypress', 'smileiconClick', 'statusdialogClick',
                    'userstatusClick', 'sk_switchtoRoom', 'addfriendKeypress', 'tabfocusClick')
    this.init()
    new Company.Models.Friend({uid: window.user['id']}).fetch({success: this.cbfetch})
  cbfetch: (model, response) ->
    @status = 1
    @friends = {}
    @recent_chat = []
    @user_online = {}
    @rooms = {}
    @unreadmsg = {}

    window.iosocket.init_Connect(this);
    if (model.get('friend_detail') != null)
      _.each(model.get('friend_detail'), (value, key) ->
        @friends[value['uid']] = value
      , this)
    @recent_chat = model.get('recent_chat')
    if (@recent_chat == null)
      @recent_chat = []

    window.friend = @friends

  init: () ->
    this.$el.removeClass('hide')
    @el_csbody = this.$el.find('#ctlf-ndisplay')
    this.$el.find('#cth-userstatus > ul').empty().parent().append(@template_user_status({show: 1, status: 'available'}));
    this.$el.find('#cthu-status-symbol').children('span').attr('class', 'cs-available')
    this.emotionLoad()

  initChattab: (user) ->
    html = @template_tab({id: user['id'], username: user['username'], avatar: user['avatar'], status: user['status'], smiles: @groupsmiles, display: user['display']})
    this.$el.children('#cts-show').append(html);

    this.saveTabToCookie(user['id'], user['display'])

    this.insertChatTab(user)

    obj = this
    $.ajax({
      type: 'POST',
      url: '/chats/init',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {user_id: window.user['id'], friend_id: user['id']},
      success: (data) ->
        tab = obj.$el.children('#cts-show').children('#ctid-' + user['id'])
        if (tab.length != 0 && data['count'] > 0)
          tab.children('.ct-show').children('.ctf-body').prepend('<div class="ctfb-loadmore" index="1" total="'+data['count']+'">load more</div>');
          obj.loadmoreAdd(data, tab)
          body_tag = obj.$el.children('#cts-show').children('#ctid-'+user['id']).children('.ct-show').children('.ctf-body:first')[0]
          body_tag.scrollTop = body_tag.scrollHeight;
    })

    $.ajax({
      type: 'POST',
      url: '/chats/add_recent_chat',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {user_id: window.user['id'], friend_id: user['id']},
      success: (data) ->
        #console.log(data)
      error: (response) ->
        #console.log(response)
    })

    $('.chat-autoresize').autosize({
      callback: () ->
        body = $(this).parent().prev();
        input_height = (parseInt($(this).height()) + 8 - 27);
        body_height = body.height();
        body.css('height', (254 - input_height - 28) + 'px');
    });

  #listen from socket
  sk_cbConnect: (users) ->
    @user_online = users
    if (@user_online == null)
      @user_online = {}
    this.put_RecentChat()
    this.put_Online()
    this.put_Offline()
    tabs = JSON.parse($.docCookies.getItem('deekau_chat_tabs'))
    cts_status = $.docCookies.getItem('deekau_chat_system')
    if (cts_status == '1')
      this.$el.children('#ctl-friends').children('.ct-hide').addClass('hide')
      this.$el.children('#ctl-friends').children('#ctlf-container').removeClass('hide')

    _.each(tabs, (value, key) ->
      if (@friends[key] != undefined)
        if (this.$el.children('#cts-show').children('#ctid-' + key).length == 0)
          this.initChattab({id: key, username: @friends[key]['username'], avatar: @friends[key]['avatar'], status: this.getStatusFromID(key), display: value})
    , this)
    $('.os-scroll').antiscroll();

  sk_userConnect: (user) ->
    if (user['id'] == window.user['id'])
      new Company.Models.Friend({uid: window.user['id']}).fetch({success: this.cbfetch})
    else
      this.uiUserConnectChange(user, user['status'])

  sk_userDisconnect: (user) ->
    if (user['id'] == window.user['id'])
      @status = 4
      window.iosocket.disconnect()
      this.emptyView()
    else
      this.uiUserConnectChange(user, 3)

  sk_receiveMessage: (msg) ->
    if (this.$el.children('#cts-show').children('#ctid-' + msg['tab_id']).length == 0)
      this.initChattab({id: msg['tab_id'], username: msg['username'], avatar: msg['avatar'], status: this.toStatus(msg['status'])})
    else
      this.addNewMessage({date: new Date(msg['time']), 'tab_id': msg['tab_id'], username: msg['username'], avatar: msg['avatar'], type: msg['id'], content: msg['content'], append: 'last'})
      this.showChatTabNoti(msg)

  sk_receiveStatus: (msg) ->
    if (msg['id'] == window.user['id'])
      @status = msg['status']
      this.$el.find('#ctlf-container').find('span:first').attr('class', 'cs-'+this.toStatus(@status))
    else
      this.uiUserConnectChange(msg, msg['status'])

  sk_switchtoRoom: (msg) ->
    if (msg['status'] == 'ok')
      tab = this.$el.children('#cts-show').children('#ctid-' + msg['id'])
      tab.attr('id', 'ctid-' + msg['groupid'])
      tab.find('.ctfb-loadmore').remove()
      tab.find('.ct-icon').empty().html('<i class="icon-fire"/><span></span>').attr('class', 'ct-icon ctgl-users');
      tab.attr('type', 'group')

  sk_userinRoom: (msg) ->
    if (@rooms[msg['id']] == undefined)
      @rooms[msg['id']] = {}
    group_title = ''
    joinRoom = ''
    count = 0
    _.each(msg['users'], (value, key) ->
      count++
      if (group_title == '')
        group_title = "<span>"+value['username']+"</span>"
      else
        group_title += ', ' + "<span>"+value['username']+"</span>"
      if (@rooms[msg['id']][key] == undefined)
        @rooms[msg['id']][key] = msg['users'][key]
        if (key != window.user['id'])
          joinRoom += '<div class="ctfb-alert">'+msg['users'][key]['username']+' join in room</div>'
    , this)

    tab = this.$el.children('#cts-show').children('#ctid-' + msg['id'])
    if (tab.length == 0)
      html = @template_tab({id: msg['id'], username: msg['users'], type: 'group', smiles: @groupsmiles})
      this.$el.children('#cts-show').append(html);
      tab = this.$el.children('#cts-show').children('#ctid-' + msg['id'])
      this.insertChatTab({id: msg['id'], username: joinRoom})
    else
      tab.find('.ct-show .ct-title').empty().append(group_title)

    tab.find('> .ct-show > .ctf-body > .ctfb-container').append(joinRoom)

  sk_leaveRoom: (msg) ->
    if (msg['id'] == window.user['id'])
      this.removeTabChat(msg['roomid'])
    else
      tab = this.$el.children('#cts-show').children('#ctid-' + msg['roomid'])
      tab_body = tab.find('.ctf-body .ctfb-container');
      html_msg = '<div class="ctfb-alert">'+msg['username']+' left room</div>'
      tab_body.append(html_msg)
      group_title = ''
      delete @rooms[msg['roomid']][msg['id']]
      _.each(@rooms[msg['roomid']], (value, key) ->
        if (group_title == '')
          group_title = "<span>"+value['username']+"</span>"
        else
          group_title += ', ' + "<span>"+value['username']+"</span>"
      )
      tab.children('.ct-show').find('.ct-title').html(group_title)

  #event
  ctmItemClick: (event) ->
    this.tabfocusClick($(event.currentTarget).children('a').attr('ref').substr(6))


  ctmCloseClick: (event) ->
    id = $(event.currentTarget).parent().children('a').attr('ref')
    $(event.currentTarget).parent().remove()
    tab = this.$el.children('#cts-show').children(id)
    if (tab.attr('type') == 'group')
      window.iosocket.sendleaveRoom({roomid: id})
    this.removeTabChat(tab.attr('id').substr(5))
    count = this.$el.children('#cts-minimize').children('ul').children('li').length
    this.$el.children('#cts-minimize').find('span').text(count)
    if (count == 0)
      this.$el.children('#cts-minimize').addClass('hide')

  friendClick: (event) ->
    id = $(event.currentTarget).attr('id').substr(9)
    username = $.trim($(event.currentTarget).children('div').children('div').text())
    status = $(event.currentTarget).find('span').attr('class')
    if (status == undefined || status == '')
      status = 'invisible'
    else
      status = status.substr(3)
    avatar = $(event.currentTarget).children('img').attr('src')
    if (this.$el.children('#cts-show').children('#ctid-' + id).length == 0)
      this.initChattab({id: id, username: username, avatar: avatar, status: status})

    this.tabfocusClick(id)

  searchFriendsKeyup: (event) ->
    content = $(event.currentTarget).val()
    this.$el.find('#ctlf-result').empty()
    if (content == '')
      this.$el.find('#ctlf-ndisplay').css('display', 'block')
    else
      this.$el.find('#ctlf-ndisplay').css('display', 'none')
      obj = this.$el.find('#ctlf-result')
      #obj.empty()
      _.each(@friends, (value, key) ->
        if (value['username'].toLowerCase().indexOf(content.toLowerCase()) != -1)
          html = @template_user({id: key, username: value['username'], status: this.toStatus(key), avatar: value['avatar']})
          obj.append(html);
      , this)
      $('.os-scroll').antiscroll();

  groupdisplayClick: (event) ->
    obj = $(event.currentTarget)
    if (obj.find('span').attr('class') == 'arrow-left')
      obj.parent().children('.ctlfc-body').removeClass('hide')
      obj.find('span').attr('class', 'arrow-down')
    else
      obj.parent().children('.ctlfc-body').addClass('hide')
      obj.find('span').attr('class', 'arrow-left')
    $('.os-scroll').antiscroll();

  cthideClick: (event) ->
    $(event.currentTarget).addClass('hide');
    $(event.currentTarget).next().removeClass('hide');
    this.changeTabCookie($(event.currentTarget).closest('.chat-tab'))

  ctheaderClick: (event) ->
    $(event.currentTarget).parent().addClass('hide');
    $(event.currentTarget).parent().prev().removeClass('hide');
    this.changeTabCookie($(event.currentTarget).closest('.chat-tab'))

  btncloseClick: (event) ->
    tab = $(event.currentTarget).closest('.chat-tab');
    id = tab.attr('id').substr(5)
    if (tab.attr('type') == 'group')
      window.iosocket.sendleaveRoom({roomid: id})
    this.removeTabChat(id)
    tab_min = this.$el.children('#cts-minimize')
    if (tab_min.css('display') != 'none')
      tab_min.find('span').text(parseInt(tab_min.find('span').text()) - 1)
      item = tab_min.children('ul').children('li:first')
      this.$el.children('#cts-show').children(item.children('a').attr('ref')).removeClass('hide')
      item.remove()
      if (tab_min.children('ul').children().length == 0)
        tab_min.addClass('hide')
    return false;

  inputareaKeypress: (event) ->
    if (event.which == 13)
      if (!event.shiftKey)
        tab = $(event.currentTarget).closest('.chat-tab')
        this.sendMessageAction(tab)
        return false;

  smileiconClick: (event) ->
    symbol = $(event.currentTarget).attr('symbol')
    textarea = $(event.currentTarget).closest('.ct-input').children('textarea')
    content = textarea.val() + symbol
    textarea.focus().val(content)

  loadmoreClick: (event) ->
    tab = $(event.currentTarget).closest('.chat-tab')
    friend_id = tab.attr('id').substr(5)
    index = $(event.currentTarget).attr('index')
    total = $(event.currentTarget).attr('total')
    if (!(((parseFloat(index) == parseInt(index)) && !isNaN(index)) && ((parseFloat(total) == parseInt(total)) && !isNaN(total))))
      return
    obj = this;
    $.ajax({
      type: 'POST',
      url: '/chats/load_more',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {user_id: window.user['id'], friend_id: friend_id, index: index, total: total},
      success: (data) ->
        if (data == null || data["log"].length == 0)
          tab.find('.ctfb-loadmore').remove()
          return
        obj.loadmoreAdd(data, tab)
    })
    $(event.currentTarget).attr('index', parseInt(index) + 1)

  msgMouseEnter: (event) ->
    $(event.currentTarget).children('.ctfbm-time').removeClass('hide')

  msgMouseLeave: (event) ->
    $(event.currentTarget).children('.ctfbm-time').addClass('hide')

  statusdialogClick: (event) ->
    type = $(event.currentTarget).parent().attr('class')
    if (type.indexOf('open') != -1)
      $(event.currentTarget).parent().removeClass('open')
    else
      $(event.currentTarget).parent().addClass('open')
    event.stopPropagation()

  userstatusClick: (event) ->
    event.stopPropagation()
    $(event.currentTarget).closest('.dropup').removeClass('open')
    type = $(event.currentTarget).attr('type')
    status = 0
    if (type == 'signout')
      status = 4
    else if (type == 'signin')
      status = 5
    else if (type == 'available')
      status = 1
    else if (type == 'busy')
      status = 2
    else if (type == 'invisible')
      status = 3

    if (status == 4)
      window.iosocket.disconnect()
      @status = 4
      this.emptyView()
    else if (status == 5)
      new Company.Models.Friend({uid: window.user['id']}).fetch({success: this.cbfetch})
    else
      window.iosocket.sendStatus({status: status})

  addfriendClick: (event) ->
    tab_body = $(event.currentTarget).closest('.ct-show')
    if (tab_body.children('.ctfb-addfriend').css('top') == '-6px')
      tab_body.children('.ctfb-addfriend').stop().animate({'top': '28px'}, 250)
    else
      tab_body.children('.ctfb-addfriend').stop().animate({'top': '-6px'}, 250)
    event.stopPropagation();

  addfriendKeypress: (event) ->
    text = $(event.currentTarget).val()
    if (text == '')
      $(event.currentTarget).parent().removeClass('open')
    else
      $(event.currentTarget).parent().children('ul').empty()
      count = 0
      tab_chat = $(event.currentTarget).closest('.chat-tab')
      tab_id = tab_chat.attr('id').substr(5)
      type = tab_chat.attr('type')

      _.each(@user_online, (value, key) ->
        if (@friends[key]['username'].toLowerCase().indexOf(text.toLowerCase()) != -1 && ((type == 'person' && tab_id != key) || (type =='group' && @rooms[tab_id][key] == undefined)))
          count++
          $(event.currentTarget).parent().children('ul').append('<li id="grca-fid-'+key+'"><a tabindex="-1" ref="">'+@friends[key]['username']+'</a></li>')
      , this)
      if (count == 0)
        $(event.currentTarget).parent().removeClass('open')
      else
        $(event.currentTarget).parent().addClass('open')
    event.stopPropagation()

  addfrienddialogClick: (event) ->
    if (event.which == 13 || event.which == 1)
      id = $(event.currentTarget).attr('id').substr(9)
      name = $(event.currentTarget).text()
      tab_f = $(event.currentTarget).closest('.ctfb-addfriend')
      tab_f.children('input').val('"' + name + '"<' + id + '>')
      tab_f.removeClass('open')

  inviteClick: (event) ->
    text = $(event.currentTarget).parent().children('input').val()
    id = text.match(/<(.)+>/g)[0]
    id = id.substr(1, id.length - 2)
    if (@user_online[id] != undefined)
      tab_chat = $(event.currentTarget).closest('.chat-tab')
      tab_id = tab_chat.attr('id').substr(5)
      if (tab_chat.attr('type') == 'person')
        groupid = window.user['id'] + '_' +Date.now()
        window.iosocket.joinRoom({id: tab_id, username: @friends[tab_id]['username'], avatar: @friends[tab_id]['avatar'], roomid: groupid, type: 0})
        tab_id = groupid
      window.iosocket.joinRoom({id: id, username: @friends[id]['username'], avatar: @friends[id]['avatar'], roomid: groupid})
    text = $(event.currentTarget).parent().children('input').val('')

  groupchatshowClick: (event) ->
    event.stopPropagation()

  tabfocusClick: (event) ->
    if (event.currentTarget != undefined)
      tab = $(event.currentTarget).closest('.chat-tab')
      tab.children('.ct-show').children('.ct-input').children('textarea').focus()
      tab.find('.ct-unread').removeClass('ct-unread')
    else
      tab = this.$el.children('#cts-show').children('#ctid-' + event)
      tab.find('.ct-unread').removeClass('ct-unread')
      this.saveTabToCookie(event, 1)
      if (tab.css('display') != 'none')
        tab.find('.ct-show').children('.ct-input').children('textarea').focus()
      else
        tab_active = null
        _.each(this.$el.children('#cts-show').children(), (value, key) ->
          if ($(value).css('display') != 'none')
            tab_active = $(value)
        , this)
        if (tab_active == null)
          return
        tab_active.addClass('hide')
        this.changeTabCookie(tab_active)
        if (this.$el.children('#cts-minimize').children('ul').find('a[ref=#'+tab_active.attr('id')+']').length == 0)
          this.$el.children('#cts-minimize').children('ul').append('<li><i class="icon-remove"></i><a tabindex="-1" ref="#'+tab_active.attr('id')+'">'+tab_active.children('.ct-show').find('.ct-title').text().replace('', '')+'</a></li>')
        this.$el.children('#cts-minimize').children('ul').find('[ref=#ctid-'+event+']').parent().remove()
        tab.removeClass('hide')


  #support function
  insertChatTab: (user) ->
    tab = this.$el.children('#cts-show').children('#ctid-' + user['id'])
    count = 0
    _.each(this.$el.children('#cts-show').children(), (value, key) ->
      if ($(value).css('display') != 'none')
        count++
    , this)
    if (count < 3)
      tab.removeClass('hide')
    else
      this.$el.children('#cts-minimize').removeClass('hide')
      if (this.$el.children('#cts-minimize').children('ul').find('a[ref=#ctid-'+user['id']+']').length == 0)
        this.$el.children('#cts-minimize').children('ul').append('<li><i class="icon-remove"></i><a tabindex="-1" ref="#ctid-'+user['id']+'">'+user['username']+'</a></li>')
      this.$el.children('#cts-minimize').find('span').text(this.$el.children('#cts-minimize').children('ul').children('li').length)

  showChatTabNoti: (item) ->
    if (item['id'] == window.user['id'])
      return
    tab = this.$el.children('#cts-show').children('#ctid-'+item['tab_id'])
    tab.find('.ct-hide').addClass('ct-unread')
    tab.find('.ct-header').addClass('ct-unread')
    this.$el.children('#cts-minimize').find('a[ref=#ctid-'+item['tab_id']+']').parent().addClass('ct-unread')
    body_tag = tab.children('.ct-show').children('.ctf-body:first')[0]
    body_tag.scrollTop = body_tag.scrollHeight;

  sendMessageAction: (tab) ->
    textarea = tab.children('.ct-show').children('.ct-input').children('textarea')
    content = textarea.val()
    if (content == '')
      return
    content = content.replace(/\n/g, '<br/>')
    textarea.val('')
    id = tab.attr('id').substr(5)
    if (tab.attr('type') == 'person')
      if (@friends[id] != undefined)
        window.iosocket.sendMessage({id: id, username: @friends[id]['username'], content: content})
        this.addNewMessage({date: new Date(), 'tab_id': id, username: window.user['username'], avatar: window.user['avatar'], type: window.user['id'], content: content, append: 'last'})
    else
      if (@rooms[id] != undefined)
        window.iosocket.sendMessageGroup({id: id, content: content})

    body_tag = tab.children('.ct-show').children('.ctf-body:first')[0]
    body_tag.scrollTop = body_tag.scrollHeight;

  changeTabCookie: (tab) ->
    if (tab.attr('type') == 'group')
      return;
    id = tab.attr('id')
    status = 0
    if (tab.children('.ct-hide').css('display') == 'none')
      status = 1
    if (id == 'ctl-friends')
      $.docCookies.setItem('deekau_chat_system', status)
    else
      id = id.substr('5')
      this.saveTabToCookie(id, status)

  saveTabToCookie: (id, status) ->
    if (id.indexOf('_') != -1)
      return
    ck_tabs = JSON.parse($.docCookies.getItem('deekau_chat_tabs'))
    if (ck_tabs == null)
      ck_tabs = {}
    ck_tabs[id] = status
    $.docCookies.setItem('deekau_chat_tabs', JSON.stringify(ck_tabs))

  removeTabFromCookie: (id) ->
    ck_tabs = JSON.parse($.docCookies.getItem('deekau_chat_tabs'))
    if (ck_tabs == null)
      ck_tabs = {}
    delete ck_tabs[id]
    $.docCookies.setItem('deekau_chat_tabs', JSON.stringify(ck_tabs))

  removeTabChat: (id) ->
    delete @unreadmsg[id]
    delete @rooms[id]
    this.$el.children('#cts-show').children('#ctid-' + id).remove()
    this.$el.children('#cts-minimize').find('ul > li > a[ref=#ctid-'+id+']').remove()
    this.removeTabFromCookie(id)

  csAddNewFriendItem: (id, item, iscreate) ->
    tab = this.$el.children('#ctl-friends').find(id).children('.ctlfc-body')
    if (tab.find('#clfb-uid-'+ item['tab_id']).length == 0)
      if (iscreate == undefined || iscreate == true)
        html = @template_user({id: item['tab_id'], username: item['username'], avatar: item['avatar'], status: item['status']})
        tab.append(html)
    else
      tab_item = tab.find('#clfb-uid-'+ item['tab_id'])
      if (item['status'] == 'invisible')
        tab_item.children('.clfb-content').children('span').remove()
      else
        if (tab_item.find('span').length == 0)
          tab_item.children('.clfb-content').append('<span class="cs-'+item['status']+'"></span>')
        else
          tab_item.find('span').attr('class', 'cs-'+item['status'])

  uiUserConnectChange: (user, type) ->
    type = parseInt(type)
    if (type == 1 || type == 2)
      @user_online[user['id']] = type;
      #remove in offline tag
      @el_csbody.children('#ctlfc-offline').find('#clfb-uid-' + user['id']).remove()
      #add in online tag if not
      this.csAddNewFriendItem('#ctlfc-fonline', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']})
    else
      delete @user_online[user['id']]
      #remove in online tag
      @el_csbody.children('#ctlfc-fonline').find('#clfb-uid-' + user['id']).remove()
      #add in offline tag if not
      this.csAddNewFriendItem('#ctlfc-offline', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']})

    #check friend in recent tab? if in -> change it's state
    this.csAddNewFriendItem('#ctlfc-recents', {id: user['id'], tab_id: user['id'], username: @friends[user['id']]['username'], status: this.toStatus(type), avatar: @friends[user['id']]['avatar']})

    #change status in tab chat if has
    chat = this.$el.children('#cts-show').children('#ctid-'+ user['id'])
    if (chat.length != 0)
      chat.children('.ct-show').find('.ct-title').children('span').children('span').attr('class', 'cs-'+this.toStatus(type))

  toStatus: (id) ->
    id = parseInt(id)
    if (id == 1)
      type = 'available'
    else if (id == 2)
      type = 'busy'
    else if (id == 3)
      type = 'invisible'

  put_RecentChat: ->
    tag_body = this.$el.find('#ctlfc-recents > .ctlfc-body')
    _.each(@recent_chat, (key, value) ->
      if (@friends[key] != undefined)
        if (tag_body.children('#clfb-uid-'+key).length == 0)
          html = @template_user({id: key, username: @friends[key]['username'], status: this.getStatusFromID(key), avatar: @friends[key]['avatar']})
          tag_body.append(html);
    , this);

  put_Online: ->
    tag_body = this.$el.find('#ctlfc-fonline > .ctlfc-body')
    _.each(@user_online, (value, key) ->
      if (@friends[key] != undefined)
        if (tag_body.children('#clfb-uid-'+key).length == 0)
          html = @template_user({id: key, username: @friends[key]['username'], status: this.getStatusFromID(key), avatar: @friends[key]['avatar']})
          tag_body.append(html);
    , this);

  put_Offline: ->
    tag_body = this.$el.find('#ctlfc-offline > .ctlfc-body')
    _.each(@friends, (value, key) ->
      if (@user_online[key] == undefined)
        if (tag_body.children('#clfb-uid-'+key).length == 0)
          html = @template_user({id: key, username: @friends[key]['username'], status: 'invisible', avatar: @friends[key]['avatar']})
          tag_body.append(html);
    , this);

  emotionLoad: () ->
    @smiles = {
    ':) ': {'title': 'happy', 'static_image': '/img/smiles/1.png', 'dynamic_image': '/img/smiles/1.gif'},
    ':( ': {'title': 'sad', 'static_image': '/img/smiles/2.png', 'dynamic_image': '/img/smiles/2.gif'},
    ';) ': {'title': 'winking', 'static_image': '/img/smiles/3.png', 'dynamic_image': '/img/smiles/3.gif'},
    ':D ': {'title': 'big grin', 'static_image': '/img/smiles/4.png', 'dynamic_image': '/img/smiles/4.gif'},
    ';;) ': {'title': 'batting eyelashes', 'static_image': '/img/smiles/5.png', 'dynamic_image': '/img/smiles/5.gif'},
    '>:D< ': {'title': 'big hug', 'static_image': '/img/smiles/6.png', 'dynamic_image': '/img/smiles/6.gif'},
    ':-/ ': {'title': 'confused', 'static_image': '/img/smiles/7.png', 'dynamic_image': '/img/smiles/7.gif'},
    ':x ': {'title': 'love stuck', 'static_image': '/img/smiles/8.png', 'dynamic_image': '/img/smiles/8.gif'},
    ':"> ': {'title': 'blushing', 'static_image': '/img/smiles/9.png', 'dynamic_image': '/img/smiles/9.gif'},
    ':P ': {'title': 'tongue', 'static_image': '/img/smiles/10.png', 'dynamic_image': '/img/smiles/10.gif'},
    ':-* ': {'title': 'kiss', 'static_image': '/img/smiles/11.png', 'dynamic_image': '/img/smiles/11.gif'},
    '=(( ': {'title': 'broken heart', 'static_image': '/img/smiles/12.png', 'dynamic_image': '/img/smiles/12.gif'},
    ':o ': {'title': 'surprise', 'static_image': '/img/smiles/13.png', 'dynamic_image': '/img/smiles/13.gif'},
    'X( ': {'title': 'angry', 'static_image': '/img/smiles/14.png', 'dynamic_image': '/img/smiles/14.gif'},
    ':> ': {'title': 'smug', 'static_image': '/img/smiles/15.png', 'dynamic_image': '/img/smiles/15.gif'},
    'B-( ': {'title': 'cool', 'static_image': '/img/smiles/16.png', 'dynamic_image': '/img/smiles/16.gif'},
    ':-S ': {'title': 'worried', 'static_image': '/img/smiles/17.png', 'dynamic_image': '/img/smiles/17.gif'},
    '#:-S ': {'title': 'whew!', 'static_image': '/img/smiles/18.png', 'dynamic_image': '/img/smiles/18.gif'},
    '>:) ': {'title': 'devil', 'static_image': '/img/smiles/19.png', 'dynamic_image': '/img/smiles/19.gif'},
    ':(( ': {'title': 'crying', 'static_image': '/img/smiles/20.png', 'dynamic_image': '/img/smiles/20.gif'},
    ':)) ': {'title': 'laughing', 'static_image': '/img/smiles/21.png', 'dynamic_image': '/img/smiles/21.gif'},
    ':| ': {'title': 'straight face', 'static_image': '/img/smiles/22.png', 'dynamic_image': '/img/smiles/22.gif'},
    '/:) ': {'title': 'raised eyebrows', 'static_image': '/img/smiles/23.png', 'dynamic_image': '/img/smiles/23.gif'},
    '=)) ': {'title': 'rolling on the floor', 'static_image': '/img/smiles/24.png', 'dynamic_image': '/img/smiles/24.gif'},
    }
    @groupsmiles = '<table>'
    irun = 0
    open_tag = true
    _.each(@smiles, (value, key) ->
      if (irun % 6 == 0)
        if (irun == 0)
          @groupsmiles += '<tr>'
        else if (irun == @smiles.length - 1)
          @groupsmiles += '</tr>'
        else
          @groupsmiles += '</tr><tr>'
      @groupsmiles += '<td class="smileicon" title=\''+ value['title'] + '\' symbol=\'' + key + '\'><img src="' + value['static_image'] + '"/></td>'
      irun++;
    , this)
    @groupsmiles += '</table>'

  addNewMessage: (msg, type) ->
    content = this.replaceEmotions(msg['content'])
    if (type == undefined || type == 'person')
      tab = this.$el.children('#cts-show').children('#ctid-' + msg['tab_id']).find('.ctf-body .ctfb-container');
    else
      tab = this.$el.children('#cts-show').children('#ctid-' + msg['tab_id']).find('.ctf-body .ctfb-container');
    if (msg['append'] == 'last')
      finder = 'div:last'
    else
      finder = 'div:first'
    if (tab.children(finder).length == 0 || tab.children(finder).attr('class').indexOf(msg['type']) == -1)
      time = this.convertTwoDigital(msg['date'].getMinutes())
      rtime = msg['date'].getHours() - 12
      if (rtime > 0)
        time = this.convertTwoDigital(rtime)+ ':' + time + ' PM';
      else
        time = this.convertTwoDigital(msg['date'].getHours()) + ':'  + time + ' AM';
      html_msg = @template_msg({time: time, type: msg['type'], 'avatar': msg['avatar'], username: msg['username'], content: content})
      if (msg['append'] == 'last')
        tab.append(html_msg)
      else
        tab.prepend(html_msg)
    else
      if (msg['append'] == 'last')
        tab.children(finder).children('.ctfbm-body').append('<div>' + content + '</div>');
      else
        tab.children(finder).children('.ctfbm-body').prepend('<div>' + content + '</div>');

  convertTwoDigital: (time)->
    if (time < 10)
      return '0' + time
    else
      return time

  replaceEmotions: (content) ->
    patterns = []
    metachars = /[[\]{}()*+?.\\|^$\-,&#\s]/g

    _.each(@smiles, (value, key) ->
      if (@smiles.hasOwnProperty(key))
        patterns.push('('+key.replace(metachars, "\\$&")+')')
    , this)
    smiles = @smiles
    content = content.replace(new RegExp(patterns.join('|'),'g'), (match) ->
      if (smiles[match] != 'undefined')
        return '<img src="'+smiles[match]['dynamic_image']+'"/>'
      else
        return match;
    );
    return content

  getStatusFromID: (id) ->
    status = 'invisible'
    if (@user_online[id] != undefined)
      if (@user_online[id] == 1)
        status = 'available'
      else if (@user_online[id] == 2)
        status = 'busy'
    return status

  loadmoreAdd: (model, tab) ->
    log = model['log'].reverse()
    _.each(log, (value, key) ->
      type = value['sender']
      if (window.user['id'] == value['sender'])
        username = window.user['username']
        avatar = window.user['avatar']
      else
        username = @friends[value['sender']]['username']
        avatar = @friends[value['sender']]['avatar']
      this.addNewMessage({date: new Date(value['timestamp']), 'tab_id': tab.attr('id').substr(5), avatar: avatar, username: username, content: value['content'], type: type, append: 'first'})
    , this)
    date = model['date']
    date = date.split('T')[0].split('-')
    tab.find('.ctf-body .ctfb-container').prepend('<div class="ctfb-seperator" style="font-weight: bold; margin: 0px 0px -3px 4px; color: rgb(150, 150, 150);">' + date[1] + '-' + date[2]+'</div>')

  emptyView: () ->
    this.$el.find('#cth-userstatus > ul').empty().parent().append(@template_user_status({show: 1, status: 'off'}));
    this.$el.find('#cthu-status-symbol').children('span').attr('class', 'cs-off')
    this.$el.children('#cts-show').empty()
    this.$el.find('.ctlfc-body').empty()
    this.$el.find('#ctlf-result').empty()
    this.$el.find('#ctlf-ndisplay').css('display', 'block')
    this.$el.children('#cts-minimize').addClass('hide').children('ul').empty()

  lostconnect: (msg) ->
    this.$el.find('#ctlfc-friends').children('div:first').prepend('<div class="ctlfc-status clip-content">attemping to reconnect.......</div>')

  reconnect_success: (msg) ->
    this.$el.find('#ctlfc-friends').children('div:first').find('.ctlfc-status').remove()

  reconnect_fail: (msg) ->
    this.emptyView()
    this.$el.find('#ctlfc-friends').children('div:first').children('.ctlfc-status').text('disconnect to server')
    this.$el.find('#cthu-status-symbol').children('span').attr('class', 'cs-off')
    this.$el.find('#ctlf-container').find('.dropdown-menu:first').empty().append('<li type="signin"><div class="cicon cicon-online status-icon"></div><div class="status-name">Sign in</div></li>');