class Company.Views.MerchantBussinessView extends Backbone.View
  el: ".leftContent"
  template: JST['backbone/desktop/templates/merchants/bussiness']
  events:
    "click #search_merchant": "search_matches"
    "click #merchant_create": "merchant_create"
    "click #type_offer": "show_offer_type_select"
    "keypress #businessName_address": "filterOnEnter"


  initialize: () ->

  render: ->
    $(@el).append(@template())
    view = @template()
    @initTimeTemplate()
    @initMapOffer()
    @selectMerchantType()
    @initAutocomplete()
    @initShow()
    @

  initMapOffer: ->
    myOptions =
      zoom: 16
      center: new google.maps.LatLng(10.823099,106.629664)
      mapTypeId: google.maps.MapTypeId.ROADMAP
      scrollwheel: false

    map = new google.maps.Map(document.getElementById("map_offers"), myOptions)
  initShow:->
    $('#offer_type').hide()

  show_offer_type_select:->
    $('#offer_type').show()
    $('#offer_type').focus()
  search: (e) ->
    $(e.element).append(view);

#  initOpenHour: ->
#    open_hour = new Company.Views.SetTime()
#    open_hour.render()
#
#    @$('#popover')
#      .popover(
#        html : true,
#        content: ->
#          open_hour.el
#
#      )
#      .click( (e) ->
#        $('.time_picker').timepicker({
#          timeFormat:'H:i'
#        });
#        e.preventDefault()
#      )

  initTimeTemplate:->
    open_hour = new Company.Views.SetTime()
    open_hour.render()
    @$('#repeat_time')
      .popover(
        html : true,
        content: ->
          open_hour.el
      )
      .click( (e) ->
        $('.time_picker').timepicker({
        timeFormat:'H:i'
        });
        $('.type_offer_select').hide()
        e.preventDefault()
      )


  $(".match_radio").live 'click', (e) ->

    $(".detailed-data").show()
    $('#business_lat').val($(e.currentTarget).attr("lat"))
    $('#business_lng').val($(e.currentTarget).attr("lng"))
    $('#business_name').val($(e.currentTarget).attr("business_name"))
    $("#business_street").val($(e.currentTarget).attr("business_street"))
    $("#business_city").val($(e.currentTarget).attr("business_city"))
    $("#business_postal_code").val($(e.currentTarget).attr("business_postal_code"))


    myOptions =
      zoom: 16
      center: new google.maps.LatLng(parseFloat(e.currentTarget.attributes.lat.value), parseFloat(e.currentTarget.attributes.lng.value))
      mapTypeId: google.maps.MapTypeId.ROADMAP
      scrollwheel: false
    $("#map_merchants").show()
    map = new google.maps.Map(document.getElementById("map_merchants"), myOptions)
    image = "/assets/markers/pin/image.png"
    verifyMarker = new google.maps.Marker(
      map: map
      position: new google.maps.LatLng(parseFloat(e.currentTarget.attributes.lat.value), parseFloat(e.currentTarget.attributes.lng.value))
      draggable: true
      icon: image
    )
    google.maps.event.addListener(
      verifyMarker,
      'dragend',
      () ->
      #updateMarkerPosition(marker.position);
        console.log(verifyMarker.position)
        $('#business_lat').val(verifyMarker.position.lat())
        $('#business_lng').val(verifyMarker.position.lng())
    )
    e.preventDefault()

  filterOnEnter: (e) ->
    if e.keyCode != 13
      return
    @search_matches(e)
    e.preventDefault()
  search_matches: (e) ->
    console.log(@$("#businessName_address").val())
    el = $(this)

    $.ajax
      type: "POST"
      url: "/get_google_places"
      data: "content=" + @$("#businessName_address").val()
      dataType: "json"
      success: (data) ->
        console.log(data.to_s)
        map_view = new Company.Views.MerchantMapView(collection: data)
        map_view.render()

        $('#search_merchant')
          .popover(
            html: true,
            #template:'<div class="popover jhj"><div class="popover-inner"><h2 class="popover-title"></h2><div class="popover-content ghg"><div></div></div></div></div>',

            content: ->
              $("#result_business").html()
          )
        $('#search_merchant').popover('show')

      error: (e) ->

    e.preventDefault()

  select_merchant: (e) ->

    $(".detailed-data").show()
    $('#business_lat').val($(e.currentTarget).attr("lat"))
    $('#business_lng').val($(e.currentTarget).attr("lng"))
    $('#business_name').val($(e.currentTarget).attr("business_name"))
    $("#business_street").val($(e.currentTarget).attr("business_street"))
    $("#business_city").val($(e.currentTarget).attr("business_city"))
    $("#business_postal_code").val($(e.currentTarget).attr("business_postal_code"))


    myOptions =
      zoom: 16
      center: new google.maps.LatLng(parseFloat(e.currentTarget.attributes.lat.value), parseFloat(e.currentTarget.attributes.lng.value))
      mapTypeId: google.maps.MapTypeId.ROADMAP
      scrollwheel: false
    $("#map_merchants").show()
    map = new google.maps.Map(document.getElementById("map_merchants"), myOptions)
    image = "/assets/markers/pin/image.png"
    verifyMarker = new google.maps.Marker(
      map: map
      position: new google.maps.LatLng(parseFloat(e.currentTarget.attributes.lat.value), parseFloat(e.currentTarget.attributes.lng.value))
    )
    e.preventDefault()

  selectMerchantType: ->
    data = window.categories

    @$("#merchant_type").autocomplete(
      source: data
      appendTo: "#box_merchant_type"
      autoFocus: true
      select: (event, ui) ->
        id = ui.item.id
        if(id)
          $("#business_category").val(id)
          console.log(id)


    )
  merchant_create:(e) ->
    $.ajax
      type: "POST"
      url: "/merchants"
      data:
        "merchant[coordinates][]": [$("#business_lat").val(), $("#business_lng").val()]
        "merchant[category_id]": $("#offer_category_id").val()
        "merchant[name]": $("#business_name").val()
        "merchant[business_street]": $("#business_street").val()
        "merchant[business_city]": $("#business_city").val()
        "merchant[business_postal_code]": $("#business_postal_code").val()
        "merchant[mon]": $("#mon_open").attr("aria-checked")
        "merchant[tue]": $("#tue_open").attr("aria-checked")
        "merchant[wed]": $("#wed_open").attr("aria-checked")
        "merchant[thu]": $("#thu_open").attr("aria-checked")
        "merchant[fri]": $("#fri_open").attr("aria-checked")
        "merchant[sat]": $("#sat_open").attr("aria-checked")
        "merchant[sun]": $("#sun_open").attr("aria-checked")
        "merchant[local_mon_start_time]": $("#mon_start_time").val()
        "merchant[local_mon_end_time]": $("#mon_end_time").val()
        "merchant[local_tue_start_time]": $("#tue_start_time").val()
        "merchant[local_tue_end_time]": $("#tue_end_time").val()
        "merchant[local_wed_start_time]": $("#wed_start_time").val()
        "merchant[local_wed_end_time]": $("#wed_end_time").val()
        "merchant[local_thu_start_time]": $("#thu_start_time").val()
        "merchant[local_thu_end_time]": $("#thu_end_time").val()
        "merchant[local_fri_start_time]": $("#fri_start_time").val()
        "merchant[local_fri_end_time]": $("#fri_end_time").val()
        "merchant[local_sat_start_time]": $("#sat_start_time").val()
        "merchant[local_sat_end_time]": $("#sat_end_time").val()
        "merchant[local_sun_start_time]": $("#sun_start_time").val()
        "merchant[local_sun_end_time]": $("#sun_end_time").val()
      dataType: "json"
      success: (data) ->
        console.log(data.to_s)
        alert "success"
      error: (e) ->
        alert e
    e.preventDefault()

  initAutocomplete:->

    #    data = window.categories
    $("#offer_type").autocomplete({
    source:(req, add) ->
      $.getJSON "/categories/top_lvl", req, (data) ->
        add(data)
    ,
    appendTo: "#type_offer_select",
    autoFocus: true,
    select:(events, ui) ->
      $('.type_offer').attr('value',ui.item.value)
      $('#offer_category_id').attr('value',ui.item.id)
      $('#offer_type').hide()
    });
