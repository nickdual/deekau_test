class Company.Views.OffersView extends Backbone.View
  el: "#list_offers"
  template: JST['backbone/desktop/templates/merchants/drap_drop_offers']
#  events:
#    "click #search_merchant": "search_matches"
  render: ->
    $(@el).append(@template(collection: @collection))
    @
