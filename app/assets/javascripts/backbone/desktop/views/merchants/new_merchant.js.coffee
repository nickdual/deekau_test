class Company.Views.MerchantNewView extends Backbone.View
  el: "#detail_merchants"
  template: JST['backbone/desktop/templates/merchants/form_merchant']

  render: ->
    $(@el).append(@template())
    @initTimePicker()
    @initCatgories()

    @
  initTimePicker: ->
    @$('.time_picker').timepicker({
    timeFormat:'H:i'
    });


  initCatgories: ->
    $.ajax
      type: "GET"
      url: "/categories/top_lvl"
      dataType: "json"
      success: (data) ->
        $("#merchant_merchant_type_id").html(JST['backbone/desktop/templates/categories/categories'](categories: data))