class Company.Views.MerchantMapView extends Backbone.View
  el: "#result_business"
  template: JST['backbone/desktop/templates/merchants/map']
  events:
    "click .match_radio": "select_merchant"
  render: ->
    $(@el).html(@template(collection: @collection))


    @
  select_merchant: (e) ->
    console.log($(e.currentTarget).attr("lat"))
    #console.log(e.currentTarget.attributes.lat.value)
    $(".detailed-data").show()
    $('#business_lat').val($(e.currentTarget).attr("lat"))
    $('#business_lng').val($(e.currentTarget).attr("lng"))
    $('#business_name').val($(e.currentTarget).attr("business_name"))
    $("#business_street").val($(e.currentTarget).attr("business_street"))
    $("#business_city").val($(e.currentTarget).attr("business_city"))
    $("#business_postal_code").val($(e.currentTarget).attr("business_postal_code"))


    myOptions =
      zoom: 16
      center: new google.maps.LatLng(parseFloat(e.currentTarget.attributes.lat.value), parseFloat(e.currentTarget.attributes.lng.value))
      mapTypeId: google.maps.MapTypeId.ROADMAP
      scrollwheel: false
    $("#map_merchants").show()
    map = new google.maps.Map(document.getElementById("map_merchants"), myOptions)
    image = "/assets/markers/pin/image.png"
    verifyMarker = new google.maps.Marker(
      map: map
      position: new google.maps.LatLng(parseFloat(e.currentTarget.attributes.lat.value), parseFloat(e.currentTarget.attributes.lng.value))
    )
    e.preventDefault()
