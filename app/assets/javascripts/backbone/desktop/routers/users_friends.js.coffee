class Company.Routers.User_Friends extends Backbone.Router
  routes:
    'users/friends#tab/invitation': 'friend_invitation'

  initialize: () ->
    #this.route(/users\/friends#access_token=/, "hello")
    Backbone.history.navigate(window.location.pathname + window.location.search+ window.location.hash, {trigger: true, replace: false});
    $('#userinfo-friends a').click((e) ->
      id = $(this).attr('href').substr(1)
      $(this).parent().parent().children().removeClass('active')
      $(this).parent().addClass('active')
      tab = $(this).closest('#userinfo-friends').children('div:first')
      tab.children('div').removeClass('active')
      tab.children("[id='"+id+"']").addClass('active')
    )

  friend_invitation: () ->
    invitation = new Company.Views.Friends({router: this})

  hello: () ->
    params = window.location.hash.split('&')
    @token = null
    _.each(params, (value) ->
      pair = value.split('=')
      if (pair[0].indexOf('access_token') != -1)
        @token = pair[1]
    , this)
    Backbone.history.navigate('users/friends#tab/invitation', {trigger: true, replace: true});