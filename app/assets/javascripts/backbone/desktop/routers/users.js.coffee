class Company.Routers.Users extends Backbone.Router
  routes:
    "users/profile":"profile"
    "map": "map"
    "" : "index"
    "find_friend_google": "find_friend_google"
    "users/friends": "friendRoutes"

  initialize: () ->
    Backbone.history.navigate(window.location.pathname + window.location.search+ window.location.hash, {trigger: true, replace: false});

  friendRoutes: () ->
    if (@friend_routes == undefined)
      @friend_routes = new Company.Routers.User_Friends()

#  sign_up: ->
#    sign_up = new Company.Views.UsersSignUp()
#    sign_up.render()
#    #$(".signupForm.rfloat").html(sign_up.el)
#  sign_in: ->
#    sign_in = new Company.Views.UsersSignIn()
#    sign_in.render()
  map: ->
    map_view = new Company.Views.MapView()
    map_view.render()


  profile:->
    profile = new Company.Views.UsersProfile()
    profile.render()
    $('body').removeClass('home').addClass('user_profile')
