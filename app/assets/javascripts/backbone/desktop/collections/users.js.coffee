class Company.Collections.Users extends Backbone.Collection

  model: Company.Models.User
  url: '/users'
