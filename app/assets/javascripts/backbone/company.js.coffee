#= require_self
# require ./common
#= require_tree ./desktop/models
#= require_tree ./desktop/templates
#= require_tree ./desktop/views
#= require_tree ./desktop/routers
window.Company =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  init: ->
    model = Company.currentUser
    if model
      $('body').removeClass('index').addClass('home')
    else
      $('body').removeClass('home').addClass('index')
    new Company.Routers.Users()
    new Company.Routers.Merchants()
    sign_up = new Company.Views.UsersSignUp()
    sign_up.render()
    sign_in = new Company.Views.UsersSignIn()
    sign_in.render()
#    myOptions =
#      zoom: 16
#      center: new google.maps.LatLng(40.7143528, -74.00597309999999)
#      #new google.maps.LatLng(parseFloat($(this).attr("lat")), parseFloat($(this).attr("lng")))
#      mapTypeId: google.maps.MapTypeId.ROADMAP
#      scrollwheel: false
#
#    map = new google.maps.Map(document.getElementById("map_show_business"), myOptions)
#    image = "/assets/markers/pin/image.png"
#    verifyMarker = new google.maps.Marker(
#      map: map
#      position: new google.maps.LatLng(40.7143528, -74.00597309999999)
#    )
    if (window.user != undefined)
      window.chat = new Company.Views.ChatIndex;

$(document).ready ->
  $("a").click(() ->
    pag = $(this).attr("href")
    if (pag.indexOf('/') == 0)
      link = page;
    else
      link = window.location.pathname + window.location.search + $(this).attr("href");
    Backbone.history.navigate(link, {trigger: true, replace: false});
  );
  Company.init()
  Backbone.history.start({pushState: true, silent: false})

$(document).mouseup  (e) ->
    container = $(".popover")
    if container.has(e.target).length == 0
      container.hide()
      $("#map_merchants").css("margin-top", 0)
    else
      if container.parents() == $(".input-append")
        $("#map_merchants").css("margin-top", '120px')


