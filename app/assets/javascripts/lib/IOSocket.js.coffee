class IOSocket
  socket: ''
  link: '192.168.1.107'
  view: ''
  connect_status: 0
  port: '27290'

  options: {
    'connect timeout': 10000,
    'reconnect': true,
    'reconnection delay': 600,
    'max reconnection attempts': 5
    'force new connection': true
    }

  constructor: ->
    _.bindAll(this, 'userConnect', 'userDisconnect', 'receiveMessage', 'receiveStatus', 'switchtoRoom',
                    'userinRoom', 'leaveRoom', 'listen_disconnect',
                    'listen_reconnecting', 'listen_connect', 'listen_reconnect_failed')

  init_Connect: (view) ->
    @view = view
    @socket = io.connect(@link + ':' + @port, @options)
    this.setHandle()

  setHandle: ->
    @socket.on 'connect', this.listen_connect
    @socket.on 'user-connect', this.userConnect
    @socket.on 'user-disconnect', this.userDisconnect
    @socket.on 'receive-message', this.receiveMessage
    @socket.on 'receive-status', this.receiveStatus
    @socket.on 'switch-to-room', this.switchtoRoom
    @socket.on 'users-in-room', this.userinRoom
    @socket.on 'leave-room', this.leaveRoom
    @socket.on 'disconnect', this.listen_disconnect
    @socket.on 'reconnecting', this.listen_reconnecting
    @socket.on 'reconnect_failed', this.listen_reconnect_failed

  #listening event from server
  listen_reconnect_failed: (msg) ->
    @connect_status = 0
    @view.reconnect_fail(msg)

  listen_connect: (msg) ->
    if (@connect_status != 2)
      if (@connect_status == 1)
        @view.reconnect_success(msg)
      @connect_status = 2
      this.connect(window.user)

  userConnect: (user) ->
    @view.sk_userConnect(user)

  userDisconnect: (user) ->
    @view.sk_userDisconnect(user)

  receiveMessage: (msg) ->
    @view.sk_receiveMessage(msg)

  receiveStatus: (msg) ->
    @view.sk_receiveStatus(msg)

  switchtoRoom: (msg) ->
    @view.sk_switchtoRoom(msg)

  userinRoom: (msg) ->
    @view.sk_userinRoom(msg)

  leaveRoom: (msg) ->
    @view.sk_leaveRoom(msg)

  listen_reconnecting: (msg) ->

  listen_disconnect: (msg) ->
    @connect_status = 1
    @view.lostconnect(msg)

  #send event to server
  #connect
  connect: (user) ->
    if (@connect_status == 2)
      @socket.emit('turn-on', user, @view.sk_cbConnect);

  #disconnect
  disconnect: () ->
    @socket.disconnect();

  cbDisconnect: () ->


  #send message
  sendMessage: (data) ->
    if (@connect_status == 2)
      @socket.emit('send-message', data);

  #set status
  sendStatus: (data) ->
    if (@connect_status == 2)
      @socket.emit('set-status', data);

  #join room
  joinRoom: (data) ->
    if (@connect_status == 2)
      @socket.emit('join-room', data);

  sendMessageGroup: (data) ->
    if (@connect_status == 2)
      @socket.emit('send-msg-room', data);

  sendleaveRoom: (data) ->
    if (@connect_status == 2)
      @socket.emit('leave-room', data);

window.iosocket = new IOSocket()
