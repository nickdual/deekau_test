$(document).ready ->
#  hide input tag for search type offer
  $(".type_offer_select").hide()

#  define chosen for button policy offer
  $('.chosen').chosen();
  $('.chzn-search').hide()
  $('#result').hide()

#  autocomplete for search type offer
  $("#offer_type").autocomplete({
    source:(req, add) ->
      $.getJSON "/categories/top_lvl", req, (data) ->
        add(data)
    ,
    appendTo: "#type_offer_select",
    autoFocus: true,
    select:(events, ui) ->
      $('.type_offer').attr('value',ui.item.value)
      $('#offer_category_id').attr('value',ui.item.id)
      $('#offer_category_name').attr('value',ui.item.value)
      $('#offer_type').hide()
    });

#  click type button
  $('#type_offer').live 'click',  ->
    $(".type_offer_select").show()
    $('#offer_type').focus()

#    set time offer
  open_hour = new Company.Views.SetTime()
  open_hour.render()
  $('#repeat_time')
    .popover(
      html : true,
      content: ->
        open_hour.el
    )
    .click( (e) ->
      $('.time_picker').timepicker({
        timeFormat:'H:i'
      });
      $('.type_offer_select').hide()
      e.preventDefault()
  )

#    save set time

#  $('.set_time').live 'click', ->
#    $('.popover').hide()
#    $('.time_mon_start').attr('value', $('.mon_start').val())
#    $('.time_tue_start').attr('value', $('.tue_start').val())
#    $('.time_wed_start').attr('value', $('.wed_start').val())
#    $('.time_thu_start').attr('value', $('.thu_start').val())
#    $('.time_fri_start').attr('value', $('.fri_start').val())
#    $('.time_sat_start').attr('value', $('.sat_start').val())
#    $('.time_sun_start').attr('value', $('.sun_start').val())
#    $('.time_mon_end').attr('value', $('.mon_end').val())
#    $('.time_tue_end').attr('value', $('.tue_end').val())
#    $('.time_wed_end').attr('value', $('.wed_end').val())
#    $('.time_thu_end').attr('value', $('.thu_end').val())
#    $('.time_fri_end').attr('value', $('.fri_end').val())
#    $('.time_sat_end').attr('value', $('.sat_end').val())
#    $('.time_sun_end').attr('value', $('.sun_end').val())

