class window.Linkedin
  app_id: '329926527085527'
  info: null
  name: 'linkedin'

  constructor: (view)->
    _.bindAll(this, 'onLinkedInLoad', 'cb_logout', 'cb_login');
    @view = view


  onLinkedInLoad: (response) ->
    IN.Event.on(IN, "auth", @cb_login);
    IN.parse()
    $('#btn-linkedin > span > span > span').click();

  cb_login: (response) ->
    @info = response
    obj = this
    IN.API.Connections("me").result((response) ->
      friends_id = []
      obj.view.friends = {}
      _.each(response.values, (value) ->
        friends_id.push(value['id'])
        if (value['pictureUrl'] == undefined)
          value['pictureUrl'] = 'https://lh6.googleusercontent.com/-48cg63WOg_g/AAAAAAAAAAI/AAAAAAAAAAA/cWBSGTs0bJM/s96-c/photo.jpg'
        obj.view.friends[value['id']] = {username: value['firstName'] + ' ' + value['lastName'], avatar: value['pictureUrl']}
      , this)
      obj.view.cb_pvfriends({data: friends_id, 'provider': obj.name})
    );

  cb_logout: (response) ->

  login: () ->
    if (@info == null)
      IN.init({
        api_key: "1tzzmg3qvkn4",
        authorize: true,
        scope: "r_basicprofile r_contactinfo r_fullprofile r_emailaddress r_network w_messages"
      });
      IN.Event.on(IN, "frameworkLoaded", @onLinkedInLoad);
    else
      #call login custom linkedin button
      #$('#btn-linkedin > span > span > span').click();
      @cb_login()