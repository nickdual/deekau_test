class window.Facebook
  name: 'facebook'
  config: {
    appId: '329926527085527'
    status: true
    cookie: true
    xfbml: true
  }
  info: null
  
  constructor: (view)->
    _.bindAll(this, 'cb_login', 'cb_logout', 'cb_getStatus');
    @view = view

  setHandle: ->
    FB.Event.subscribe('auth.login', @cb_login);
    FB.Event.subscribe('auth.logout', @cb_logout);


  cb_login: (response) ->
    @info = response
    obj = this
    obj.view = @view
    FB.api('/me/friends', (response) ->
      friends_id = []
      obj.view.friends = {}
      _.each(response.data, (value) ->
        friends_id.push(value['id'])
        obj.view.friends[value['id']] = {username: value['name'], avatar: 'https://graph.facebook.com/'+value['id']+'/picture'}
      , this)
      obj.view.cb_pvfriends({data: friends_id, 'provider': obj.name})
    );

  cb_logout: (response) ->

  cb_getStatus: (response) ->
    @info = response
    if (@info.status == 'connected')
      @cb_login(@info)
    else
      FB.login(@cb_login, {scope: 'email,user_birthday,read_friendlists, publish_stream, xmpp_login, offline_access'})

  login: () ->
    if (@info == null)
      FB.init(@config);
      @setHandle()
    FB.getLoginStatus(@cb_getStatus);

