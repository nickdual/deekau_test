class window.Google
  name: 'google_oauth2'
  config: {
    client_id: '1074400486561.apps.googleusercontent.com',
    scope: 'https://www.google.com/m8/feeds/'
    immediate: true
  }
  api_key: 'AIzaSyBvlWeBkDTCU3tZzKTG1oU6RAZQzWZYtso'
  info: null

  constructor: (view)->
    _.bindAll(this, 'cb_getStatus')
    @view = view

  login: () ->
    if (@info == null)
      gapi.client.setApiKey(@api_key);
      object = this
      window.setTimeout(()->
        gapi.auth.authorize(object.config, object.cb_getStatus);
      ,1);
    else
      @cb_getStatus()

  cb_getStatus: (response) ->
    if (response == null)
      obj = this
      window.setTimeout(()->
        gapi.auth.authorize({client_id: obj.config.client_id, scope: obj.config.scope, immediate: false}, obj.cb_getStatus);
      , 1)
      return
    @info = response
    authParams = gapi.auth.getToken()
    authParams.alt = 'json';
    authParams['max-results'] = 10000
    obj = this
    $.ajax({
      url: 'https://www.google.com/m8/feeds/contacts/default/full',
      dataType: 'json',
      data: authParams,
      success: (response) ->
        friends_id = []
        obj.view.friends = {}
        email = response.feed.id['$t']
        _.each(response.feed.entry, (value) ->
          name = ''
          _.each(value['title'], (value) ->
            if (value != '' && value != 'text')
              name = value
          )
          _.each(value['gd$email'], (value) ->
            if (value['address'] != email)
              friends_id.push(value['address'])
              if (name == '')
                obj.view.friends[value['address']] = {username: value['address']}
              else
                obj.view.friends[value['address']] = {username: name}
              obj.view.friends[value['address']]['avatar'] = 'https://lh6.googleusercontent.com/-48cg63WOg_g/AAAAAAAAAAI/AAAAAAAAAAA/cWBSGTs0bJM/s96-c/photo.jpg'
          )

        , this)
        obj.view.cb_pvfriends({data: friends_id, 'provider': obj.name})
    });