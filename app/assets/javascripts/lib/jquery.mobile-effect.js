(function($){
    $.fn.change_screen = function(options) {
        var defaults = {
            speed: 300,
            effect: 'slide-in',
            remove: false,
            begin: function(){},
            finish: function(){}
        };
        var opts = $.extend(defaults, options);
        $(document).on('click', this.selector, function() {
            var obj = $(this)
            opts.begin.call(obj, obj)
            var des = $(obj.attr('ref'))
            var src = obj.closest('.mt-screen')
            if (opts.effect == 'slide-in') {
                des.css({'top': '0', 'left': '100%', 'display': 'block'});
                src.animate({'left': '-100%'}, opts.speed, function() {
                    src.css('display', 'none');
                    if (opts.remove == true)
                        src.remove();
                    opts.finish.call(obj, obj)
                })
                des.animate({'left': '0'}, opts.speed)
            }
            else if (opts.effect == 'slide-out') {
                des.css({'top': '0', 'left': '-100%', 'display': 'block'});
                src.animate({'left': '100%'}, opts.speed, function() {
                    src.css('display', 'none');
                    if (opts.remove == true)
                        src.remove();
                    opts.finish.call(obj, obj)
                })
                des.animate({'left': '0'}, opts.speed)
            }
        })
    }
    $.fn.show_dialog = function(options) {
        var defaults = {
            speed: 300,
            corner: 'all',
            id: 'msdid-' + Math.random().toString(36).substr(2, 8),
            parent: 'body',
            header_color: 'green',
            header_title: 'Dialog Box',
            effect_in: 'slide',
            effect_out: 'slide',
            direction_in: 'top',
            direction_out: 'top',
            btn_submit: null,
            begin: function(){},
            finish: function(){}
        };
        var opts = $.extend(defaults, options);
        var html =
                '<div class="mfullscreen ms-dialog" id="msdid-' + opts.id + '">' +
                    '<div class="msd-foreground mfullscreen"></div>' +
                    '<div class="mb-dialog mui-corner-'+opts.corner+'">' +
                        '<div class="md-header mui-corner-top mui-bg-'+opts.header_color+'">' +
                            '<a class="mi-close micon" style="position: absolute; right: 12px;"></a>' +
                            '<span>'+opts.header_title+'</span>' +
                        '</div>' +
                        '<div id="'+opts.id+'" class="md-body">' +
                        '</div>' +
                        '<div class="md-footer">' +
                            '<div class="mdf-submit mui-corner-all mui-btn-gray mi-btn-shadow">Submit</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        var el = this;
        function close_dialog(obj) {
            obj.children('.msd-foreground').animate({opacity: 0}, opts.speed + 100);
            var params = {}
            if (opts.effect_out == 'blur') {
                params['opacity'] = '0'
            }
            else if (opts.effect_out == 'slide') {
                params['opacity'] = 0.75;
                params[opts.direction_out] = '-100%'
            }
            obj.children('.mb-dialog').animate(params, opts.speed, function() {
                obj.remove();
            })
        }

        function show_dialog() {
            var obj = $(opts.parent).children('.ms-dialog');
            obj.css('display', 'block');
            var params = {opacity: 1};
            if (opts.effect_in == 'blur') {
                obj.children().css('opacity', '0');
            }
            else if (opts.effect_in == 'slide') {
                var in_p = {}
                in_p['margin-'+opts.direction_in] = '-100%';
                obj.children('.mb-dialog').css(in_p)
                params['margin'] = '10%'
            }
            obj.children('.msd-foreground').animate({opacity: 1}, opts.speed);
            setTimeout(function() {
                obj.children('.mb-dialog').animate(params, opts.speed);
            }, 100);
        }

        $(document).on('click', '#msdid-' + opts.id, function() {
            close_dialog($(this))
        });

        $(document).on('click', '#msdid-' + opts.id + ' .mb-dialog', function(e) {
            e.stopPropagation();
        });

        $(document).on('click', '#msdid-' + opts.id + ' .md-footer > .mdf-submit', function() {
            if (opts.btn_submit != null)
                opts.btn_submit.call(el, {currentTarget: el.selector, dialog: '#'+opts.id})
            close_dialog($(this).closest('.ms-dialog'))
        });

        $(document).on('click', '#msdid-' + opts.id + ' .mi-close', function() {
            close_dialog($(this).closest('.ms-dialog'))
        });

        $(document).on('click', this.selector, function() {
            $(opts.parent).append(html)
            opts.begin.call(el, {currentTarget: el.selector, dialog: '#'+opts.id})
            show_dialog()
            opts.finish.call(el)
        })
    }

    $.fn.popover_arrow = function(options) {
        var defaults = {
            id: 'popid-' + Math.random().toString(36).substr(2, 8),
            show: false,
            begin: function(){},
            finish: function(){}
        };
        var opts = $.extend(defaults, options);
        var el = this;
        var pos = $(this).position()
        var size = {height: $(this).outerHeight(true), width: $(this).outerWidth(true)}

        var html =
            '<div class="triangle-border popover_arrow" id="' + opts.id + '" style="display: none; position: absolute; font-size: 12px">' +
            '</div>';

        $(document).on('click', this.selector, function() {
            var popover = $('#' + opts.id)
            if (popover.length == 0) {
                popover = $('#' + opts.id);
                $(el).parent().prepend(html)
                put_data(popover);
            }
            else
                if (opts.show == true) {
                    popover.empty()
                    put_data(popover);
                }
                else
                    popover.remove()
        });
        function put_data(popover) {
            opts.begin.call(el, {currentTarget: el.selector, element: '#'+opts.id})
            popover = $('#' + opts.id);
            var type = 'top_left';
            if (pos.left >= $(window).width() / 2)
                type = 'top'
            popover.addClass(type)
            if (type == 'top')
                popover.css({'display': 'block', 'top': pos.top + size.height + 12, 'left': pos.left + size.width/2 + 12 - popover.width()})
            else
                popover.css({'display': 'block', 'top': pos.top + size.height + 12, 'left': pos.left + size.width/2 - 12})
        }

    }

    $.json_length = function(obj) {
        var length = 0;
        _.each(obj, function() {
            length++
        })
        return length;
    }

    $.docCookies = {
        getItem: function (sKey) {
            if (!sKey || !this.hasItem(sKey)) { return null; }
            return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"));
        },
        setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return; }
            var sExpires = "";
            if (vEnd) {
                switch (vEnd.constructor) {
                    case Number:
                        sExpires = vEnd === Infinity ? "; expires=Tue, 19 Jan 2038 03:14:07 GMT" : "; max-age=" + vEnd;
                        break;
                    case String:
                        sExpires = "; expires=" + vEnd;
                        break;
                    case Date:
                        sExpires = "; expires=" + vEnd.toGMTString();
                        break;
                }
            }
            document.cookie = escape(sKey) + "=" + escape(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
        },
        removeItem: function (sKey, sPath) {
            if (!sKey || !this.hasItem(sKey)) { return; }
            document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sPath ? "; path=" + sPath : "");
        },
        hasItem: function (sKey) {
            return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
        },
        keys: /* optional method: you can safely remove it! */ function () {
            var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
            for (var nIdx = 0; nIdx < aKeys.length; nIdx++) { aKeys[nIdx] = unescape(aKeys[nIdx]); }
            return aKeys;
        }
    };
})(jQuery);