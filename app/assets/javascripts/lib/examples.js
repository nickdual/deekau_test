$(function () {

  $('textarea.mention').mentionsInput({
    onDataRequest:function (mode, query, callback) {
        var data = [
            { id:1, name:'eat',  'type':'type' },
            { id:2, name:'Drink',  'type':'type' },
            { id:3, name:'Fun',  'type':'type' },
            { id:4, name:'Music',  'type':'type' },
            { id:5, name:'Fastion',  'type':'type' },
            { id:6, name:'Film',  'type':'type' }
        ];

      data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

      callback.call(this, data);
    }
  });
});