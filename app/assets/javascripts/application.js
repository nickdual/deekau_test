// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.

//= require_self
//= require jquery
//= require jquery_ujs

//= require less-1.3.0.min.js
//= require tinymce
//= require lib/deekau
//= require ./lib/iphone-style-checkboxes
//= require ./lib/connect_find_friend
//= require lib/jquery.validate
//= require i18n
//= require i18n/translations
//= require ./lib/webcam
//= require lib/json2
//= require lib/socket.io
//= require underscore
//= require backbone
//= require lib/jquery.autosize
//= require lib/antiscroll
//= require lib/IOSocket
//= require_tree ./lib/oauth
//= require lib/chosen
//= require lib/jquery.mobile-effect
//= require ./lib/jquery.events.input
//= require ./lib/jquery.elastic
//= require ./lib/jquery.mentionsInput

//= require ./lib/jquery_file_upload/jquery.ui.widget.js
//= require ./lib/jquery_file_upload/tmpl.min.js
//= require ./lib/jquery_file_upload/load-image.min.js
//= require ./lib/jquery_file_upload/canvas-to-blob.min.js
//= require ./lib/jquery_file_upload/jquery.iframe-transport.js
//= require ./lib/jquery_file_upload/jquery.fileupload.js
//= require ./lib/jquery_file_upload/jquery.fileupload-fp.js
//= require ./lib/jquery_file_upload/jquery.fileupload-ui.js
//= require ./lib/jquery_file_upload/locale.js
//= require ./lib/jquery_file_upload/main.js


//= require lib/jquery-ui-1.9.1.custom.min.js
//= require lib/bootstrap-tooltip
//= require lib/bootstrap-popover

//= require ./lib/jquery.timepicker.min
//= require ./lib/new_offer

//= require ./lib/bootstrap-carousel
//= require ./lib/bootstrap-modal
//= require ./lib/bday-picker

//= require backbone/company
