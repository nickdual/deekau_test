source 'https://rubygems.org'

gem 'rails', '3.2.2'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'
#Fetch data from geonames.org and make the required models
#gem "geonames_rails", "~> 0.2.2"

# Automatically detect mobile devices that access your Rails application
gem "mobile-fu", "~> 1.1.0"

# HTML, XML, SAX, and Reader parse
gem "nokogiri", "~> 1.5.5"

# This gem provides a simple and extremely flexible way to upload files from Ruby applications. It works well with Rack based web applications, such as Ruby on Rails.

gem "carrierwave-mongoid", :git => "git://github.com/jnicklas/carrierwave-mongoid.git", :branch => "mongoid-3.0" ,:require => 'carrierwave/mongoid'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer'

  gem 'uglifier', '>= 1.0.3'
end

gem 'haml'
gem 'less-rails'
gem 'less-rails-bootstrap'

gem "jquery-rails", "~> 2.1.3"

gem "bson_ext", "~> 1.7.0"

gem "mongoid", "~> 3.0.11"

gem "devise", "~> 2.1.2"

gem 'cancan'
# An extension that becomes possible use paginate method with Mongoid
gem "will_paginate_mongoid", "~> 1.1.0"

# RMagick is an interface between Ruby and ImageMagick.
gem "rmagick", "~> 2.13.1"

# Mongoid backend for delayed_job
gem "delayed_job_mongoid", "~> 2.0.0"

# Rack middleware for creating HTTP endpoints for files stored in MongoDB's GridFS
gem "rack-gridfs", "~> 0.4.1"

# This gem integrates Plupload with the Rails 3.x asset pipeline. It's been tested with Rails 3.1 and 3.2.
gem 'plupload-rails'

# Simple full text search implementation
gem "mongoid_search", "~> 0.3.0"

# Geokit provides geocoding and distance calculation in an easy-to-use API
gem "geokit", "~> 1.6.5"

# Provides object geocoding (by street or IP address), reverse geocoding (coordinates to street address),
# distance queries for ActiveRecord and Mongoid, result caching, and more.
# Designed for Rails but works with Sinatra and other Rack frameworks too.
gem "geocoder", "~> 1.1.4"

# The Mechanize library is used for automating interaction with websites.
#Mechanize automatically stores and sends cookies, follows redirects, and can follow links and submit forms.
#Form fields can be populated and submitted.
#Mechanize also keeps track of the sites that you have visited as a history.
gem "mechanize", "~> 2.5.1"

gem 'tinymce-rails'
gem 'therubyracer'

gem 'daemons'
gem 'rufus-scheduler'
gem "rmagick", "~> 2.13.1"
group :development,:test do
  gem "rspec-rails"
  gem "factory_girl_rails",:require => false
  gem 'guard-spork'
  gem "spork", "~> 0.9.2"
end
group :test do
  gem "capybara"
  gem 'cucumber-rails' ,:require => false
  gem 'database_cleaner'
  gem "launchy"   # save and open page
  #gem 'webrat'
  gem 'selenium-webdriver'
  gem "mongoid-rspec"


end

gem "omniauth", "~> 1.1.0"
gem 'omniauth-linkedin'
gem 'omniauth-yammer'
gem 'omniauth-openid'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem 'omniauth-google-oauth2'
gem 'oauth'
gem "oauth2", "~> 0.6.1"

gem "koala"
gem "linkedin"
gem 'gmail_xoauth'
#gem "httpclient", "~> 2.3.0.1"
#gem "gdata", "~> 1.1.2",:require => 'jcode' if RUBY_VERSION < '1.9'

gem "backbone-on-rails"
gem "i18n", "~> 0.6.0"
gem "i18n-js", "~> 2.1.2"
#gem 'i18n_data'
#gem "rspec-rails", ">= 2.9.0.rc2", :group => [:development, :test]
#gem "database_cleaner", ">= 0.7.2", :group => :test
#gem "mongoid-rspec", ">= 1.4.4", :group => :test
#gem "factory_girl_rails", ">= 3.0.0", :group => [:development, :test]
#gem "email_spec", ">= 1.2.1", :group => :test
#gem "cucumber-rails", ">= 1.3.0", :group => :test
#gem "capybara", ">= 1.1.2", :group => :test
#gem "launchy", ">= 2.1.0", :group => :test

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'ruby-debug19', :require => 'ruby-debug'

