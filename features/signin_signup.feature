@javascript

Feature: Sign in / Sign up
  I am a user, i want to sign in with my account
  or i am not yet a user and i want to register a account
  Scenario: Sign in with valid account
    Given user exits
    Then I am on home page
    And I fill in "user_email" with "abc123@gmail.com"
    And I fill in "user_password" with "123456"
    When I click "Login"
  Scenario: Sign in with invalid account
    Given user exits
    Then I am on home page
    And I fill in "user_email" with "abc1@gmail.com"
    And I fill in "user_password" with "123456"
    When I click "Login"
    Then A alert error show



  Scenario: Sign up with valid attributes
    Given I am on home page
    And I fill in "user_user_name" with "abc123"
    And I fill in "user_email" with "abc123@gmail.com"
    And I fill in "user_email_confirmation" with "abc123@gmail.com"
    And I fill in "user_password" with "123456"
    And I press button "Sign up"
    Then A popup show success

  Scenario: Sign up with invalid attributes
    Given I am on home page
    And I fill in "user_user_name" with "abc123"
    And I fill in "user_email" with ""
    And I fill in "user_email_confirmation" with ""
    And I fill in "user_password" with ""
    And I press button "Sign up"
    Then A alert error show
