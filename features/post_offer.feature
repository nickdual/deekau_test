@javascript

Feature: post_offer
  I am a user, i want to post an offer to wall

  Scenario: Post offer successfull
#    Given user exits
#    And I am on home page
#    And I fill in "user_email" with "abc123@gmail.com"
#    And I fill in "user_password" with "123456"
#    And I click "Login"
    Given I go to 'offer page'
    And I fill in "Offer_description" with "Offer_description"
    And I attach the file "./public/images/test.png" to "fileinput"
    When I click "type_offer"
    Then I fill in "offer_type" with "Dr"
    And I click "Drink" in autocomplete results
    And I click "repeat_time"
    And I click "gwt-uid-mon"
    And I click "Set"
    And I click "Post"

  Scenario: Post offer unsuccessfull with not set type
    Given I go to 'offer page'
    And I fill in "Offer_description" with "Offer_description"
    And I attach the file "./public/images/test.png" to "fileinput"
    And I click "repeat_time"
    And I click "gwt-uid-mon"
    And I click "Set"
    And I click "Post"
    And I get a error with "type offer not set"


  Scenario: Post offer unsuccessfull with not description
    Given I go to 'offer page'
    And I fill in "Offer_description" with ""
    And I attach the file "./public/images/test.png" to "fileinput"
    And I click "repeat_time"
    And I click "gwt-uid-mon"
    And I click "Set"
    And I click "Post"
    And I get a error with "description is empty"

  Scenario: Post offer unsuccessfull with type image wrong
    Given I go to 'offer page'
    And I fill in "Offer_description" with "Offer_description"
    And I attach the file "./public/images/test.txt" to "fileinput"
    And I click "repeat_time"
    And I click "gwt-uid-mon"
    And I click "Set"
    And I click "Post"
    And I get a error with "image type is wrong"

