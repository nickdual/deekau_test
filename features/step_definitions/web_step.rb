require 'uri'
require 'cgi'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "selectors"))

module WithinHelpers
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
end
World(WithinHelpers)


Given /^user exits$/ do
  @user = FactoryGirl.create(:user)
end

When /^I am in merchant admin page$/ do
  visit '/users/1'
end

Then /^I redirect to merchant admin page$/ do
  visit '/users/1'
end



Then /^show me the page$/ do
  save_and_open_page
end

Then /^I should see an form$/ do
  #page.should have_css('div.addUser_subsection', :visible => false)
  find_link('Add user').visible?
  page.has_no_selector?('div.addUser_subsection')
end

Then /^I should see an form edit user$/ do
  find_link('edit_user_admin').visible?
  page.has_no_selector?('div.addUser_subsection')
end


Then /^A new user should be created and show in users table\.$/ do
  #save_and_open_page
end

#When /^I reload the page$/ do
#  visit current_path
#end
Then /^wait$/ do
  sleep 2
end


When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end



# Single-line step scoper
When /^(.*) within (.*[^:])$/ do |step, parent|
  with_scope(parent) { When step }
end

# Multi-line step scoper
When /^(.*) within (.*[^:]):$/ do |step, parent, table_or_string|
  with_scope(parent) { When "#{step}:", table_or_string }
end
Given /^(?:|I )am on sign in$/ do
  post '/users/sign_in'
end

Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
Given /^(?:|I )am on merchant admin$/ do
  visit user_path(user)
end

When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end

When /^(?:|I )should go to (.+)$/ do |page_name|
  visit path_to(page_name)
end

When /^I press button "([^"]*)"$/ do |button|
  click_button(button)

end
When /^(?:|I )press "([^"]*)"$/ do |button|
  click_link(button)

end
When /^(?:|I )click "([^"]*)"$/ do |button|
  click_on(button)

end
When /^(?:|I )follow "([^"]*)"$/ do |link|
  click_link(link)
end

When /^(?:|I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, :with => value)
end

When /^(?:|I )fill in "([^"]*)" for "([^"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end

# Use this to fill in an entire form with data from a table. Example:
#
#   When I fill in the following:
#     | Account Number | 5002       |
#     | Expiry date    | 2009-11-01 |
#     | Note           | Nice guy   |
#     | Wants Email?   |            |
#
# TODO: Add support for checkbox, select og option
# based on naming conventions.
#
When /^(?:|I )fill in the following:$/ do |fields|
  fields.rows_hash.each do |name, value|
    When %{I fill in "#{name}" with "#{value}"}
  end
end

When /^(?:|I )click "([^"]*)" in autocomplete results$/ do |target|
  page.execute_script %Q{ $('.ui-menu-item a:contains("#{target}")').trigger("mouseenter").click(); }
end

When /^(?:|I )select "([^"]*)" from "([^"]*)"$/ do |value, field|
  select(value, :from => field)
end

When /^(?:|I )check "([^"]*)"$/ do |field|
  check(field)
end

When /^(?:|I )uncheck "([^"]*)"$/ do |field|
  uncheck(field)
end

When /^(?:|I )choose "([^"]*)"$/ do |field|
  choose(field)
end

When /^(?:|I )attach the file "([^"]*)" to "([^"]*)"$/ do |path, field|
  attach_file(field, File.expand_path(path))
end
#When /^I attach the file at "([^\"]*)" to "([^\"]*)"$/ do |path, field|
#  attach_file(field, path)
#end

Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

Then /^(?:|I )should see \/([^\/]*)\/$/ do |regexp|
  regexp = Regexp.new(regexp)

  if page.respond_to? :should
    page.should have_xpath('//*', :text => regexp)
  else
    assert page.has_xpath?('//*', :text => regexp)
  end
end

Then /^(?:|I )should not see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_no_content(text)
  else
    assert page.has_no_content?(text)
  end
end

Then /^(?:|I )should not see \/([^\/]*)\/$/ do |regexp|
  regexp = Regexp.new(regexp)

  if page.respond_to? :should
    page.should have_no_xpath('//*', :text => regexp)
  else
    assert page.has_no_xpath?('//*', :text => regexp)
  end
end

Then /^the "([^"]*)" field(?: within (.*))? should contain "([^"]*)"$/ do |field, parent, value|
  with_scope(parent) do
    field = find_field(field)
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    if field_value.respond_to? :should
      field_value.should =~ /#{value}/
    else
      assert_match(/#{value}/, field_value)
    end
  end
end

Then /^the "([^"]*)" field(?: within (.*))? should not contain "([^"]*)"$/ do |field, parent, value|
  with_scope(parent) do
    field = find_field(field)
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    if field_value.respond_to? :should_not
      field_value.should_not =~ /#{value}/
    else
      assert_no_match(/#{value}/, field_value)
    end
  end
end

Then /^the "([^"]*)" checkbox(?: within (.*))? should be checked$/ do |label, parent|
  with_scope(parent) do
    field_checked = find_field(label)['checked']
    if field_checked.respond_to? :should
      field_checked.should be_true
    else
      assert field_checked
    end
  end
end

Then /^the "([^"]*)" checkbox(?: within (.*))? should not be checked$/ do |label, parent|
  with_scope(parent) do
    field_checked = find_field(label)['checked']
    if field_checked.respond_to? :should
      field_checked.should be_false
    else
      assert !field_checked
    end
  end
end

Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end

Then /^(?:|I )should have the following query string:$/ do |expected_pairs|
  query = URI.parse(current_url).query
  actual_params = query ? CGI.parse(query) : {}
  expected_params = {}
  expected_pairs.rows_hash.each_pair{|k,v| expected_params[k] = v.split(',')}

  if actual_params.respond_to? :should
    actual_params.should == expected_params
  else
    assert_equal expected_params, actual_params
  end
end



Then /^A confirm message show for me$/ do
  page.driver.browser.switch_to.alert.accept
end


Then /^A popup show success$/ do
  alert = page.driver.browser.switch_to.alert;
  alert.accept;
end


Then /^A error message will show for me because the user which i want to delete is myself$/ do
  page.driver.browser.switch_to.alert.accept
end


Then /^That user was delete$/ do
  pending # express the regexp above with the code you wish you had
end



Then /^A error message will show for me if i am not admin or user which i want to delete is myself$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see a error message$/ do
  find('#modal_error').has_content?("can't be empty")|| find('#modal_error').has_content?("can't be blank")
end

When /^I confirm popup$/ do
  page.driver.browser.switch_to.alert.accept
end

When /^I dismiss popup$/ do
  page.driver.browser.switch_to.alert.dismiss
end


When /^I get a error with "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_no_content(text)
  else
    assert page.has_no_content?(text)
  end
end
Then /^I should see a validate message$/ do
  find('#flash_alert').has_content?("Invalid email or password")
end
Then /^A alert error show/ do
  page.driver.browser.switch_to.alert.accept
end

