require 'rufus/scheduler'  # Need this to make use of Rufus::Scheduler

require 'rubygems'   # Need this to make use of any Gem, in our case it is rufus-scheduler

require 'rake'     # Need this to make use of Rake::Task

#load File.join(Rails.root.to_s, 'lib', 'tasks', 'tmp.rake')

temp_files_cleaning_scheduler = Rufus::Scheduler.start_new
# Making use of the syntax used in Crontab

temp_files_cleaning_scheduler.every '5s' do
  puts '12345'
#  task = Rake::Task["tempfile:delete_all"]
#
#  task.reenable  # If only you do this, will your rake task run the next time you invoke it.
#
#  task.invoke
end