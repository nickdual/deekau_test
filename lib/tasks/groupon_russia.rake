# encoding: utf-8
def get_groupon_detail_russia(link_detail,tag_type)
  
  link_detail = (link_detail[0] == '/')? "http://www.groupon.ru" + link_detail : link_detail
  puts link_detail
  b = Mechanize.new
  b.gzip_enabled = false
  b.ignore_bad_chunking = true
  b.keep_alive = false
  b.get(link_detail)
  name = b.page.search(".body").blank? ? nil : b.page.search(".body").first.search("h1").first.text
  puts 'name'
  pr_dis_cur = b.page.search(".info").blank? ? nil : b.page.search(".info").first.search("tr")[1]
  if !pr_dis_cur.blank?
    price = pr_dis_cur.search("td")[0].blank? ? nil : get_price_russia(pr_dis_cur.search("td")[0].text.squish)
    currency = pr_dis_cur.search("td")[0].blank? ? nil : pr_dis_cur.search("td")[0].text.squish[-4..-1]
    discount = pr_dis_cur.search("td")[1].blank? ? nil : pr_dis_cur.search("td")[1].text.gsub("%"," ").squish
  else
    price = b.page.search(".price").blank? ? nil : get_price_russia(b.page.search(".price").first.search("strong").first.text.squish)
    currency = b.page.search(".price").blank? ? nil : b.page.search(".price").first.search("span").first.text.squish
    discount = nil
  end

  if (!discount.blank? && !discount[0..1].to_f.in?(1..9))
    discount = discount[3..99].to_f
  end


  puts 'price currency discount'
  des = b.page.search(".description").blank? ? nil : b.page.search(".description").first.text.squish
  puts 'des'
  phone = b.page.search(".phones").blank? ? nil : b.page.search(".phones").first.search("span").first
  if !phone.blank?
    phone = phone.text.squish
  else
    phone = b.page.search(".phones").text.squish.split(":")[1]
  end
  puts 'phone'
  address = b.page.search(".address").blank? ? nil : b.page.search(".address").first.text.squish
  puts 'address'
  city = nil
  end_time = b.page.search(".time").blank? ? nil : b.page.search(".time").first.search("strong")
  if !end_time.blank?
    end_time = end_time.text.squish.split(":")[0].to_i
  else
    end_time = 10
  end
  puts 'endtime'
  link_temp = (des.blank?)? nil : b.page.search(".description").first.search("a").first
  if !link_temp.blank?
    link = link_temp.attributes["href"].value
    link_text = link_temp.text.squish
  else
    link_text = nil
  end
  puts 'link'
  mer_name = name
  mer_description = des
  mobile_phone = nil
  
  @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
  @offer = create_offer(@merchant,link,name,des,end_time,price,discount,currency,address,phone,mobile_phone)

  create_image(@merchant,b.page.search(".slideshow li img").first.attributes["src"].value)
  b.page.search(".slideshow li img").each do |i|
    puts image = i.attributes["src"].value
    image = (image[0]=='/')? link_detail +  image : image
    create_image(@offer,image)

  end
  puts
  puts 

end

def get_groupon_russia(link,mer_type_id)
  
  link = (link[0] == '/')? "http://www.groupon.ru" + link : link
  b = Mechanize.new
  b.gzip_enabled = false
  b.ignore_bad_chunking = true
  b.keep_alive = false
  b.get(link)
  b.page.search(".line").search(".offer").each do |offer|
    get_groupon_detail_russia(offer.search("a").first.attributes["href"].value,mer_type_id)
  end

end


# 
def get_groupon_city_russia(a,link_city,mer_type_good,mer_type_gateways,mer_type_concert)
  puts 'get groupon cities'
  a.get(link_city)
  
  # AFTER GOTO CITY, GET LINK MENU TYPE MERCHANT , GET GROUPON
  puts 'get groupon goods'
  get_groupon_russia(a.page.search(".shopping a").first.attributes["href"].value,mer_type_good) if !a.page.search(".shopping").blank?
  puts 'get groupon gateways'
  get_groupon_russia(a.page.search(".travel a").first.attributes["href"].value,mer_type_gateways) if !a.page.search(".travel").blank?
  puts 'get groupon concert'
  get_groupon_russia(a.page.search(".concert a").first.attributes["href"].value,mer_type_concert) if !a.page.search(".concert").blank?
end




desc "GET GROUPON RUSSIA"
task :groupon_russia => :environment do
  
  require 'rubygems'
  require 'mechanize'
  mer_type_good = ['good']
  mer_type_gateways = ['gateway']
  mer_type_concert = ['concert']


  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  page_cities = []
  a.get("http://www.groupon.ru/piter")
  a.page.search(".cities").first.search("li a").each do |city|
    page_cities << city.attributes["href"].value
  end

  # GET ALL CITIES LINK and other type
  page_cities.each do |link_city|
    get_groupon_city_russia(a,link_city,mer_type_good,mer_type_gateways,mer_type_concert)
  end

end
