# encoding: utf-8
def get_detail_groupon_china(groupon,tag_type)
  link_detail = groupon.attributes["href"].value
  puts link_detail
  c = Mechanize.new
  c.get(link_detail)
  
  mer_name = groupon.text
  name = mer_name
  puts 'mer name'
  price = c.page.search(".price_b").blank? ? nil : get_price(c.page.search(".price_b").first.search("del").first.text[1..99].squish)
  puts 'price'
  currency = c.page.search(".price_b").blank? ? nil : get_price(c.page.search(".price_b").first.search("del").first.text.squish[0])
  puts 'currency'
  end_time = c.page.search(".time_box").blank? ? nil : c.page.search(".time_box").text.squish.split(' ')[0]
  end_time = end_time.blank? ? 10 : end_time[0..end_time.length-2].to_i
  puts 'endtime'
  offer_description = c.page.search(".d_title").blank? ? nil : c.page.search(".d_title").first.text
  puts 'offer_description'
  arr_address = c.page.search(".list_address").blank? ? nil : c.page.search(".list_address dd")
  phone = arr_address[0].search("em").first.text.squish
  puts 'phone'
  address = arr_address[1].search("em").first.text.squish
  puts 'address'
  link = nil
  link_text = nil
  city = nil
  discount = 0
  mer_description = offer_description
  mobile_phone = nil
  
  @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
  @offer = create_offer(@merchant,link,name,offer_description,end_time,price,discount,currency,address,phone,mobile_phone)
   puts image = c.page.search(".post_content").search('img').first.attributes['src'].value
   create_image(@offer, image)
   create_image(@merchant, image)
end

def get_groupon_china(page_link,tag_type)
  require 'rubygems'
  require 'mechanize'
  
  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false


  a.get(page_link)
  
  # PAGE DESIGN HAVE 2 TYPE SHOW GROUPON 
  a.page.search(".post").each do |feature_groupon|
    get_detail_groupon_china(feature_groupon.search("a").first,tag_type)
  end
  a.page.search(".listpost").each do |feature_groupon|
    get_detail_groupon_china(feature_groupon.search("a").first,tag_type)
  end
  
end




desc "GET GROUPON CHINA"
task :groupon_china => :environment do
  mer_type_good = ['good']
  mer_type_gateways = ['gateway']
  mer_type_game = ['game']
  mer_type_famous = ['famous']

  # PAGE HAVE 4 MENU - 4 TYPE MERCHANT 
  get_groupon_china("http://www.gaopeng.com/deals/quanguo?ADTAG=quanguo_from_quanguo",mer_type_good)
  get_groupon_china("http://www.gaopeng.com/deals/travel?ADTAG=travel_from_quanguo",mer_type_gateways)
  get_groupon_china("http://www.gaopeng.com/deals/game?ADTAG=game_from_travel",mer_type_game)
  get_groupon_china("http://www.gaopeng.com/deals/premium?ADTAG=premium_from_game",mer_type_famous)

end
