# encoding: utf-8

def get_groupon_detail_cungmua(merchant_name_all,link_detail,tag_type)
  b = Mechanize.new
  (link_detail[0] == '/' ) ? link_detail = "http://www.cungmua.com" + link_detail : link_detail
  puts link_detail
  puts 'link_detail'
  b.get(link_detail)
  puts ' get link detail'
  name = b.page.search(".product_detail_name").blank? ? nil : b.page.search(".product_detail_name").first.text.squish
  puts name
  
  # CHECK GROUPON EXISTED IN DB
  if merchant_name_all.include?(name)
    puts '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
    puts 'groupon exsisted'
    puts 
    puts 
  else
    puts 'name'
    offer_description = b.page.search(".product_detail_con").blank? ? nil : b.page.search(".product_detail_con").first.text.squish
    puts offer_description
    puts 'offer_description'
    price = b.page.search(".info_price").blank? ? nil : get_price_vn(b.page.search(".info_price").first.text[0..-1])
    price = get_price_vn(b.page.search(".info_price1").first.text.squish.split("/")[0]) if price.blank?
    puts price
    puts 'price'
    discount = b.page.search(".info_price1").blank? ? nil : b.page.search(".info_price1").first.text.gsub("Giảm",'').squish[0..-1].to_f
    puts discount
    puts 'discount'
    currency = 'VNĐ'
    end_time = 1
    phone = nil
    mobile_phone = nil
    link = nil
    if b.page.search(".product_detail_map").blank?
      address = nil#(b.page.search(".product_detail_con").blank? || b.page.search(".product_detail_con")[1].blank? )? nil : b.page.search(".product_detail_con")[1].text.squish
      phone = nil
      mobile_phone = nil
    else
      link = (b.page.search(".product_detail_map").blank? || b.page.search(".product_detail_map").search("a").blank? ) ? nil : b.page.search(".product_detail_map").search("a").first.attributes['href'].value
      flag_address = 0 # address may be in position 0 or 1
      if !b.page.search(".product_detail_map").search("strong").blank?
        flag_address = 1
      end
      b.page.search(".product_detail_map").search("p>span").each_with_index do |ar,i|
        if flag_address == 1
          if i == 1
            puts '000000000000000000000000'
            address = ar.text.squish.gsub("Địa chỉ:",'').gsub("ĐC:",'').gsub("Đ/c:",'').gsub("CN1:",'')
          elsif i == 2
            puts '111111111111111111'
            puts ar.text
            ar = ar.text.gsub("Điện thoại:",'')
            ar = ar.gsub("ĐT:",'')
            phone = ar.split("-")[0].blank? ? nil : ar.split("-")[0].squish
            mobile_phone = ar.split("-")[1].blank? ? nil : ar.split("-")[1].squish
            break
          end
        else
          if i == 0
            puts '+++++++++++++++++++++'
            address = ar.text.squish
            puts address
          elsif i == 1
            puts '__________________________'
            ar = ar.text.gsub("Điện thoại:",'')
            ar = ar.gsub("ĐT:",'')
            phone = ar.split("-")[0].blank? ? nil : ar.split("-")[0].squish
            mobile_phone = ar.split("-")[1].blank? ? nil : ar.split("-")[1].squish
            puts mobile_phone
            
            break
          end
        end
        
      end
    end
    puts address
    puts 'address'
    mer_name = name
    mer_description = offer_description
    
    
    @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
    @offer = create_offer(@merchant,link,name,offer_description,end_time,price,discount,currency,address,phone,mobile_phone)
    
    
    img = b.page.search("#slideShowImages").search("img").each_with_index do |img,i|
      if i == 0
        create_image(@merchant ,img.attributes['src'].value)
      end
      create_image(@offer, img.attributes['src'].value)
    end
  end
  
end



# GET GROUPON WHEN CHOOSE CITY AND SELECT MENU TAG
# NOTE: MUST USE COOKIE TO GET CITY AGAIN, IF NOT IT WILL RANDOM CITY
def get_groupon_cate_cungmua(merchant_name_all,city,link_cate , tag_type)
  puts 'link cate'
  a = Mechanize.new
  a.get("http://www.cungmua.com")
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  cookie = Mechanize::Cookie.new('CityId', "#{city}")
  cookie.domain = "www.cungmua.com"
  cookie.path = "/"
  a.cookie_jar.add(a.history.last.uri,cookie)
  
  
  a.get(link_cate)
  puts 'get link cateeeeeeeeeeeeeeeeeeeeeeee'
  
  # GET ALL LINK DETAIL AND GO TO EACH TO GET DETAIL
  a.page.search(".bg_M").search("li a").each do |link_detail|
    get_groupon_detail_cungmua(merchant_name_all,link_detail.attributes['href'].value,tag_type)
  end
  
end




# city be use for setting cookie 'CityId'
def get_groupon_cungmua(merchant_name_all,city)
  
  a = Mechanize.new
  a.get("http://www.cungmua.com")
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  cookie = Mechanize::Cookie.new('CityId', "#{city}")
  cookie.domain = "www.cungmua.com"
  cookie.path = "/"
  a.cookie_jar.add(a.history.last.uri,cookie)
  
  a.get("http://www.cungmua.com") # GO TO THE MAIN PAGE WITH THE CITY SELECTED
  puts 'get main page with city'
  
  # GET ALL LINK MENU EXSIT IN CITY AND GO TO EACH MENU TO GET GROUPON
  a.page.search(".calague").search("a").each_with_index do |menu,i|
    if i != 0
      puts menu.attributes['href'].value
      if menu.attributes['href'].value == 'http://www.cungmua.com/du-lich'
        get_groupon_cate_cungmua(merchant_name_all,city,menu.attributes['href'].value,['tour'])       #(merchant_name_all , link_menu , type merchant)
      elsif menu.attributes['href'].value == 'http://www.cungmua.com/gia-dung'
        get_groupon_cate_cungmua(merchant_name_all,city,menu.attributes['href'].value,['life'])
      else
        get_groupon_cate_cungmua(merchant_name_all,city,menu.attributes['href'].value,['good'])
      end
    end
  end
  
end









desc "GET GROUPON CUNG MUA"
task :groupon_cungmua => :environment do
  merchant_name_all = Merchant.all.map(& :name)
  require 'rubygems'
  require 'mechanize'
  
  #get_groupon_detail_cungmua(merchant_name_all,"http://www.cungmua.com/khuyen-mai/ha-noi/khoa-hoc-boi-20-buoi-tai-be-boi-166-truong-chinh-hoac-be-boi-vinh-tuy-tri-gia-1500000d-chi-con-690000d-8F030F24F4357783",['good'])
  
  #  cookie :   CityId
  #TPHCM 1
  get_groupon_cungmua(merchant_name_all,1)

  #HA NOI 2
  get_groupon_cungmua(merchant_name_all,2)

  #  bh q9 td 	21
  get_groupon_cungmua(merchant_name_all,21)
  
  #  can tho		16
  get_groupon_cungmua(merchant_name_all,16)
  
  #  da nang		18
  get_groupon_cungmua(merchant_name_all,18)  
  
  #  long xuyen 	65
  get_groupon_cungmua(merchant_name_all,65)  
  
  #  binh duong	12
  get_groupon_cungmua(merchant_name_all,12)
  
  #  vinh long 	62
  get_groupon_cungmua(merchant_name_all,62)
  
  #  nha trang	27
  get_groupon_cungmua(merchant_name_all,27)

end
