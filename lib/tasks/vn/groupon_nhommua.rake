# encoding: utf-8

#GET DETAIL GROUPON
def get_groupon_detail_nhommua(merchant_name_all,link_detail, tag_type)
 
  puts '==================================='
  puts link_detail
  b = Mechanize.new
  b.gzip_enabled = false
  b.ignore_bad_chunking = true
  b.keep_alive = false
  b.get(link_detail)
  
  mer_name = (b.page.search(".title-detail").blank? || b.page.search(".title-detail").first.search("h1").blank? ) ? nil : b.page.search(".title-detail").first.search("h1").first.text.squish
  mer_name = b.page.title.gsub("nhomMua.com",'').squish if mer_name.blank?
  puts mer_name
  
#  CHECK EXISTED
  if merchant_name_all.include?(mer_name)
    puts '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
    puts 'groupon exsisted'
    puts 
    puts 
  else
    puts 'mer name'
    offer_description = (b.page.search("div[itemprop='description']").blank?) ? nil : b.page.search("div[itemprop='description']").first.text.gsub("nhómMua",'Groupon Deekau').squish
    puts offer_description
    puts 'offer_description'
    end_time = b.page.search("#Time_CountDown_0").blank? ? 0 : b.page.search("#Time_CountDown_0").first.text.split(":")[0].to_i/24
    (end_time == 0)? end_time = 1 : end_time
    puts end_time
    puts 'endtime'
    link = b.page.search("em[itemprop='reviewer'] a").blank? ? nil : b.page.search("em[itemprop='reviewer'] a").first.attributes['href'].value
    puts link
    puts 'link'
    price = b.page.search(".price-org del").blank? ? nil : get_price_vn(b.page.search(".price-org del").first.text)
    puts price
    puts 'price'
    discount = (b.page.search(".right-detail-inner").blank? || b.page.search(".right-detail-inner").search(".save").blank?)? nil : b.page.search(".right-detail-inner").search(".save").first.text[0..-1].to_f
    puts discount
    puts 'discount'
    currency = b.page.search("i[itemprop='priceCurrency']").blank? ? nil : b.page.search("i[itemprop='priceCurrency']").first.text.squish
    if currency.blank?
      currency = b.page.search("i[itemprop='pricecurrency']").blank? ? nil : b.page.search("i[itemprop='pricecurrency']").first.text.squish 
    end
    puts currency
    puts 'currency'
    name = mer_name
    mer_description = offer_description



  
  
    # GET ADDRESS PHONE AND CREATE MERCHANT + OFFER
    b.page.search("p span[itemprop='tel']").each_with_index do |add,index|
      arr_phone = b.page.search("li>span[itemprop='tel']")[index].blank? ? nil : b.page.search("li>span[itemprop='tel']")[index].text.squish
      if arr_phone.blank?
        phone = nil
        mobile_phone = nil
      else
        arr_phone = arr_phone.gsub("Điện thoại:","").squish.split(",")
        if arr_phone.count > 1
          phone = arr_phone[0]
          mobile_phone = arr_phone[1]
        else
          phone = arr_phone[0]
          mobile_phone = nil
        end
      end
      puts add.text.squish
      puts phone
      puts mobile_phone
      (index==0)? @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,add.text.squish,phone,mobile_phone) : create_other_address(@merchant,add.text.squish,phone,mobile_phone)
      (index==0)? @offer = create_offer(@merchant,link,name,offer_description,end_time,price,discount,currency,add.text.squish,phone,mobile_phone) : create_other_address(@offer,add.text.squish,phone,mobile_phone)
    end
  
  
    # GET IMAGE
    b.page.search("#div_IMGList").search("img").each_with_index do |img,i|
      puts img.attributes['src'].value
      create_image(@offer,img.attributes['src'].value)
      if i == 0
        create_image(@merchant, img.attributes['src'].value)
      end
    end
    puts 
    puts 
  
  end
end


# GET GROUPON
def get_groupon_nhommua(merchant_name_all,class_deal,link,tag_type)
  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  a.get(link)
  
  # GET ALL LINK DETAIL AND GET DETAIL
  a.page.search(class_deal).each_with_index do |deal,index|
    if (index == 0 && class_deal=='.deal')
    else
      get_groupon_detail_nhommua(merchant_name_all,deal.search('a').first.attributes['href'].value ,tag_type)
    end
    
  end
  
end

def get_groupon_luxury_detail_nhommua(merchant,link_detail)
  puts 'offerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr'
  c = Mechanize.new
  c.gzip_enabled = false
  c.ignore_bad_chunking = true
  c.keep_alive = false
  c.get(link_detail)
  name = c.page.search("h1[itemprop='name']").blank? ? nil : c.page.search("h1[itemprop='name']").first.text.squish
  puts name
  puts 'name'
  offer_description = c.page.search("div[itemprop='description']").blank? ? nil : c.page.search("div[itemprop='description']").first.text.squish
  puts offer_description
  puts 'offer_description'
  price = c.page.search("strong[itemprop='price']").blank? ? nil : get_price_vn(c.page.search("strong[itemprop='price']").text.gsub("VNĐ",''))
  puts price
  puts 'price'
  discount =  nil
  link = nil
  currency = c.page.search("i[itemprop='pricecurrency']").blank? ? nil : c.page.search("i[itemprop='pricecurrency']").text
  puts currency
  puts 'currency'
  end_time = 1
  @offer = create_offer(merchant,link,name,offer_description,end_time,price,discount,currency,address = nil,phone = nil ,mobile_phone = nil)

  c.page.search("#div_IMGList li img").each do |img|
    puts img.attributes['src'].value
    create_image(@offer, img.attributes['src'].value)
  end
  
end



def get_groupon_luxury_cate_nhommua_luxury(merchant_name_all,link_cate)
  puts 'main pageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
  b = Mechanize.new
  b.gzip_enabled = false
  b.ignore_bad_chunking = true
  b.keep_alive = false
  b.get(link_cate)
  
  paging = Mechanize.new
  paging.gzip_enabled = false
  paging.ignore_bad_chunking = true
  paging.keep_alive = false
  paging.get(link_cate)
  mer_name = (b.page.search("#dCompanyInfo").blank? || b.page.search("#dCompanyInfo").search("h1").blank?)? nil : b.page.search("#dCompanyInfo").search("h1").first.text
  puts mer_name
  puts 'mer_name'
  if merchant_name_all.include?(mer_name)
    
    puts '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
    puts 'groupon LUXURYYYYYYYYYYYYYYYYYYYYYYYYYYY exsisted'
    puts 
    puts 
  else
    img_mer = (b.page.search("#dCompanyInfo").blank? || b.page.search("#dCompanyInfo").search("img").blank?)? nil : b.page.search("#dCompanyInfo").search("img").first.attributes['src'].value
    puts img_mer
    puts 'img_mer'
    des_mer = b.page.search("div[itemprop='description']").blank? ? nil : b.page.search("div[itemprop='description']").first.text.squish
    puts des_mer
    puts 'des_mer'
    end_time = 10
     @merchant = create_merchant(mer_name,des_mer,['luxury'],end_time,address = nil,phone = nil,mobile_phone = nil)
    @merchant = Merchant.create(:name => mer_name,:description => des_mer,
      :tags=>['luxury'],:start_open_time=>Time.current,:end_open_time=>(Time.current + end_time.days))
    create_image(@merchant, img_mer)
    b.page.search(".small-box-trans").each do |link_detail|
      get_groupon_luxury_detail_nhommua(@merchant,link_detail.search("a").first.attributes['href'].value)
    end
    b.page.search(".paging").search("a").each do |link_paging|
      puts 'other pageeeeeeeeeeeeeeeeeeeeeeeeeee'
      paging.get(link_paging.attributes['href'].value)
      paging.page.search(".small-box-trans").each do |link_detail|
        get_groupon_luxury_detail_nhommua(@merchant,link_detail.search("a").first.attributes['href'].value)
      end
    end
  
  end
end




def get_groupon_nhommua_luxury(merchant_name_all,link)
  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  a.get(link)
  a.page.search(".small-group-box").each do |deal|
    get_groupon_luxury_cate_nhommua_luxury(merchant_name_all,deal.search("a").first.attributes["href"].value)
  end
end


desc "GET GROUPON NHOM MUA"
task :groupon_nhommua => :environment do
  
  #GET ARRAY NAME MERCHANT EXSIT
  merchant_name_all = Merchant.all.map(& :name)
  require 'rubygems'
  require 'mechanize'
#get_groupon_detail_nhommua(merchant_name_all,"http://www.nhommua.com/tp-ho-chi-minh/tour-du-lich/tour-campuchia-4n3d-cong-ty-mua-travel-7F050904037C.html",['good'])
  
  
  
  #TPHCM
  #(merchant_name_all , class wrap groupon , link page get groupon, type merchant)
  get_groupon_nhommua(merchant_name_all,".deal", "http://www.nhommua.com/tp-ho-chi-minh/khach-san-resort/",['hotel'])
  get_groupon_nhommua(merchant_name_all,".deal", "http://www.nhommua.com/tp-ho-chi-minh/tour-du-lich/",['tour'])
  get_groupon_nhommua(merchant_name_all,".small-box-white", "http://www.nhommua.com/tp-ho-chi-minh/dich-vu-gia-re.html",['life'])
  get_groupon_nhommua(merchant_name_all,".small-box-white", "http://www.nhommua.com/tp-ho-chi-minh/san-pham/",['good'])
  get_groupon_nhommua_luxury(merchant_name_all,"http://www.nhommua.com/tp-ho-chi-minh/hang-hieu/")

  #HA NOI
  get_groupon_nhommua(merchant_name_all,".deal","http://www.nhommua.com/ha-noi/khach-san-resort/",['hotel'])
  get_groupon_nhommua(merchant_name_all,".deal","http://www.nhommua.com/ha-noi/tour-du-lich/",['tour'])
  get_groupon_nhommua(merchant_name_all,".small-box-white","http://www.nhommua.com/ha-noi/san-pham/",['life'])
  get_groupon_nhommua(merchant_name_all,".small-box-white","http://www.nhommua.com/ha-noi/dich-vu-gia-re.html",['good'])  
  get_groupon_nhommua_luxury(merchant_name_all,"http://www.nhommua.com/ha-noi/hang-hieu/")
  
  #HAI PHONG
  get_groupon_nhommua(merchant_name_all,".deal","http://www.nhommua.com/hai-phong/khach-san-resort/",['hotel'])
  get_groupon_nhommua(merchant_name_all,".deal","http://www.nhommua.com/hai-phong/tour-du-lich/",['tour'])
  get_groupon_nhommua(merchant_name_all,".small-box-white","http://www.nhommua.com/hai-phong/dich-vu-gia-re.html",['life'])
  get_groupon_nhommua(merchant_name_all,".small-box-white","http://www.nhommua.com/hai-phong/san-pham/",['good'])  

  #CAN THO
  get_groupon_nhommua(merchant_name_all,".deal","http://www.nhommua.com/can-tho/khach-san-resort/",['hotel'])
  get_groupon_nhommua(merchant_name_all,".deal","http://www.nhommua.com/can-tho/tour-du-lich/",['tour'])
  get_groupon_nhommua(merchant_name_all,".small-box-white","http://www.nhommua.com/can-tho/dich-vu-gia-re.html",['life'])
  get_groupon_nhommua(merchant_name_all,".small-box-white","http://www.nhommua.com/can-tho/san-pham/",['good'])  





end
