# encoding: utf-8

def validation (field)   #  fill validate field needed
  if field.blank?
    return false
  end
  return true
end



def create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
  name = name.split.each{|i| i.capitalize!}.join(' ')
  address = address.split.each{|i| i.capitalize!}.join(' ')
  return Merchant.create(:name => mer_name,:description=>mer_description,
    :tags=>tag_type,:start_open_time=>Time.current,:end_open_time=>(Time.current + end_time.days),:add_description=>address,:phone => phone , :mobile_phone => mobile_phone)
end

def create_other_address(object,address,phone,mobile_phone)
  object.addresses.create(:description=>address,:phone => phone , :mobile_phone => mobile_phone) if !object.blank?
end

def create_image(object,url)
  object.images.create(:img_url_web => url) if !object.blank?
end

def create_offer(merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)
  if validation(address) || validation(phone)
  return merchant.offers.create(:link=>link,:name=>name,:active=>1,:description=>description,
    :start_time=>Time.current,:end_time=>Time.current + end_time.days,:price=>price,:discount=>discount,
    :currency=>currency,:add_description=>address,:phone => phone , :mobile_phone => mobile_phone)
  else
    return nil
  end
end

def get_price_others(price_s)
  price = ''
  price_s.split(",").each do |p|
    price += p
  end
  return price.to_f
end

def get_price_russia(price_s)
  if !(price_s[0].to_f.in?(1..9))
    price_s = price_s[3..99]
  end
  price = ''
  price_s[0..price_s.length-5].split(" ").each do |p|
    price += p
  end
  return price.to_f
end


def get_price_vn(string)
  price = '' 
  string.squish.split(".").each do |s|
    price = price + s
  end
  return price.to_f
end