# encoding: utf-8
def get_groupon_detail_small_frank_gateway(link_detail,merchant)
  b = Mechanize.new
  puts '========= small details =================='
  puts link_detail
  link_detail = (link_detail)? "http://sejours.groupon.fr" + link_detail : link_detail
  b.get(link_detail)
  if !b.page.search(".sidebox").search(".address").first.text.blank?
    name = b.page.search("#shortdescription").text.squish
    puts 'name'
    offer_description = nil
    b.page.search("#tabs-1").search(".product").each do |d|
      if !d.blank?
        offer_description = d.text.squish
        break
      end
    end
    puts 'offer_description'

    price = b.page.search("#content_right_top").blank? ? nil : b.page.search("#content_right_top").first.search(".price").first.text.gsub("A partir de","").to_f
    puts 'price'
    currency = b.page.search("#content_right_top").blank? ? nil : b.page.search("#content_right_top").first.search(".price").first.text.squish[-2]
    puts 'currency'
    discount = nil
    end_time = 10
    address = b.page.search(".address").first.text.squish.gsub(",",'')
    puts 'address'
    phone = nil
    link = nil
    link_text = nil



    create_offer(merchant,link,name,offer_description,end_time,price,discount,currency,address,phone,mobile_phone = nil)
    if merchant.name.blank?
      merchant.update_attributes(:name => name,:phone=>phone,:add_description= => address)
    end


    b.page.search(".ad-thumb-list").first.search("img").each do |i|
      img = i.attributes['src'].value
      puts img
      img = (img[0]=='/')? link_detail +  img : img
      create_image(@offer, img)
    end
  end
end


def get_groupon_detail_frank_gateway(link_detail,tag_type)
  puts '======================='
  puts link_detail
  link_detail = (link_detail[0]=='/')? "http://voyages.groupon.fr" + link_detail : link_detail
  puts link_detail
  a = Mechanize.new
  a.get(link_detail)
  puts ' load page'
  # check groupon .SINGLE OR MAY BE A CAT HAVE MANY small groupon
  if a.page.search(".product").blank?
    
    # IF HAVE NO ADDRESS , NOT GET GROUPON
    if !a.page.search(".infomap_content").search("p").first.blank?
      mer_name = a.page.search("#shorttitle").text.squish
      puts 'mer name'
      name = a.page.search("#shortdescription").text.squish
      puts 'name'
      offer_description = nil
      a.page.search("#tabs-1").search("p").each do |d|
        if !d.blank?
          offer_description = d.text.squish
          break
        end
      end
      puts 'offer_description'

      price = a.page.search("#content_right").first.search(".from").first.text.gsub("A partir de","").gsub("€","").to_f
      puts 'price'
      currency = a.page.search("#content_right").first.search(".from").first.text.squish[-1]
      puts 'currency'
      discount = nil
      end_time = a.page.search("#daysLeft").blank? ? 10 : a.page.search("#daysLeft").text.to_i
      puts 'end time'
      address = a.page.search(".infomap_content").search("p").first.text.squish.gsub(",",'')
      puts 'address'
      phone = nil
      link = a.page.search(".infomap_content").search("a").first.blank? ? nil : a.page.search(".infomap_content").search("a").first.attributes['href'].value
      puts 'link'
      link_text = a.page.search(".infomap_content").search("a").first.blank? ? nil : a.page.search(".infomap_content").search("a").first.text.squish
      puts 'link text'
      city = "Frank"
      mer_description = offer_description
      mobile_phone = nil
      @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
      @offer = create_offer(@merchant,link,name,offer_description,end_time,price,discount,currency,address,phone,mobile_phone)

      mer_img = a.page.search("#fthumbsview").first.search("img").first.attributes['src'].value
      mer_img = (mer_img.include?("groupon.fr"))?  mer_img : link_detail[0..-1] + mer_img
      puts mer_img
      create_image(@merchant, mer_img)
      a.page.search("#fthumbsview").first.search("img").each do |i|
        img = i.attributes['src'].value
        img = (img.include?("groupon.fr"))?  img : link_detail[0..-1] + img

        
        puts img
        create_image(@offer, img)
      end
    end
  else
    # GROUPON IS A CAT
    mer_img = a.page.search(".corner-image").blank? ? nil : a.page.search(".corner-image").first.search("img").first.attributes["src"].value
    puts mer_img
    mer_name = a.page.title.squish
    mer_description = mer_name
    @merchant = create_merchant(mer_name,mer_description,tag_type,10,address = nil,phone = nil,mobile_phone = nil)
    
    # GET SMALL GROUPON LINK
    a.page.search(".product").each do |offer|
      get_groupon_detail_small_frank_gateway(offer.search("a").first.attributes['href'].value,@merchant)
    end

  end
end


desc "GET GROUPON FRANK GATEWAY"
task :groupon_frank_gateway => :environment do
  
  require 'rubygems'
  require 'mechanize'
  
  mer_type_gateways = ['gateway']
#  get_groupon_detail_frank_gateway("http://voyages.groupon.fr/6354306-mediumvirgendelosreyes/",mer_type_gateways)
  


  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  
  page_link = "http://voyages.groupon.fr/"
  a.get(page_link)
  
  # Get groupon link
  a.page.search(".dealsbox_content").each do |groupon|
    get_groupon_detail_frank_gateway(groupon.search("a").first.attributes['href'].value,mer_type_gateways)
  end


  

end
