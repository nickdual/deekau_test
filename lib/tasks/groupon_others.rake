# encoding: utf-8
def get_detail_groupon_default_others(page_link,link_detail,tag_type,currency_position)
  puts 'get detail'
  puts link_detail
  b = Mechanize.new
  (link_detail[0] == '/')? b.get( page_link + link_detail) : b.get(link_detail)
  
  if !b.page.search("#contentDealBuyBox").blank?
    puts 'details========='
    
    puts link_detail
    mer_name = b.page.search("#contentDealTitle").blank? ? nil : b.page.search("#contentDealTitle").first.text.squish
    puts 'mer name'
    end_time = b.page.search(".countdownTime .daysLeft").blank? ? 10 : b.page.search(".countdownTime .daysLeft").first.text.to_f
    puts 'end time'
    link = b.page.search(".merchantContact").search("a").blank? ? '' : b.page.search(".merchantContact").search("a").first.attributes['href'].value
    puts 'link'
    link_text = b.page.search(".merchantContact").search("a").blank? ? '' : b.page.search(".merchantContact").search("a").first.text
    puts 'link text'

    if currency_position == 0
      currency = b.page.search(".price .noWrap").blank? ? nil : b.page.search(".price .noWrap").first.text.squish[0]
    elsif currency_position == 2
      currency = b.page.search(".price .noWrap").blank? ? nil : b.page.search(".price .noWrap").first.text.squish[0..2]
    elsif currency_position == 3
      currency = b.page.search(".price .noWrap").blank? ? nil : b.page.search(".price .noWrap").first.text.squish[0..3]
    elsif currency_position == 11
      currency = b.page.search(".price .noWrap").blank? ? nil : b.page.search(".price .noWrap").first.text.squish[-2..-1]
    elsif currency_position == 111
      currency = b.page.search(".price .noWrap").blank? ? nil : b.page.search(".price .noWrap").first.text.squish[-3..-1]
    else
      currency = b.page.search(".price .noWrap").blank? ? nil : b.page.search(".price .noWrap").first.text.squish[-1]
    end
    puts 'currency'


    phone = nil
    
    arr_address = b.page.search(".merchantContact").blank? ? nil : b.page.search(".merchantContact").first.text
    if !arr_address.blank?
      arr_address_h2 = b.page.search(".merchantContact").search("h2").first.text
      arr_address = arr_address.gsub(arr_address_h2,'')
      arr_address = arr_address.gsub(link_text,'').split(".")
      if arr_address.count == 2
        address = arr_address[0].squish
        city = arr_address[1].squish
      else
        address = arr_address[0].squish
        city = nil
      end
    end
    mer_description = b.page.title.squish
    mobile_phone = nil
    
    @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
    
    image = b.page.search("#deal").first.search("img").first.attributes['src'].value
    puts image
    image = (image[0]=='/')? link_detail +  image : image
    create_image(@merchant, image)

    ## ONLY 1 OFFER IN MERCHANT
    if b.page.search("#jMultiDealList li").blank?
      puts '**********ONLY ONE OFFER************'
      name = mer_name
      puts 'name off'
      description = b.page.title
      discount = b.page.search("#contentDealBuyBox").first.search(".saving").blank? ? nil : b.page.search("#contentDealBuyBox").first.search(".saving").first.search(".row2 td").first.text.squish[0..-2].to_f
      puts 'discount'
      # SOMETIME DISCOUNT IS NULL 
      if !discount.blank?
        if currency_position == 0
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish[1..99]) * 100 / discount
        elsif currency_position == 2
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish[2..99]) * 100 / discount
        elsif currency_position == 3
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish[3..99]) * 100 / discount
        else
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish) * 100 / discount
        end
        
      else
        if currency_position == 0
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish[1..99])
        elsif currency_position == 2
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish[2..99])
        elsif currency_position == 3
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish[3..99])
        else
          price = b.page.search(".price span").blank? ? nil : get_price_others(b.page.search(".price span").first.text.squish)
        end

        
      end
      puts 'price'
      @offer = create_offer(@merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)
      create_image(@offer,image)
    else # MERCHANT HAVE MANY OFFER
      b.page.search("#jMultiDealList li").each do |li|
        name = li.search(".multiDealTitle").blank? ? nil : li.search(".multiDealTitle").first.text
        puts 'name off'
        description = name
        discount = li.search(".multiDealConditions span").blank? ? nil : li.search(".multiDealConditions span").first.text[0..-2].to_f
        puts 'discount'
        if discount.blank?
          price = li.search(".multiDealPrice span").last.text.squish[1..99].to_f
        else
          price = li.search(".multiDealPrice span").last.text.squish[1..99].to_f * 100 / discount
        end
        puts 'price'

        @offer = create_offer(@merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)
        create_image(@offer, image)
      end
    end

    
    puts
    puts
  end
end

def get_groupon_default_others(page_link,mer_type_good,mer_type_gateway,mer_type_service,currency_position)
  require 'rubygems'
  require 'mechanize'

  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false

  b = Mechanize.new
  b.gzip_enabled = false
  b.ignore_bad_chunking = true
  b.keep_alive = false


  a.get(page_link)

  # some country must choose languaGE and some country must fill email
  if a.page.search("#merchant-info").blank?
    puts 'merchant info blank'
    if !a.page.forms.first.field_with(:id => 'email-input').blank?
      a.page.forms.first.field_with(:id => 'email-input').value = 'dieuit07@gmail.com'
    end
    a.page.forms.first.submit
  end
  
  
  puts page_link
  puts '1111111111111111111'
  
  
  # some country have style menu different
  if !a.page.search("#jSHOPPING").blank?
    link_shopping = a.page.search("#jSHOPPING").blank? ? nil : a.page.search("#jSHOPPING").first.attributes['href'].value
    link_shopping = (link_shopping[0]== '/')? page_link[0..-2] + link_shopping : link_shopping
  end
  if !a.page.search("#jTRAVEL").blank?
    link_gateway = a.page.search("#jTRAVEL").first.attributes['href'].value
    link_gateway = (link_gateway[0]== '/')? page_link[0..-2] + link_gateway : link_gateway
  end
  puts '222222222222222222'
  
#  GET TAB WRAPPING CITIES MENU
  cites_menu = a.page.search("#jCitiesSelectBox").first.search("li")

  
  cites_menu.each do |menu|
    # GET LINK OF CITIES AND SOME OTHER TYPE
    link = menu.attributes['onclick'].value.split("\'")[1]
    
 
    puts 'linkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk'
    puts link
    
    # SET TYPE OF MERCHANT
    if link == link_shopping
      tag_type = mer_type_good
    elsif link!= link_gateway
      tag_type = mer_type_gateway
    else
      tag_type = mer_type_service
    end
    b.get(link)
    puts '33333333333333333333'
    # avoid cookie expire or not set cookie (some country require cookie in page inside...)
    if b.page.search("#merchant-info").blank? && link!="http://www.groupon.co.il/deals/national-deal" && link!="http://www.groupon.co.il/deals/travel-iw" && link!= "http://www.groupon.pt/deals/groupon-travel"
      if !b.page.forms.first.field_with(:id => 'email-input').blank?
        b.page.forms.first.field_with(:id => 'email-input').value = 'dieuit07@gmail.com'
      end
      b.page.forms.first.submit
    end
    puts '4444444444444444444444444444'
    
    # GET GROUPON FEATURE
    link_detail = page_link
    get_detail_groupon_default_others(page_link,link_detail,tag_type,currency_position)
    
    # FIND THE TAB WRAP MORE GROUPON
    if !b.page.search(".sidebarBoxMain > .sidebarBoxContent").blank?
      b.page.search(".sidebarBoxMain > .sidebarBoxContent").first.search(".extraDealMulti").each do |groupon|
        get_detail_groupon_default_others(page_link,groupon.search("a").first.attributes['href'].value,tag_type,currency_position)
      end
      puts '55555555555555555555555555'
      
    end
    
    
  end

  

end







desc "GET GROUPON ELSE OTHERS"
task :groupon_default_others => :environment do
  mer_type_service = ['service']   # default - service
  mer_type_gateway = ['gateway']
  mer_type_good = ['good']
  page_links = {}

  # TYPE OF CURRENCY AND PRICE
  # 0 : $123
  # 1 : 123 $
  # 2 : dr123
  # 3 : dr.123
  # 11: 123je
  #111: 12jef
  
  #page_links["http://www.groupon.com.ar/"] = 0    #argentina
  #   page_links["http://www.groupon.at/"] = 0       #Austria
  #   page_links["http://www.groupon.be/"] = 1        #Belgium
  #   page_links["http://www.groupon.com.br/"] = 2      #Brazil
  # page_links["http://www.groupon.com.co/"] = 0          #Colombia
  #page_links["http://www.groupon.dk/"]   = 3        #Denmark
  #page_links["http://www.groupon.fi/"]   = 1        #Findland
  #page_links["http://www.groupon.fr/"]   = 1        #Frank
  #page_links["http://www.groupon.de/"]   = 1        #Germany
  #page_links["http://www.groupon.gr/"]   = 1        #Greek
  #page_links["http://www.crazeal.com/"]   = 0        #India
  #page_links["http://www.groupon.ie/"]   = 0        #Ireland
  #page_links["http://www.groupon.co.il/"]   = 0        #Irael
  #page_links["http://www.groupon.it/"]   = 0        #Italy
  #page_links["http://www.groupon.my/"]   = 2        #Malaysia
  #page_links["http://www.groupon.com.mx/"]   = 0        #Mexico
  #page_links["http://www.groupon.nl/"]   = 0        #Netherland
  #page_links["http://www.grouponnz.co.nz/"]   = 0        #Newzeland
  #page_links["http://www.groupon.no/"]   = 3        #Norway
  #page_links["http://www.groupon.com.pe/"]   = 3        #Peru
  #page_links["http://www.beeconomic.com.ph/"]   = 3        #Philipin
  #page_links["http://www.groupon.pl/"]   = 11        #Poland
  #page_links["http://www.groupon.pt/"]   = 1        #Portugal
  #page_links["http://www.groupon.ro/"]   = 111        #Romania
  #page_links["http://www.groupon.sg/"]   = 2        #Singapo
  #page_links["http://www.groupon.co.za/"]   = 0        #South Africa
  #page_links["http://www.groupon.es/"]   = 1        #Spain
  page_links["http://www.groupon.se/"]   = 11        #Sweden
  #page_links["http://www.groupon.ch/"]   = 3        #Switzerland
  #page_links["http://www.mygroupon.co.th/"]   = 0        #ThaiLan
  #page_links["http://www.sehirfirsati.com/"]   = 11        #Turkey
  #page_links["http://www.groupon.ae/"]   = 3        #United Arab Emirates
  #page_links["http://www.groupon.co.uk/"]   = 0        #United Kingdom
  #page_links["http://www.groupon.com.uy/"]   = 0        #Uruguay
  #page_links["http://www.groupon.hk/"]   = 3        #Hongkong

  
  # GET GROUPON EACH COUNTRY
  page_links.each do |page_link , currency_position|
    get_groupon_default_others(page_link,mer_type_good,mer_type_gateway,mer_type_service,currency_position)
  end
end
