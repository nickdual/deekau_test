# encoding: utf-8
def get_detail_gateways_australia(groupon,tag_type)
  link_detail = groupon.search(".dealsbox_image").first.search("a").first.attributes['href'].value
  puts link_detail
  discount = groupon.search(".dealsbox_discount").first.text.squish.split(' ')[0][0..-1]
  puts 'discount'
  price = groupon.search(".dealsbox_buyinfo").first.search("strong").first.text.squish.split(' ').last.to_f
  puts 'price'
  currency = groupon.search(".dealsbox_buyinfo").first.search("strong").first.text.squish.split(' ')[1]
  puts 'currency'

  d = Mechanize.new
  d.gzip_enabled = false
  d.ignore_bad_chunking = true
  d.keep_alive = false
  (link_detail[0] == '/' )? d.get("http://getaways.groupon.com.au" + link_detail) : d.get(link_detail)

  name = d.page.search("#shorttitle").blank? ? nil : d.page.search("#shorttitle").first.text
  puts 'name'
  mer_name = name
  mer_description = d.page.search("#shortdescription").blank? ? nil : d.page.search("#shortdescription").first.text
  puts 'mer_description'
  description = mer_description
  end_time = d.page.search("#daysLeft").text.blank? ? 10 : d.page.search("#daysLeft").text.to_i
  puts 'end_time'
  if !d.page.search("#tabs-3").blank?
    array_address = d.page.search("#tabs-3").search("p")[1].text.split('  ')
    address = array_address[1].blank? ? nil : array_address[1]
    puts 'address'
    city = array_address[2].blank? ? nil : array_address[2]
    puts 'city'
    array_phone = d.page.search("#tabs-3").search("p")[2].text.split('  ')
    phone = array_phone[0].blank? ? nil : array_phone[0][7..99]
    puts 'phone'
    link_text = d.page.search("#tabs-3").search("a").blank? ? nil : d.page.search("#tabs-3").search("a").first.text
    puts 'link text'
    link = d.page.search("#tabs-3").search("a").blank? ? nil : d.page.search("#tabs-3").search("a").first.attributes['href'].value
    puts 'link'
  else
    address = nil
    puts 'address nil'
    city = nil
    puts 'city nil'
    phone = nil
    puts 'phone nil'
    link_text = nil
    puts 'link text nil'
    link = nil
    puts 'link nil'
  end
  puts
  puts 
  mobile_phone = nil
  @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
  @offer = create_offer(@merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)

  ##
  ## GET IMAGE
  ##
  create_image(@merchant, d.page.search("#ImageGallery-Controls").first.search("img").first.attributes['src'].value)
  d.page.search("#ImageGallery-Controls").first.search("img").each do |i|
    puts i.attributes['src'].value
    create_image(@offer, i.attributes['src'].value)
  end
end
def get_groupon_australia_getways(page_link,tag_type)
  require 'rubygems'
  require 'mechanize'
  
  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  a.get(page_link)

  a.page.search("#tabs-0 .dealsbox_content").each do |groupon|
    get_detail_gateways_australia(groupon,tag_type)
  end

end
def get_detail_elses_australia(link_detail,tag_type)
  c = Mechanize.new
  (link_detail[0] == '/')? c.get("http://www.groupon.com.au" + link_detail) : c.get(link_detail)

  # CHECK GROUPON WAS EXPIRED
  if !c.page.search("#contentBoxLeftContainer").blank?
  
    
    puts link_detail
    mer_name = c.page.search("#contentDealTitle").blank? ? nil : c.page.search("#contentDealTitle").first.text.squish
    puts 'mer name'
    end_time = c.page.search(".countdownTime .daysLeft").blank? ? 10 : c.page.search(".countdownTime .daysLeft").first.text.to_f
    puts 'end time'
    link = c.page.search(".merchantContact").search("a").blank? ? '' : c.page.search(".merchantContact").search("a").first.attributes['href'].value
    puts 'link'
    link_text = c.page.search(".merchantContact").search("a").blank? ? '' : c.page.search(".merchantContact").search("a").first.text
    puts 'link text'
    currency = c.page.search(".price .noWrap").first.text[0..1].to_f
    puts 'currency'
    phone = nil
    mobile_phone = nil
    
    mer_description = c.page.search(".contentBoxNormalLeft").blank? ? nil : c.page.search(".contentBoxNormalLeft").first.text.squish
    description = mer_description
    
    # GET ADDRESS AND CITY INSIDE  TAB CLASS merchantContact
    arr_address = c.page.search(".merchantContact").first.text
    arr_address_h2 = c.page.search(".merchantContact").search("h2").first.text
    arr_address = arr_address.gsub(arr_address_h2,'')
    arr_address = arr_address.gsub(link_text,'').split(".")
    if arr_address.count == 2
      address = arr_address[0].squish
      city = arr_address[1].squish
    else
      address = arr_address[0].squish
      city = nil
    end




    @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
    puts image = c.page.search("#contentDealDescription").first.search('img').first.attributes['src'].value
    create_image(@merchant, image)
    

    ## 1 Merchant have one or many OFFER
    ## case 1: ONLY 1 OFFER IN MERCHANT
    if c.page.search("#jMultiDealList li").blank?
      puts '**********ONLY ONE OFFER************'
      name = mer_name
      puts 'name off'
      
      discount = c.page.search(".savings").first.search(".row2 td").first.text.squish[0..-1].to_f
      puts 'discount'
      price = c.page.search(".price span").first.text.squish[1..99].to_f * 100 / discount
      puts 'price'
      @offer = create_offer(@merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)
    else
      # MANY OFFERS IN MERCHANT
      c.page.search("#jMultiDealList li").each do |li|
        name = li.search(".multiDealTitle").blank? ? nil : li.search(".multiDealTitle").first.text
        puts 'name off'
        description = name
        discount = li.search(".multiDealConditions span").first.text[0..-1].to_f
        puts 'discount'
        price = li.search(".multiDealPrice span").last.text[1..99].to_f * 100 / discount
        puts 'price'

        @offer = create_offer(@merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)
        create_image(@offer, image)
      end
    end

    puts
    puts 
  end
end

# GET GROUPON 
def get_groupon_australia_else(page_link,mer_type_good,mer_type_city,mer_type_gateway)
  require 'rubygems'
  require 'mechanize'
  
  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false

  b = Mechanize.new
  b.gzip_enabled = false
  b.ignore_bad_chunking = true
  b.keep_alive = false


  a.get(page_link)
  
  # get all LINK CITIES IN THE MENU TOP 
  a.page.search("#jCitiesSelectBox").first.search("li").each do |menu|
    link = menu.attributes['onclick'].value.split("\'")[1]
    puts 'linkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk'
    puts link
    if link != "http://www.groupon.com.au/deals/travelcity"
      tag_type = mer_type_gateway
    elsif link == "http://www.groupon.com.au/deals/national-deal"
      tag_type = mer_type_good
    else
      tag_type = mer_type_city
    end
    
    # GO TO PAGE CITIES GROUPON AND GET LINK DETAIL
    b.get(link)
    puts 'get link'
    
    # THE FIRST GROUPON IS SHOWED WHEN GO TO PAGE CITY
    link_detail = page_link
    get_detail_elses_australia(link_detail,tag_type)
    
    
    # GET MORE GROUPON IN LIST RIGHT (MORE DEALS)
    more_details = nil 
    
    #GET TAB WRAPPING MORE DEAL
    b.page.search("#sidebar").search(".sidebarBoxHeader>.headline").each do |headline|
      if headline.text.squish == "More Deals"
        more_details = headline.parent.parent
        break
      end
    end
    
    
    #GET GROUPON RIGHT LIST
    if !more_details.blank?
      more_details.search(".extraDealMulti").each do |groupon|
        puts 'more offers'
        get_detail_elses_australia(groupon.search("a").first.attributes['href'].value,tag_type)
      end
    end

  end
  
end





###  WEB CHANGE STRUCTURE ->> CAN NOT GET IMAGE 
#desc "GET GROUPON GATEWAYS AUSTRALIA"
#task :groupon_australia_gateways => :environment do
#  page_link = "http://getaways.groupon.com.au/"
#  tag_type = ['gateway']
#  get_groupon_australia_getways(page_link,tag_type)
#end


desc "GET GROUPON ELSE AUSTRALIA"
task :groupon_australia_elses => :environment do
  mer_type_good = ['good']
  mer_type_city = ['city']
  mer_type_gateway = ['gateway']
  page_link = "http://www.groupon.com.au/"
  get_groupon_australia_else(page_link,mer_type_good,mer_type_city,mer_type_gateway)
end
