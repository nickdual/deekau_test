# encoding: utf-8
def get_groupon_detail_us_canada(link_detail,description,tag_type)
  puts link_detail
  doc_detail = Mechanize.new
  (link_detail[0]=='/')? doc_detail.get("http://www.groupon.com" + link_detail) : doc_detail.get(link_detail)
  puts link_detail


  ## ------------------------------
  ## get price - discount - currency
  ## ------------------------------
  puts '1111111111111111111'
  price    = (doc_detail.page.search(".value").blank? || doc_detail.page.search(".value").search(".integer").blank? )? nil : doc_detail.page.search(".value").search(".integer").first.text
  puts 'price'
  currency = (doc_detail.page.search(".value").blank? || doc_detail.page.search(".value").search(".unit").blank? )? nil : doc_detail.page.search(".value").search(".unit").first.text
  puts 'currency'
  feature_discount = (doc_detail.page.search(".discount").blank? || doc_detail.page.search(".discount").search(".integer").blank? )? nil : doc_detail.page.search(".discount").search(".integer").first.text
  if price.blank?
    puts 'no discount'
    price = doc_detail.page.search(".price").blank? ? 0 : doc_detail.page.search(".price").search(".integer").first.text.squish.to_f
    currency = doc_detail.page.search(".price").search(".unit").first.text.squish
  end
  puts 'price=========='

  # ------------------------------
  # get address - city
  # ------------------------------
  if !doc_detail.page.search(".deal_location").blank?
    city_name = doc_detail.page.search("address").first.search("h4").first.text
    address = doc_detail.page.search("address").first.search("p").first.text.split("\n")
    puts 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'

    primary_address = (address[1].blank?) ? nil : address[1].squish
    phone = (address[5].blank?)? nil : address[5].squish.split("|")[0]
    puts 'bbbbbbbbbbbbbbbb'

    #feature_summary = doc_detail.page.search(".deal_description").first.search(".pitch").first.text.squish

  end



  mer_name = doc_detail.page.search("hgroup h2").first.text.squish
  of_name = doc_detail.page.search("hgroup h2").first.text.squish
  get_link = doc_detail.page.search(".deal_right_rail").first.search(".name a").first
  if !get_link.blank?
    link_text = get_link.text
    link = get_link.attributes['href'].value
  end
  end_time = (doc_detail.page.search(".groupon_countdown").blank?)? 10 : doc_detail.page.search(".groupon_countdown").first.text.squish.split(':')[0].to_i
  puts 'cccccccccccccccccccccccc'
  #  i = 0
  #  while i < 10000
  mer_description = description
  mobile_phone = nil
  
  @f_merchant = create_merchant(mer_name,mer_description,tag_type,end_time,primary_address,phone,mobile_phone)

  @f_offer = create_offer(@f_merchant,link,of_name,description,end_time,price,feature_discount,currency,primary_address,phone,mobile_phone)
  #    i = i + 1
  #  end

  ## ------------------------------
  ## save img of groupon
  ## ------------------------------
            
  if doc_detail.page.search(".deal_thumbnails").blank? 
    img = doc_detail.page.search(".image_wrapper img").first.attributes['src'].value
    create_image(@f_offer, img)
    create_image(@f_merchant, img)
    puts img
  else
    create_image(@f_merchant,doc_detail.page.search(".deal_thumbnails").first.search("img").first.attributes['src'].value)
    doc_detail.page.search(".deal_thumbnails").first.search("img").each do |img|
      create_image(@f_offer, img.attributes['src'].value)
      puts img.attributes['src'].value
    end
  end
end

def get_groupon_us_canada(menu_id,tag_type)
  
  require 'rubygems'
  require 'mechanize'


  city = Mechanize.new
  city.get("http://www.groupon.com/cities?dl=d47388")
  
  # use for set cookie 
  a = Mechanize.new
  a.get("http://www.groupon.com/cities?dl=d47388")
  
  #GET ALL Cities link 
  city.page.search(".changing_location").each do |a_city|
    puts a_city.text
    if a_city.text == "\/cities?dl=d47388"
      puts "**************"
      puts 'error link'
      puts "**************"
    else
      ## SET COOKIE TO SHOW MENU EXACTLY
      cookie = Mechanize::Cookie.new('division', a_city.text.downcase.parameterize)
      puts 'cookieeeeeeeeeeeeeeeeeeeeeeee'
      puts cookie
      puts 'cookieeeeeeeeeeeeeeeeeeeeeeee'
      cookie.domain = ".groupon.com"
      cookie.path = "/"
      a.cookie_jar.add(a.history.last.uri,cookie)
      a.get("http://www.groupon.com/cities?dl=d47388")
      #######################################################################

      
      # In PAGE /cities find the menu type merchant in params input and get link
      if a.page.search(menu_id + " a").blank?
        puts 'city not have this merchant type'
      else
        puts '1111111111111111111111111'

        a.page.link_with(:text => a.page.search(menu_id + " a").first.text).click
        puts 'click link'
        puts
        if a.page.search("title").first.text == "All Cities | Groupon"
          puts 'can not connect link'
        else
          
          puts a.page.search("title").first.text

          ## ******************************************************
          ##   GET Feature
          ## ******************************************************
          div_feature = a.page.search(".featured").first
          if !div_feature.nil?
            feature_title = div_feature.search(".title").first.text.squish
            puts 'title'
            feature_subtitile = div_feature.search(".subtitle").first.text.squish
            puts 'subtitle'
            feature_description = div_feature.search(".description").first.text.squish
            puts 'des'
            # get link feature and go to page detail
            link_detail_feature = a.page.search(".view-deal-button a").first.attributes['href'].value
            get_groupon_detail_us_canada(link_detail_feature,feature_description,tag_type)

          end
          ## ******************************************************
          ##   END GET Feature
          ## ******************************************************


          ## &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
          ##   GET Groupon normal
          ## &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
          a.page.search(".top").each do |groupon|
            description = groupon.text.squish
            link_detail_groupon = groupon.search("a").first.attributes['href'].value
            link_detail_groupon = (link_detail_groupon[0] == '/')? "http://www.groupon.com" + link_detail_groupon : link_detail_groupon
            get_groupon_detail_us_canada(link_detail_groupon,description,tag_type)
            puts

            
          end
          ## &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
          ##   END GET Groupon normal
          ## &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


        end

      end
    end
  end
end


desc "GET GROUPON GATEWAYS"
task :groupon_us_canada_gateways => :environment do
  menu_id = "#getaways_nav"
  tag_type = ["gateway"]
  get_groupon_us_canada(menu_id,tag_type)
end


desc "GET GROUPON GOODS"
task :groupon_us_canada_goods => :environment do
  menu_id = "#goods_nav"
  tag_type = ["good"]
  get_groupon_us_canada(menu_id,tag_type)
end


desc "GET GROUPON SCHOOL 'S OUT"
task :groupon_us_canada_schoolouts => :environment do
  menu_id = "#occasions_graduation_nav"
  tag_type = ["school-out"]
  get_groupon_us_canada(menu_id,tag_type)
end


desc "GET GROUPON HOUSE GRADENT"
task :groupon_us_canada_housegardents => :environment do
  menu_id = "#occasions_house_and_garden_nav"
  tag_type = ["house-gradent"]
  get_groupon_us_canada(menu_id,tag_type)
end


desc "GET GROUPON FATHER 'S DAY"
task :groupon_us_canada_fatherday => :environment do
  menu_id = "#occasions_fathers_day_nav"
  tag_type = ["father-day"]
  get_groupon_us_canada(menu_id,tag_type)
end


desc "Run all rake"
task :test_schedule => :environment do
  #Rake::Task['groupon_us_canada_gateways'].invoke()
  require 'rufus/scheduler'
  scheduler = Rufus::Scheduler.start_new
  scheduler.every '2s' do
    puts "order ristretto"
    #  Rake::Task['groupon_us_canada_gateways'].invoke()
  end

  
  scheduler.join
end