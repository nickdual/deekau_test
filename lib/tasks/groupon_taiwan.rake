# encoding: utf-8
def get_groupon_detail_taiwan(link_detail,tag_type)
  b = Mechanize.new
  link_detail = (link_detail[0]== '/')? "http://www.groupon.com.tw" + link_detail : link_detail
  puts '=============111==========='
  puts link_detail
  b.get(link_detail)
  mer_name = b.page.search(".main_b1").first.search("div")[0].text.squish
  puts 'mer name'
  name = b.page.search(".buy_body").search("h1").first.text.squish
  puts 'name'
  description = b.page.title
  if !b.page.search(".bluebox").blank?
    price =  b.page.search(".bluebox").first.search("tr")[0].search("td").blank? ? nil : b.page.search(".bluebox").first.search("tr")[0].search('td').first.text.squish[1..99].to_f
    puts 'price'
    currency = b.page.search(".bluebox").first.search("tr")[0].search("td").blank? ? nil : b.page.search(".bluebox").first.search("tr")[0].search('td').first.text.squish[0]
    end_time = b.page.search("#timerbg").blank? ? 10 : b.page.search("#timerbg").text.squish.split(":")[0].to_i
    puts 'end time'
  else
    price =  nil
    puts ' no price'
    currency = nil
    end_time = 10
    puts 'no end time'
  end
  discount = nil
  link = nil
  link_text = nil
  
  # CHECK link 
  b.page.search(".remove").first.search("tr")[0].search("td")[1].search("a").each do |l|
    if "官方網站".in? l.text.squish
      link = l.attributes['href'].value
      link_text = "官方網站"
      break
    end
  end
  puts 'link text'
  
  phone = nil
  #chEck Phone
  b.page.search(".remove").first.search("td")[1].search("p").each do |ph|
    if "電話".in? ph.text.squish
      phone = ph.text.squish[3..99]
      break
    end
  end
  puts 'phone'

  address = nil
  # check address
  b.page.search(".remove").first.search("td")[1].search("span").each do |add|
    if "地址".in? add.text.squish
      address = add.text.squish.split("電話")[0][3..99]
      break
    end
  end
  puts 'address'
  mer_description = description
  mobile_phone = nil
  
  @merchant = create_merchant(mer_name,mer_description,tag_type,end_time,address,phone,mobile_phone)
  @offer = create_offer(@merchant,link,name,description,end_time,price,discount,currency,address,phone,mobile_phone)

  puts img = b.page.search("#img1 img").first.attributes["src"].value
  @offer.images.create(:img_url_web => img)
  @merchant.images.create(:img_url_web => img)
  puts

end



def get_groupon_taiwan(link,tag_type)
  a = Mechanize.new
  a.gzip_enabled = false
  a.ignore_bad_chunking = true
  a.keep_alive = false
  puts '======================================='
  puts link
  a.get(link)
  puts 'get page'

  
  a.page.search(".list_name a").each do |link_detail|
    get_groupon_detail_taiwan(link_detail.attributes['href'].value,tag_type)
  end


end


desc "GET GROUPON TAIWAN"
task :groupon_taiwan => :environment do
  
  require 'rubygems'
  require 'mechanize'
  mer_type_good = ['good']
  mer_type_gateways = ['gateway']

  # 2 MENU TAG MAIN
  get_groupon_taiwan("http://www.groupon.com.tw/deallist2.php?mt=2832",mer_type_good)
  get_groupon_taiwan("http://getaways.groupon.com.tw/?mt=3322",mer_type_gateways)
  
end
