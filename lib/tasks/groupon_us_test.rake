# encoding: utf-8
def get_groupon_detail_us_test(link_detail,description,tag_type)
  puts '==============='
  doc_detail = Mechanize.new
  (link_detail[0]=='/')? doc_detail.get("http://www.groupon.com" + link_detail) : doc_detail.get(link_detail)

  address = doc_detail.page.search(".deal_location").blank? ? nil :doc_detail.page.search("address").first.search("p").first.text.split("\n")
  primary_address = (address.blank? || address[1].blank?) ? nil : address[1].squish
  phone = (address.blank? || address[5].blank?)? nil : address[5].squish.split("|")[0]

  f_merchant = Merchant.create(:name => doc_detail.page.search("hgroup h2").first.text.squish,
    :description=>description,
    :phone=>phone,
    :primary_address => primary_address,
    :tags=>tag_type,
    :start_open_time=>Time.current,
    :end_open_time=>Time.current + ((doc_detail.page.search(".groupon_countdown").blank?)? 10 : doc_detail.page.search(".groupon_countdown").first.text.squish.split(':')[0].to_i).days)

  
  if doc_detail.page.search(".value").blank? || doc_detail.page.search(".value").search(".integer").blank? 
    f_offer = f_merchant.offers.create(
      :link=>doc_detail.page.search(".deal_right_rail").first.search(".name a").first.blank? ? nil : doc_detail.page.search(".deal_right_rail").first.search(".name a").first.attributes['href'].value,
      :phone => phone,
      :name=>doc_detail.page.search("hgroup h2").first.text.squish,
      :active=>1,
      :description=>description,
      :primary_address => primary_address,
      :start_time=>Time.current,
      :end_time=>Time.current + ((doc_detail.page.search(".groupon_countdown").blank?)? 10 : doc_detail.page.search(".groupon_countdown").first.text.squish.split(':')[0].to_i).days,
      :price=> doc_detail.page.search(".price").search(".integer").first.text.squish.to_f,
      :discount=>(doc_detail.page.search(".discount").blank? || doc_detail.page.search(".discount").search(".integer").blank? )? nil : doc_detail.page.search(".discount").search(".integer").first.text,
      :currency=>doc_detail.page.search(".price").search(".unit").first.text.squish,
      :city_name=>doc_detail.page.search(".deal_location").blank? ? nil : doc_detail.page.search("address").first.search("h4").first.text)

  else
    f_offer = f_merchant.offers.create(     
      :link=>doc_detail.page.search(".deal_right_rail").first.search(".name a").first.blank? ? nil : doc_detail.page.search(".deal_right_rail").first.search(".name a").first.attributes['href'].value,
      :phone => phone,
      :name=>doc_detail.page.search("hgroup h2").first.text.squish,
      :active=>1,
      :description=>description,
      :primary_address => primary_address,
      :start_time=>Time.current,
      :end_time=>Time.current + ((doc_detail.page.search(".groupon_countdown").blank?)? 10 : doc_detail.page.search(".groupon_countdown").first.text.squish.split(':')[0].to_i).days,
      :price=> (doc_detail.page.search(".value").blank? || doc_detail.page.search(".value").search(".integer").blank? )? nil : doc_detail.page.search(".value").search(".integer").first.text,
      :discount=>(doc_detail.page.search(".discount").blank? || doc_detail.page.search(".discount").search(".integer").blank? )? nil : doc_detail.page.search(".discount").search(".integer").first.text,
      :currency=>(doc_detail.page.search(".value").blank? || doc_detail.page.search(".value").search(".unit").blank? )? nil : doc_detail.page.search(".value").search(".unit").first.text,
      :city_name=>doc_detail.page.search(".deal_location").blank? ? nil : doc_detail.page.search("address").first.search("h4").first.text)

  end
  
            
  if doc_detail.page.search(".deal_thumbnails").blank?

    f_offer.images.create(:img_url_web => doc_detail.page.search(".image_wrapper img").first.attributes['src'].value)
    f_merchant.images.create(:img_url_web => doc_detail.page.search(".image_wrapper img").first.attributes['src'].value)

  else

    f_merchant.images.create(:img_url_web => doc_detail.page.search(".deal_thumbnails").first.search("img").first.attributes['src'].value)
    doc_detail.page.search(".deal_thumbnails").first.search("img").each do |img|
      f_offer.images.create(:img_url_web => img.attributes['src'].value)
    end

  end
end
def get_groupon_us_test(menu_id,tag_type)
  
  require 'rubygems'
  require 'mechanize'


  city = Mechanize.new
  city.get("http://www.groupon.com/cities?dl=d47388")
  city.page.search(".changing_location").each do |a_city|

    if a_city.text == "\/cities?dl=d47388"

    else
      ## vi` trang web dang cap nhat du lieu nen phai xet dk lai (set cookie refesh - kiem tra menu ton tai theo city)
      a = Mechanize.new
      a.get("http://www.groupon.com/cities?dl=d47388")

      cookie = Mechanize::Cookie.new('division', a_city.text.downcase.parameterize)
      
      cookie.domain = ".groupon.com"
      cookie.path = "/"
      a.cookie_jar.add(a.history.last.uri,cookie)
      a.get("http://www.groupon.com/cities?dl=d47388")
      

      if a.page.search(menu_id + " a").blank?
      
      else
      


        a.page.link_with(:text => a.page.search(menu_id + " a").first.text).click

        if a.page.search("title").first.text == "All Cities | Groupon"

        else
          link_detail_feature = a.page.search(".featured").first.blank? ? nil : a.page.search(".view-deal-button a").first.attributes['href'].value
          if !link_detail_feature.blank?
            get_groupon_detail_us_test(link_detail_feature,(a.page.search(".featured").first.blank? ? '' : a.page.search(".featured").first.search(".description").first.text.squish),tag_type)
          end
          a.page.search(".top").each do |groupon|
            get_groupon_detail_us_test(link_detail_feature,(a.page.search(".featured").first.blank? ? '' : a.page.search(".featured").first.search(".description").first.text.squish),tag_type)
          end
          
         
        end

      end
    end
  end
end


desc "GET GROUPON GATEWAYS"
task :groupon_us_test => :environment do
  get_groupon_us_test("#getaways_nav",["gateway"])
end

