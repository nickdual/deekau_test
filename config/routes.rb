Company::Application.routes.draw do

  resources :photos, :only => [:index, :show, :new, :create] do
    post 'upload', :on => :collection
  end

  match "/albums" => "albums#index"
  match "/social_post" => "home#social_post"
  post "users/unique_email"
  get "home/" => "home#index"
  get "home/lookup_address" =>"home#lookup_address"
  get 'home/autocomplete'
  get 'home/autocomplete_category'
  get 'home/get_current_position' => 'home#get_current_position' ,:as => :get_current_position
  get 'offers/tmp_list' , :as => :offers_list
  get "/users/get_user"
  get '/list_offers' => "offers#list_offer" , :as => :list_offers
  get 'merchats/tmp_detail'
  get 'offers/tmp_filter'
  get '/categories/top_lvl'
  get '/merchants/get_offers_quota'
  match 'categories/:category' => "categories#category"
  match 'categories/:category/offers' => "categories#offers"
  match 'categories/:category/:sub_category' => "categories#sub_category"
  match 'categories/:category/:sub_category/offers' => "categories#offers"
  
  resources :articles
  resources :countries do
    resources :cities
  end

  
  match '/users/profile' => 'users#show' ,:as => :user_profile
  match '/merchants/get_image' => 'merchants#get_image' ,:as => :merchant_get_image
  match '/offers/get_image' => 'offers#get_image' ,:as => :offer_get_image

  match "offers/:id/get_images" => "offers#get_image"
  post 'offers/create/:id'  => "offers#create"
  
  match "/images/:id/delete" => "images#destroy" ,:as => :delete_image
  match "/merchant_images/:id/delete" => "merchant_images#destroy" ,:as => :delete_merchant_image
  match "/offer_images/:id/delete" => "offer_images#destroy" ,:as => :delete_offer_image
  match 'offers/new' => 'offers#new'
  match '/offers' => 'offers#index'
  get 'offers/create/:id'  => "offers#get_offer"
  #  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" } do
  #    #post '/users' => 'registrations#create'
  #    #get '/users/sign_up' => 'registrations#new'
  #    get 'sign_in', :to => 'users/sessions#new', :as => :new_user_session
  #    get 'sign_out', :to => 'users/sessions#destroy', :as => :destroy_user_session
  #  end

  devise_for :users, :controllers => { :registrations => "registrations",:omniauth_callbacks => "omniauth_callbacks",:sessions => "sessions" }
  match "/offers/get_more_offers" => "offers#get_more_offers"
  
  resources :merchant_types
  resources :merchants do
    collection do
      get 'set_favourite'
    end
    resources :offers do 
      collection do
        get 'set_favourite'
      end
    end
  end

  match '/users/profile/users/update' =>  'users#update'
  match '/users/update' =>  'users#update'
  match '/users/friends' => 'users#friends'
  match '/users/find_friend_oauth_provider' => 'users#find_friend_oauth_provider'
  match '/users/friend_action' => 'users#friend_action'
  match '/users/invite' => 'users#invite'

  match '/chats/find/:id' => 'chats#find'
  match '/chats/load_more' => 'chats#load_more'
  match '/chats/init' => 'chats#init'
  match '/chats/add_recent_chat' => 'chats#add_recent_chat'

  post '/get_google_places'  => "offers#get_google_places"
  #post '/home' => "offers#create"
  #match '/#post_offer' =>
  #  devise_for :users, :controllers => { :registrations => "registrations" ,:omniauth_callbacks => "omniauth_callbacks"} do
  #   get '/users/sign_out' => "devise/sessions#destroy"
  #  end

  
  #  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" } do
  #    post '/users' => 'registrations#create'
  #    get '/users/sign_up' => 'registrations#new'
  #    get 'sign_in', :to => 'users/sessions#new', :as => :new_user_session
  #    get 'sign_out', :to => 'users/sessions#destroy', :as => :destroy_user_session
  #  end
  root :to =>'home#index'
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
