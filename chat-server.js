var http = require('http').createServer();
var io = require('socket.io');
var _ = require('underscore')._;
var mongo = require('mongoskin');
var db = mongo.db('localhost:27017/company_development?auto_reconnect', {safe: false});

io = io.listen(http);
io.configure(function(){
  io.set("transports", ["xhr-polling"]); 
  io.set("polling duration", 10); 
  io.set("close timeout", 12);
  io.set("log level", 1);
})

var CHAT_STATUS = (function() {
	var value = {
		'AVAILABLE': 1,
        'BUSY': 2,
		'INVISIBLE': 3,
		'TURN_OFF': 4
	};

	return {
        get: function(name) { return value[name]; }
    };
})();
		
var clients = {};

io.sockets.on('connection', function(socket) {
	var user;
	
	socket.on('turn-on', function(data, fn) {
		user = data;
		socket['userid'] = user.id;
		if (clients[user.id] == undefined) {
			user.status = CHAT_STATUS.get('AVAILABLE')
			clients[user.id] = {id: user.id, username: user.username, avatar: user.avatar, status: CHAT_STATUS.get('AVAILABLE'), socket: {}, friends: {}, room: {}};
		} else {
			user.status = clients[user.id]['status']
			_.each(clients[user.id]['room'], function(value, rid) {
				io.sockets.socket(socket.id).join(rid);				
				var cbd_friends = {}
				_.each(io.sockets.clients(data.roomid), function(value, sid) {
					id = value['userid'];
					if (id != undefined && clients[id] != undefined) {
						cbd_friends[id] = {username: clients[id]['username'], avatar: clients[id]['avatar'], status: clients[id]['status']};
					}
				});
				io.sockets.socket(socket.id).emit('users-in-room', {id: rid, users: cbd_friends});
				
			});
		}
		clients[user.id]['socket'][socket.id] = data.ip_address;
		//load user's friends from mongodb to indentify who is online and his status
		db.collection('friends').findOne({uid: data.id}, function(err, friends){
			var cbd_friends = {};
			if (friends != null) {
				_.each(friends['list'], function(friend, key) {
					if (clients[key] != undefined) {
						//set list friend online for current user
						cbd_friends[key] = clients[key]['status'];
						//set status of user in friend's attributes
						clients[key]['friends'][user.id] = CHAT_STATUS.get('AVAILABLE');
						//loop client's sockets 
						_.each(clients[key]['socket'], function(value, sid) {
							io.sockets.socket(sid).emit('user-connect', user);
						});
					}
				});
			}
			clients[user.id]['friends'] = cbd_friends;
			fn(cbd_friends);
		});
	});

	socket.on('set-status', function(data, fn) {
		if (data.status == 0)
			return
		clients[user.id]['status'] = data.status;
		user.status = data.status;
		_.each(clients[user.id]['socket'], function(value, sid) {
			io.sockets.socket(sid).emit('receive-status', user);
		});
		_.each(clients[user.id]['friends'], function(value, friendid) {
			if (clients[friendid] != undefined) {
				clients[friendid]['friends'][user.id] = data.status;
				_.each(clients[friendid]['socket'], function(value, sid) {
					io.sockets.socket(sid).emit('receive-status', user);
				});
			}
		});
	});
	
	socket.on('send-message', function(data, fn) {
		var compare = {};
		var time_now = new Date();
		if (user.id > data.id)
			compare = {"uid1": user.id, "uid2": data.id, "date": new Date(time_now.getFullYear(),time_now.getMonth(),time_now.getDate() + 1)};
		else
			compare = {"uid2": user.id, "uid1": data.id, "date": new Date(time_now.getFullYear(),time_now.getMonth(),time_now.getDate() + 1)};
		db.collection('chats').update(compare, {$push: {"log": {"timestamp": time_now, "sender": user.id, "content": data.content}}, $inc: {"count": 1}}, {upsert: true});	
			

		if (clients[data.id] != undefined) {
			_.each(clients[data.id]['socket'], function(value, key) {
				io.sockets.socket(key).emit('receive-message', {id: user.id, tab_id: user.id, username: user.username, avatar: user.avatar, status: clients[user.id]['status'],content: data.content, time: time_now});		
			});
		}
	});
	
	socket.on('join-room', function(data, fn) {
		if (clients[data.id] != undefined) {
			clients[user.id]['room'][data.roomid] = 1;
			clients[data.id]['room'][data.roomid] = 1;
			_.each(clients[user.id]['socket'], function(value, sid) {
				io.sockets.socket(sid).join(data.roomid);
				if (data.type != undefined && data.type == 0) {
					io.sockets.socket(sid).emit('switch-to-room', {id: data.id, status: 'ok', groupid: data.roomid});
				}
			});
			_.each(clients[data.id]['socket'], function(value, sid) {
				io.sockets.socket(sid).join(data.roomid);
				if (data.type != undefined && data.type == 0) {
					io.sockets.socket(sid).emit('switch-to-room', {id: user.id, status: 'ok', groupid: data.roomid});
				}
			});
			cbd_friends = {}
			_.each(io.sockets.clients(data.roomid), function(value, sid) {
				id = value['userid'];
				if (id != undefined && clients[id] != undefined) {
					cbd_friends[id] = {username: clients[id]['username'], avatar: clients[id]['avatar'], status: clients[id]['status']};
				}
			});
			_.each(io.sockets.clients(data.roomid), function(value, sid) {
				io.sockets.socket(value.id).emit('users-in-room', {id: data.roomid, users: cbd_friends});
			});
		}
	});
	
	socket.on('leave-room', function(data, fn) {
		delete clients[user.id]['room'][data.roomid];		
		_.each(clients[user.id]['socket'], function(value, sid) {
			io.sockets.socket(sid).leave(data.roomid);
		});
		_.each(io.sockets.clients(data.roomid), function(value, sid) {
			id = value['userid']
			if (id != undefined && clients[id] != undefined) {
				io.sockets.socket(value.id).emit('leave-room', {id: user.id, username: user.username, roomid: data.roomid});
			}
		});
	});
	
	socket.on('send-msg-room', function(data, fn) {
		var time_now = Date.now();
		io.sockets.in(data.id).emit('receive-message', {id: user.id, username: user.username, avatar: user.avatar, status: clients[user.id]['status'], content: data.content, time: time_now, tab_id: data.id});
	});
	
	socket.on('disconnect', function () {
		if (user) {
			console.log("Disconnected: ", user.id)
	    	delete clients[user.id]['socket'][socket.id];
	    	var empty = true;
	    	//clients can have multi connection from different browsers -> delete connect which belong to browser has been closed
	    	_.each(clients[user.id]['socket'], function(value, key) {
				empty = false;	
			});
	    	if (empty == true) {
				//loop list friends
				_.each(clients[user.id]['friends'], function(value, key) {
					if (clients[key] != undefined) {
						delete clients[key]['friends'][user.id];
						_.each(clients[key]['socket'], function(value, sid) {
							io.sockets.socket(sid).emit('user-disconnect', user);
						});
					}
				});
				_.each(io.sockets.manager.roomClients[socket.id], function(value, key) {
					if (key != '') {
						roomid = key.substr(1, key.length);
						_.each(clients[user.id]['socket'], function(value, sid) {
							io.sockets.socket(sid).leave(roomid);
						});
						_.each(io.sockets.clients(roomid), function(value, sid) {
							id = value['userid']
							if (id != undefined && clients[id] != undefined) {
								io.sockets.socket(value.id).emit('leave-room', {id: user.id, username: user.username, roomid: roomid});
							}
						});
					}	
				});
				delete clients[user.id];
			}
		}
	});
});

//this line is necessary for heroku
var port = process.env.PORT || 27290;
http.listen(port);
