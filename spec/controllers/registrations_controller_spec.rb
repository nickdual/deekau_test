require "spec_helper"

describe RegistrationsController do
  describe "Sign up" do
    it "valid attributes" do
      request.env["devise.mapping"] = Devise.mappings[:user]
      post :create , :user => FactoryGirl.attributes_for(:user_sign_up),:format => 'json'
      response.should contain(:user)
    end
    it "unvalid attributes"   do
      request.env["devise.mapping"] = Devise.mappings[:user]
      post :create , :user => {:user_name => "jack",:email => '',:email_confirmation => '',:password => '123456'} ,:format => 'json'
      response.body.should have_content "{\"password\":[\"can't be blank\"]}"
    end
  end


end