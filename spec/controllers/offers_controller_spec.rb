require "spec_helper"
describe OffersController do
  describe "Post offer" do
    it 'post offer successful' do
      @user = FactoryGirl.create(:user)
      request.env["devise.mapping"] = Devise.mappings[:user]
      post :create , :user => {:email => @user.email, :password => @user.password }  ,:format => 'json'
      post :create , :offer=>{:description => "abcdef",:local_mon_start_time=>"00:00", :local_mon_end_time=>"19:00", :local_tue_start_time=>"00:00", :local_tue_end_time=>"21:00",  :mon=>"true", :tue=>"false", :wed=>"false", :thu=>"false", :fri=>"false", :sat=>"false", :sun=>"false"}, :files=>File.open(File.join(Rails.root, './public/images/test.png')) ,:format => 'json'
      #@offer.get_image.should eq("/uploads/image/url/#{@offer.id}/test.png")
      #response.should include('test.png')
      response.should be_success
    end

    it 'post offer unsuccessful with not description' do
      @user = FactoryGirl.create(:user)
      request.env["devise.mapping"] = Devise.mappings[:user]
      post :create , :user => {:email => @user.email, :password => @user.password }  ,:format => 'json'
      post :create , :offer=>{:description => ""}, :files=>File.open(File.join(Rails.root, './public/images/test.png')) ,:format => 'json'
      #@offer.get_image.should eq("/uploads/image/url/#{@offer.id}/test.png")
      #response.should include('test.png')
      response.code.should eq('500')
    end
  end
end