FactoryGirl.define do
  factory :user_sign_up do
    user_name 'Jack'
    email 'abc123@gmail.com'
    email_confirmation 'abc123@gmail.com'
    password '123456'

  end

  factory :user, :class => User do
    user_name 'Jack'
    email 'abc123@gmail.com'
    password '123456'
  end
end