require 'spec_helper'

describe "photos/index" do
  before(:each) do
    assign(:photos, [
      stub_model(Photo,
        :image_file_name => "Image File Name",
        :description => "Description",
        :image_file_size => 1,
        :image_type => "Image Type"
      ),
      stub_model(Photo,
        :image_file_name => "Image File Name",
        :description => "Description",
        :image_file_size => 1,
        :image_type => "Image Type"
      )
    ])
  end

  it "renders a list of photos" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Image File Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Image Type".to_s, :count => 2
  end
end
