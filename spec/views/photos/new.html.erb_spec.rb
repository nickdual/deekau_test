require 'spec_helper'

describe "photos/new" do
  before(:each) do
    assign(:photo, stub_model(Photo,
      :image_file_name => "MyString",
      :description => "MyString",
      :image_file_size => 1,
      :image_type => "MyString"
    ).as_new_record)
  end

  it "renders new photo form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => photos_path, :method => "post" do
      assert_select "input#photo_image_file_name", :name => "photo[image_file_name]"
      assert_select "input#photo_description", :name => "photo[description]"
      assert_select "input#photo_image_file_size", :name => "photo[image_file_size]"
      assert_select "input#photo_image_type", :name => "photo[image_type]"
    end
  end
end
