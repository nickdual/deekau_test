require 'spec_helper'

describe "photos/show" do
  before(:each) do
    @photo = assign(:photo, stub_model(Photo,
      :image_file_name => "Image File Name",
      :description => "Description",
      :image_file_size => 1,
      :image_type => "Image Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Image File Name/)
    rendered.should match(/Description/)
    rendered.should match(/1/)
    rendered.should match(/Image Type/)
  end
end
