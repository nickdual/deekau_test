module GeonamesRails
  module Writers
    class ActiveRecord
      
      def write_country(country_mapping)  
        iso_code = country_mapping[:iso_code_two_letter]
        c = Country.find_or_initialize_by(:iso_code_two_letter => iso_code)
      
        created_or_updating = c.new_record? ? 'Creating' : 'Updating'
        
        c.attributes = country_mapping.slice(
          :iso_code_two_letter,
          :iso_code_three_letter,
          :iso_number,
          :capital,
          :continent,
          :geonames_id)
        c._id = country_mapping[:name].to_s
        c.save!
        
        "#{created_or_updating} db record for #{iso_code}"
      end
            
      def write_cities(country_code, city_mappings)    
        country = Country.where(:iso_code_two_letter => country_code).first
               
        city_mappings.each do |city_mapping|

          city = City.find_or_initialize_by(geonames_id: city_mapping[:geonames_id])
          city._id = city_mapping[:name] + "_" +  country.id
          #  city.country_id = country.id
          city.name = city_mapping[:name]
          city.country_tag = country.id
          city.latitude = city_mapping[:latitude]
          city.longitude = city_mapping[:longitude]
#          city.country_iso_code_two_letters = city_mapping[:country_iso_code_two_letters]
          city.geonames_timezone_id = city_mapping[:geonames_timezone_id]
               
          city.save!
        end
        
        "Processed #{country.id}(#{country_code}) with #{city_mappings.length} cities"
      end

    end
  end
end
